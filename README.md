# OLAT Question Creation

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![OpenOlat](https://img.shields.io/badge/OpenOlat-18.2--SNAPSHOT-1aa6be)](https://gitlab.com/olatorg/OpenOLAT/-/tree/OLAT_18.2)
[![Spring Boot](https://img.shields.io/badge/Spring_Boot-3.2.3-6bb536)](https://docs.spring.io/spring-boot/docs/3.2.3/reference/html/)

This module provides a course node for question creation.

## Usage

1. Add this repository to the `repositories` section of your project's `pom.xml`.

   ```xml
   <repositories>
     <repository>
       <id>olatorg-repo</id>
       <url>https://gitlab.com/api/v4/groups/olatorg/-/packages/maven</url>
     </repository>
   </repositories>
   ```

2. Add the following dependency to your project's `pom.xml`.

   ```xml
   <dependencies>
     <dependency>
       <groupId>org.olat</groupId>
       <artifactId>olat-question-creation</artifactId>
       <version>${olat-question-creation.version}</version>
     </dependency>
   </dependencies>
   ```

## Docker

OLAT Question Creation can be used with [Docker](https://www.docker.com/get-started/)
and [Docker Compose](https://docs.docker.com/compose/).

Use the following commands to start OLAT with the provided `docker-compose.yml` file.

1. Authenticate with
   the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/).

   ```shell
   docker login registry.gitlab.com -u <username> -p <token>
   ```

2. Create a `.env` file to pass environment variables.

   ```shell
   cat > .env<< EOF
   TAG=<tag>
   EOF
   ```

   where `<tag>` is 1.x-SNAPSHOT for current release version (e.g. 1.1-SNAPSHOT), or `lmsuzh-<ticketnumber>` for other branches (lowercase for branch name))

3. Start Docker containers.

   ```shell
   docker-compose up --detach
   ```

4. Stop and remove containers and volumes

   ```shell
   docker-compose down --volumes
   ``` 

## Development

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olat-question-creation.git
   cd olat-question-creation
   ```

2. Create a local PostgreSQL database (e.g. with [Docker](https://docs.docker.com/get-started/)).

   ```shell
   docker run --name olat-test-db --env POSTGRES_DB=olattest --env POSTGRES_USER=olat --env POSTGRES_PASSWORD=olat -p 5432:5432 -v olat-test-db_data:/var/lib/postgresql/data --detach postgres:11
   ```

3. Build the project and run tests.

   ```shell
   ./mvnw verify
   ```
