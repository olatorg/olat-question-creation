DO $$
    BEGIN
        IF EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_schema = 'qc' and table_name='peer_review_question' and column_name='course_node_ident')
        THEN
            ALTER TABLE qc.peer_review_question RENAME COLUMN course_node_ident TO repo_course_node_ident;
        END IF;
    END $$;

DO $$
    BEGIN
        IF EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_schema = 'qc' and table_name='submission_task' and column_name='course_node_ident')
        THEN
            ALTER TABLE qc.submission_task RENAME COLUMN course_node_ident TO repo_course_node_ident;
        END IF;
    END $$;
