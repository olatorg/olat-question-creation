CREATE TABLE IF NOT EXISTS qc.peer_review_task
(
    id                              bigint not null,
    version                         integer default 0 not null,
    fk_task                         bigint not null,
    fk_reviewer                     bigint not null,
    assign_date                     timestamp not null,
    final_review_submit_date        timestamp,
    creation_date                   timestamp not null,
    last_modified                   timestamp not null,
    PRIMARY KEY (id),
    FOREIGN KEY (fk_task) REFERENCES qc.submission_task,
    FOREIGN KEY (fk_reviewer) REFERENCES o_bs_identity,
    UNIQUE (fk_task, fk_reviewer)
);
