CREATE SCHEMA IF NOT EXISTS qc;

CREATE TABLE IF NOT EXISTS qc.submission_task
(
    id                              bigint not null,
    version                         integer default 0 not null,
    course_node_ident               varchar(50) not null,
    fk_creator                      bigint not null,
    submission_task_index           integer not null,
    final_submit_date               timestamp,
    creation_date                   timestamp not null,
    last_modified                   timestamp not null,
    PRIMARY KEY (id),
    FOREIGN KEY (fk_creator) REFERENCES o_bs_identity,
    UNIQUE (course_node_ident, fk_creator, submission_task_index)
);
