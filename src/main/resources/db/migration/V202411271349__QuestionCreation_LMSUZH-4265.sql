CREATE TABLE IF NOT EXISTS qc.peer_review_question
(
    id                              bigint not null,
    version                         integer default 0 not null,
    course_node_ident               varchar(50) not null,
    question_nr                     integer not null,
    question_text                   text not null,
    fk_creator                      bigint not null,
    creation_date                   timestamp not null,
    last_modified                   timestamp not null,
    PRIMARY KEY (id),
    FOREIGN KEY (fk_creator) REFERENCES o_bs_identity,
    UNIQUE (course_node_ident, question_nr) INITIALLY DEFERRED
);

CREATE TABLE IF NOT EXISTS qc.peer_review_evaluation
(
    id                              bigint not null,
    version                         integer default 0 not null,
    fk_peer_review_task             bigint not null,
    fk_peer_review_question         bigint not null,
    likert_scale_value              integer not null,
    creation_date                   timestamp not null,
    last_modified                   timestamp not null,
    PRIMARY KEY (id),
    FOREIGN KEY (fk_peer_review_task) REFERENCES qc.peer_review_task,
    FOREIGN KEY (fk_peer_review_question) REFERENCES qc.peer_review_question,
    UNIQUE (fk_peer_review_task, fk_peer_review_question)
);

ALTER TABLE qc.peer_review_task
    ADD COLUMN IF NOT EXISTS review_reasoning text;

ALTER TABLE qc.submission_task ALTER COLUMN final_submit_date SET NOT NULL;
