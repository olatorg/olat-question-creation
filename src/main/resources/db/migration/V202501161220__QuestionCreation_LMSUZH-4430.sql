ALTER TABLE qc.submission_task
    ADD COLUMN IF NOT EXISTS is_export_to_pool_allowed boolean default true;
