/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service;

import java.util.List;
import org.olat.core.id.Identity;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.service.Participant.QuestionStatus;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public interface ParticipantService {

  List<Participant> getParticipants(
      Identity coach,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent);

  QuestionStatus getQuestionStatus(
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment,
      String repoCourseNodeIdent,
      long identityKey,
      boolean finallySubmitted);
}
