/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.olat.basesecurity.GroupRoles;
import org.olat.core.id.Identity;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.modules.ModuleConfiguration;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.Participant;
import org.olat.questioncreation.service.Participant.QuestionStatus;
import org.olat.questioncreation.service.ParticipantService;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryRelationType;
import org.olat.repository.RepositoryService;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Service("questionCreationParticipantServiceImpl")
@Slf4j
public class ParticipantServiceImpl implements ParticipantService {

  private final RepositoryService repositoryService;
  private final SubmissionTaskService submissionTaskService;
  private final AssessmentItemService assessmentItemService;
  private final PeerReviewService peerReviewService;

  public ParticipantServiceImpl(
      @Lazy RepositoryService repositoryService,
      SubmissionTaskService submissionTaskService,
      AssessmentItemService assessmentItemService,
      PeerReviewService peerReviewService) {
    this.repositoryService = repositoryService;
    this.submissionTaskService = submissionTaskService;
    this.assessmentItemService = assessmentItemService;
    this.peerReviewService = peerReviewService;
  }

  @Override
  @Transactional(readOnly = true)
  public List<Participant> getParticipants(
      Identity coach,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent) {
    CourseEnvironment courseEnvironment = userCourseEnvironment.getCourseEnvironment();
    RepositoryEntry entry = courseEnvironment.getCourseGroupManager().getCourseEntry();
    return (userCourseEnvironment.isAdmin()
            ? repositoryService
                .getMembers(entry, RepositoryEntryRelationType.all, GroupRoles.participant.name())
                .stream()
                .distinct()
                .toList()
            : repositoryService.getCoachedParticipants(coach, entry))
        .stream()
            .map(
                identity -> {
                  Optional<SubmissionTask> submittedTaskOptional =
                      submissionTaskService
                          .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
                              identity.getKey(), repoCourseNodeIdent, 0);
                  QuestionStatus questionStatus =
                      getQuestionStatus(
                          courseNode,
                          courseEnvironment,
                          repoCourseNodeIdent,
                          identity.getKey(),
                          submittedTaskOptional.isPresent());
                  QTI21QuestionType questionType = null;
                  ResolvedAssessmentItem resolvedAssessmentItem =
                      assessmentItemService.getSavedAssessmentItem(
                          courseEnvironment, courseNode, identity.getKey());
                  if (resolvedAssessmentItem != null) {
                    questionType = QTI21QuestionType.getType(resolvedAssessmentItem);
                  }
                  SubmissionTask submittedTask = submittedTaskOptional.orElse(null);
                  boolean isExportToPoolAllowedForTask =
                      submittedTask != null && submittedTask.getIsExportToPoolAllowed();
                  return new Participant(
                      identity, questionStatus, questionType, isExportToPoolAllowedForTask);
                })
            .toList();
  }

  @Override
  public QuestionStatus getQuestionStatus(
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment,
      String repoCourseNodeIdent,
      long identityKey,
      boolean finallySubmitted) {
    ModuleConfiguration moduleConfiguration = courseNode.getModuleConfiguration();
    Date taskStartDate =
        moduleConfiguration.getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START);
    Date taskDueDate =
        moduleConfiguration.getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
    boolean peerReviewsAlreadyAssigned =
        peerReviewService.arePeerReviewsAlreadyAssigned(repoCourseNodeIdent);
    Date peerReviewDueDate =
        moduleConfiguration.getDateValue(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE);
    boolean peerReviewFinallySubmitted =
        peerReviewService.arePeerReviewTasksFinallySubmitted(repoCourseNodeIdent, identityKey);

    return evaluateQuestionStatus(
        taskStartDate,
        taskDueDate,
        finallySubmitted,
        peerReviewsAlreadyAssigned,
        peerReviewDueDate,
        peerReviewFinallySubmitted);
  }

  static QuestionStatus evaluateQuestionStatus(
      Date taskStartDate,
      Date taskDueDate,
      boolean finallySubmitted,
      boolean peerReviewAssigned,
      Date peerReviewDueDate,
      boolean peerReviewFinallySubmitted) {
    Date now = new Date();

    if (taskStartDate != null && taskStartDate.after(now)) {
      return QuestionStatus.UNPUBLISHED;
    }

    if (!finallySubmitted) {
      return taskDueDate.before(now)
          ? QuestionStatus.SUBMISSION_DEADLINE_MISSED
          : QuestionStatus.SUBMISSION_IN_PROGRESS;
    }

    if (!peerReviewAssigned) {
      return QuestionStatus.SUBMISSION_DONE;
    }

    if (!peerReviewFinallySubmitted) {
      return peerReviewDueDate.before(now)
          ? QuestionStatus.PEER_REVIEW_DEADLINE_MISSED
          : QuestionStatus.PEER_REVIEW_ASSIGNED;
    }

    return QuestionStatus.PEER_REVIEW_DONE;
  }
}
