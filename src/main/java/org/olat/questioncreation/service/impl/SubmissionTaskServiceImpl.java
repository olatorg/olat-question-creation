/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service.impl;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.data.repository.SimpleIdentityRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.olat.core.logging.AssertException;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.data.repository.SubmissionTaskRepository;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Service
public class SubmissionTaskServiceImpl implements SubmissionTaskService {

  @Autowired private SubmissionTaskRepository submissionTaskRepository;
  @Autowired private SimpleIdentityRepository simpleIdentityRepository;

  @Override
  @Transactional
  public void save(
      long creatorIdentityKey,
      String repoCourseNodeIdent,
      Integer submissionTaskIndex,
      Date finalSubmitDate,
      boolean isExportToPoolAllowed) {
    SubmissionTask submissionTask = new SubmissionTask();
    Optional<SimpleIdentity> simpleIdentityCreatorOpt =
        simpleIdentityRepository.findById(creatorIdentityKey);
    if (simpleIdentityCreatorOpt.isEmpty()) {
      throw new AssertException(
          "SubmissionTaskService: could not found SimpleIdentity with creator.key="
              + creatorIdentityKey);
    }
    submissionTask.setCreator(simpleIdentityCreatorOpt.get());
    submissionTask.setRepoCourseNodeIdent(repoCourseNodeIdent);
    submissionTask.setSubmissionTaskIndex(submissionTaskIndex);
    submissionTask.setFinalSubmitDate(finalSubmitDate);
    submissionTask.setIsExportToPoolAllowed(isExportToPoolAllowed);
    submissionTaskRepository.save(submissionTask);
  }

  @Override
  public List<SubmissionTask> findByRepoCourseNodeIdent(String repoCourseNodeIdent) {
    return submissionTaskRepository.findByRepoCourseNodeIdent(repoCourseNodeIdent);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<SubmissionTask> findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
      long identityKey, String repoCourseNodeIdent, Integer submissionTaskIndex) {
    return submissionTaskRepository.findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
        identityKey, repoCourseNodeIdent, submissionTaskIndex);
  }
}
