/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.olat.course.ICourse;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.dao.PeerReviewEvaluationDao;
import org.olat.questioncreation.data.dao.PeerReviewQuestionDao;
import org.olat.questioncreation.data.dao.PeerReviewTaskDao;
import org.olat.questioncreation.data.dao.SubmissionTaskDao;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.QuestionCreationDeletionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Service
@Slf4j
public class QuestionCreationDeletionServiceImpl implements QuestionCreationDeletionService {

  private final PeerReviewQuestionDao peerReviewQuestionDao;
  private final PeerReviewTaskDao peerReviewTaskDao;
  private final PeerReviewEvaluationDao peerReviewEvaluationDao;
  private final SubmissionTaskDao submissionTaskDao;
  private final AssessmentItemService assessmentItemService;

  public QuestionCreationDeletionServiceImpl(
      PeerReviewQuestionDao peerReviewQuestionDao,
      PeerReviewTaskDao peerReviewTaskDao,
      PeerReviewEvaluationDao peerReviewEvaluationDao,
      SubmissionTaskDao submissionTaskDao,
      AssessmentItemService assessmentItemService) {
    this.peerReviewQuestionDao = peerReviewQuestionDao;
    this.peerReviewTaskDao = peerReviewTaskDao;
    this.peerReviewEvaluationDao = peerReviewEvaluationDao;
    this.submissionTaskDao = submissionTaskDao;
    this.assessmentItemService = assessmentItemService;
  }

  @Override
  @Transactional
  public void deleteAllQuestionCreationCourseNodeData(
      ICourse course, QuestionCreationCourseNode courseNode) {
    String repoCourseNodeIdent =
        CourseNodeIdentifier.getIdentifierFor(course.getCourseEnvironment(), courseNode);
    log.info(
        "deleteAllQuestionCreationCourseNodeData of course.resourceableId {}, repoCourseNodeIdent {}",
        course.getResourceableId(),
        repoCourseNodeIdent);
    deleteQuestionCreationCourseNodeEntities(repoCourseNodeIdent);
    deleteQuestionCreationFolder(course, courseNode);
  }

  private void deleteQuestionCreationCourseNodeEntities(String repoCourseNodeIdent) {
    List<Long> submissionTaskIds =
        submissionTaskDao.findSubmissionTaskIdByRepoCourseNodeIdent(repoCourseNodeIdent);
    int numDeletedPeerReviewEvaluations = 0;
    int numDeletedPeerReviewTasks = 0;
    int numDeletedSubmissionTasks = 0;
    if (submissionTaskIds != null && !submissionTaskIds.isEmpty()) {
      List<Long> peerReviewTaskIds =
          peerReviewTaskDao.findPeerReviewTaskIdBySubmissionTaskIdIn(submissionTaskIds);
      if (peerReviewTaskIds != null && !peerReviewTaskIds.isEmpty()) {
        numDeletedPeerReviewEvaluations =
            peerReviewEvaluationDao.deleteByPeerReviewTaskIdIn(peerReviewTaskIds);
      }
      numDeletedPeerReviewTasks = peerReviewTaskDao.deleteBySubmissionTaskIdIn(submissionTaskIds);
      numDeletedSubmissionTasks =
          submissionTaskDao.deleteByRepoCourseNodeIdent(repoCourseNodeIdent);
    }
    int numDeletedPeerReviewQuestions =
        peerReviewQuestionDao.deleteByRepoCourseNodeIdent(repoCourseNodeIdent);
    log.info(
        "deleteQuestionCreationCourseNodeEntities: Deleted #PeerReviewEvaluations={}, #PeerReviewTasks={}, #SubmissionTasks={}, #PeerReviewQuestions={}",
        numDeletedPeerReviewEvaluations,
        numDeletedPeerReviewTasks,
        numDeletedSubmissionTasks,
        numDeletedPeerReviewQuestions);
  }

  private void deleteQuestionCreationFolder(ICourse course, QuestionCreationCourseNode courseNode) {
    File questionCreationFolder =
        assessmentItemService.getQTIQuestionFolder(course.getCourseEnvironment(), courseNode);
    try {
      FileUtils.deleteDirectory(questionCreationFolder);
    } catch (IOException e) {
      log.warn(
          "Question creation folder could not be deleted, path={}",
          questionCreationFolder.getAbsolutePath());
    }
  }
}
