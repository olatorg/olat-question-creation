/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.commons.modules.bc.FolderConfig;
import org.olat.core.id.Identity;
import org.olat.core.util.coordinate.Cacher;
import org.olat.core.util.coordinate.CoordinatorManager;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.ims.qti21.QTI21Service;
import org.olat.ims.qti21.model.xml.ManifestMetadataBuilder;
import org.olat.ims.qti21.pool.QTI21QPoolServiceProvider;
import org.olat.modules.qpool.QPoolService;
import org.olat.modules.qpool.QuestionItem;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.service.AssessmentItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uk.ac.ed.ph.jqtiplus.node.item.AssessmentItem;
import uk.ac.ed.ph.jqtiplus.node.result.AssessmentResult;
import uk.ac.ed.ph.jqtiplus.reading.QtiObjectReadResult;
import uk.ac.ed.ph.jqtiplus.reading.QtiObjectReader;
import uk.ac.ed.ph.jqtiplus.reading.QtiXmlInterpretationException;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;
import uk.ac.ed.ph.jqtiplus.xmlutils.XmlResourceNotFoundException;
import uk.ac.ed.ph.jqtiplus.xmlutils.locators.ResourceLocator;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Component
@Slf4j
public class AssessmentItemServiceImpl implements AssessmentItemService {

  /* Note: Due to several cyclic dependencies of the OpenOLAT beans, it's important that the
   * olat-lms beans don't affect the order in which the OpenOLAT beans are loaded. This can be
   * achieved by Spring's @Lazy annotation, which should always be used when autowiring OpenOLAT
   * services from olat-lms beans.
   */
  @Autowired @Lazy private QTI21Service qtiService;
  @Autowired @Lazy private QTI21QPoolServiceProvider qti21QPoolServiceProvider;
  @Autowired @Lazy private QPoolService qPoolService;

  public static final String ASSESSMENT_RESULT_FILENAME_SUFFIX = "assessmentResult.xml";
  public static final String QUESTION_CREATION_FOLDER_NAME = "question_creation";

  @Override
  public boolean hasAnySavedAssessmentItem(
      CourseEnvironment courseEnvironment, QuestionCreationCourseNode courseNode) {
    File[] assessmentItemFiles = getQTIQuestionFolder(courseEnvironment, courseNode).listFiles();
    if (assessmentItemFiles == null) {
      return false;
    }
    return assessmentItemFiles.length > 0;
  }

  @Override
  public ResolvedAssessmentItem getSavedAssessmentItem(
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      long creatorIdentityKey) {
    return getSavedAssessmentItem(
        resolveSavedAssessmentItemFile(courseEnvironment, courseNode, creatorIdentityKey));
  }

  @Override
  public ResolvedAssessmentItem getSavedAssessmentItem(File itemFile) {
    if (!itemFile.exists()) {
      return null;
    }
    log.debug("Load assessment item from file: {}", itemFile.toPath());
    ResolvedAssessmentItem resolvedAssessmentItem =
        qtiService.loadAndResolveAssessmentItem(itemFile.toURI(), itemFile.getParentFile());
    if (resolvedAssessmentItem.getItemLookup().wasSuccessful()) {
      return resolvedAssessmentItem;
    }
    return null;
  }

  @Override
  public File resolveSavedAssessmentItemFile(
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      long creatorIdentityKey) {
    File folder = getQTIQuestionFolder(courseEnvironment, courseNode);
    String itemFilename = creatorIdentityKey + ".xml"; // filename is identity key
    return new File(folder, itemFilename);
  }

  @Override
  public void deleteSavedAssessmentItem(
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      long identityKey) {
    File itemFile = resolveSavedAssessmentItemFile(courseEnvironment, courseNode, identityKey);
    boolean isDeleted = false;
    try {
      isDeleted = Files.deleteIfExists(itemFile.toPath());
    } catch (IOException e) {
      log.warn("Could not delete assessment item file: {}", itemFile.toPath());
    }
    if (isDeleted) {
      log.debug("Deleted assessment item file: {}", itemFile.getAbsolutePath());
    } else {
      log.debug(
          "Assessment item file could not be deleted because it does not exist, path={}",
          itemFile.getAbsolutePath());
    }
    // remove file in assessment items cache -- see org.olat.ims.qti21.manager.QTI21ServiceImpl -
    // methods afterPropertiesSet(), loadAndResolveAssessmentItem() and persistAssessmentObject():
    Cacher cacher = CoordinatorManager.getInstance().getCoordinator().getCacher();
    cacher.getCache("QTIWorks", "assessmentItems").remove(itemFile);
  }

  @Override
  public boolean exportAssessmentItemToQuestionPool(
      Identity authorIdentity,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      Locale locale,
      Identity exporterIdentity) {
    if (authorIdentity == null || courseEnvironment == null || courseNode == null) {
      return false;
    }
    File itemFile =
        resolveSavedAssessmentItemFile(courseEnvironment, courseNode, authorIdentity.getKey());
    ResolvedAssessmentItem resolvedAssessmentItem = getSavedAssessmentItem(itemFile);
    if (resolvedAssessmentItem == null) {
      // saved assessment item does not exist, do not export
      return false;
    }
    AssessmentItem assessmentItem =
        resolvedAssessmentItem.getItemLookup().getRootNodeHolder().getRootNode();
    ManifestMetadataBuilder manifestMetadataBuilder = new ManifestMetadataBuilder();
    log.debug(
        "export assessment item to question pool, assessmentItem={}", assessmentItem.getSystemId());
    QuestionItem exportedQuestionItem =
        qti21QPoolServiceProvider.importAssessmentItemRef(
            authorIdentity, assessmentItem, itemFile, manifestMetadataBuilder, locale);
    if (exporterIdentity != null) {
      qPoolService.addAuthors(List.of(exporterIdentity), List.of(exportedQuestionItem));
    }
    return exportedQuestionItem != null;
  }

  @Override
  public File getQTIQuestionFolder(
      CourseEnvironment courseEnvironment, QuestionCreationCourseNode courseNode) {
    Path path =
        Paths.get(
            FolderConfig.getCanonicalRoot(),
            courseEnvironment.getCourseBaseContainer().getRelPath(),
            QUESTION_CREATION_FOLDER_NAME,
            courseNode.getIdent());
    File qtiQuestionFolder = path.toFile();
    if (!qtiQuestionFolder.exists()) {
      boolean created = qtiQuestionFolder.mkdirs();
      if (!created) {
        log.warn("Could not create folder, path={}", qtiQuestionFolder.getAbsolutePath());
      }
    }
    return qtiQuestionFolder;
  }

  /** Implemented as in @see {@link QTI21Service} getAssessmentResult() method */
  @Override
  public AssessmentResult getAssessmentResult(
      File assessmentResultFile, ResourceLocator inputResourceLocator) {
    URI assessmentResultUri = assessmentResultFile.toURI();
    QtiObjectReader qtiObjectReader =
        qtiService.qtiXmlReader().createQtiObjectReader(inputResourceLocator, false, false);
    try {
      QtiObjectReadResult<AssessmentResult> result =
          qtiObjectReader.lookupRootNode(assessmentResultUri, AssessmentResult.class);
      return result.getRootNode();
    } catch (XmlResourceNotFoundException | QtiXmlInterpretationException | ClassCastException e) {
      log.error("", e);
      return null;
    }
  }

  @Override
  public File getAssessmentResultFile(
      long reviewerIdentityKey,
      long creatorIdentityKey,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment) {
    File questionCreationFolder = getQTIQuestionFolder(courseEnvironment, courseNode);
    String assessmentResultFilename =
        reviewerIdentityKey + "_" + creatorIdentityKey + "_" + ASSESSMENT_RESULT_FILENAME_SUFFIX;
    return new File(questionCreationFolder, assessmentResultFilename);
  }

  @Override
  public boolean assessmentFileExists(
      long reviewerIdentityKey,
      PeerReviewTask peerReviewTask,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment) {
    long creatorIdentityKey = peerReviewTask.getTask().getCreator().getId();
    File assessmentResultFile =
        getAssessmentResultFile(
            reviewerIdentityKey, creatorIdentityKey, courseNode, courseEnvironment);
    return assessmentResultFile.exists();
  }
}
