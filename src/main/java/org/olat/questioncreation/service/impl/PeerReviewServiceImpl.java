/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service.impl;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import ch.uzh.olat.lms.openolat.data.repository.SimpleIdentityRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.olat.core.id.Identity;
import org.olat.course.ICourse;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewEvaluation;
import org.olat.questioncreation.data.entity.PeerReviewQuestion;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.data.repository.PeerReviewEvaluationRepository;
import org.olat.questioncreation.data.repository.PeerReviewQuestionRepository;
import org.olat.questioncreation.data.repository.PeerReviewTaskRepository;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.PeerReviewService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Service("questionCreationPeerReviewServiceImpl")
public class PeerReviewServiceImpl implements PeerReviewService {

  private final PeerReviewTaskRepository peerReviewTaskRepository;
  private final PeerReviewQuestionRepository peerReviewQuestionRepository;
  private final PeerReviewEvaluationRepository peerReviewEvaluationRepository;
  private final SimpleIdentityRepository simpleIdentityRepository;

  public PeerReviewServiceImpl(
      PeerReviewTaskRepository peerReviewTaskRepository,
      PeerReviewQuestionRepository peerReviewQuestionRepository,
      PeerReviewEvaluationRepository peerReviewEvaluationRepository,
      SimpleIdentityRepository simpleIdentityRepository) {
    this.peerReviewTaskRepository = peerReviewTaskRepository;
    this.peerReviewQuestionRepository = peerReviewQuestionRepository;
    this.peerReviewEvaluationRepository = peerReviewEvaluationRepository;
    this.simpleIdentityRepository = simpleIdentityRepository;
  }

  @Override
  public List<PeerReviewQuestion> findPeerReviewQuestionsOfCourseNode(String repoCourseNodeIdent) {
    return peerReviewQuestionRepository.findByRepoCourseNodeIdentOrderByQuestionNumber(
        repoCourseNodeIdent);
  }

  @Override
  public void copyPeerReviewQuestions(
      QuestionCreationCourseNode sourceNode,
      ICourse sourceCourse,
      QuestionCreationCourseNode targetNode,
      ICourse targetCourse) {
    String sourceRepoCourseNodeIdent =
        CourseNodeIdentifier.getIdentifierFor(sourceCourse.getCourseEnvironment(), sourceNode);
    String targetRepoCourseNodeIdent =
        CourseNodeIdentifier.getIdentifierFor(targetCourse.getCourseEnvironment(), targetNode);
    List<PeerReviewQuestion> peerReviewQuestionsToCopy =
        findPeerReviewQuestionsOfCourseNode(sourceRepoCourseNodeIdent);
    for (PeerReviewQuestion peerReviewQuestionToCopy : peerReviewQuestionsToCopy) {
      PeerReviewQuestion newPeerReviewQuestion = new PeerReviewQuestion();
      newPeerReviewQuestion.setRepoCourseNodeIdent(targetRepoCourseNodeIdent);
      newPeerReviewQuestion.setCreator(peerReviewQuestionToCopy.getCreator());
      newPeerReviewQuestion.setQuestionNumber(peerReviewQuestionToCopy.getQuestionNumber());
      newPeerReviewQuestion.setQuestionText(peerReviewQuestionToCopy.getQuestionText());
      peerReviewQuestionRepository.save(newPeerReviewQuestion);
    }
  }

  @Override
  @Transactional
  public void resetAndSavePeerReviewQuestionsOfCourseNode(
      Identity creatorIdentity,
      String repoCourseNodeIdent,
      List<PeerReviewQuestion> peerReviewQuestions) {
    peerReviewQuestionRepository.deleteAll(
        peerReviewQuestionRepository.findByRepoCourseNodeIdentOrderByQuestionNumber(
            repoCourseNodeIdent));

    SimpleIdentity simpleIdentity =
        simpleIdentityRepository.findById(creatorIdentity.getKey()).orElseThrow();
    int questionNr = 0;
    for (PeerReviewQuestion peerReviewQuestion : peerReviewQuestions) {
      peerReviewQuestion.setRepoCourseNodeIdent(repoCourseNodeIdent);
      peerReviewQuestion.setCreator(simpleIdentity);
      peerReviewQuestion.setQuestionNumber(++questionNr);
    }
    peerReviewQuestionRepository.saveAll(peerReviewQuestions);
  }

  @Override
  @Transactional
  public List<PeerReviewTask> assignAndSavePeerReviews(
      List<SubmissionTask> submittedTasks, int numberOfPeerReviews) {
    Collections.shuffle(submittedTasks);

    Date peerReviewAssignDate = new Date();
    List<PeerReviewTask> peerReviewTasks = new ArrayList<>();

    for (int taskIndex = 0; taskIndex < submittedTasks.size(); taskIndex++) {
      SubmissionTask taskToAssignPeerReviews = submittedTasks.get(taskIndex);
      for (int nextTaskIndex = 0; nextTaskIndex < numberOfPeerReviews; nextTaskIndex++) {
        SubmissionTask taskToBeAssigned =
            submittedTasks.get((taskIndex + 1 + nextTaskIndex) % submittedTasks.size());
        peerReviewTasks.add(
            new PeerReviewTask(
                taskToAssignPeerReviews, taskToBeAssigned.getCreator(), peerReviewAssignDate));
      }
    }

    peerReviewTaskRepository.saveAll(peerReviewTasks);
    return peerReviewTasks;
  }

  @Override
  public PeerReviewTask updatePeerReviewTask(PeerReviewTask peerReviewTask) {
    return peerReviewTaskRepository.save(peerReviewTask);
  }

  @Override
  public List<PeerReviewTask> getPeerReviewTasks(String repoCourseNodeIdent) {
    return peerReviewTaskRepository.findBySubmissionTaskRepoCourseNodeIdent(repoCourseNodeIdent);
  }

  @Override
  public List<PeerReviewTask> getPeerReviewTasksByRepoCourseNodeIdentAndReviewer(
      String repoCourseNodeIdent, long reviewerIdentityKey) {
    return peerReviewTaskRepository.findBySubmissionTaskRepoCourseNodeIdentAndReviewerId(
        repoCourseNodeIdent, reviewerIdentityKey);
  }

  @Override
  public List<PeerReviewTask> getPeerReviewTasksBySubmissionTask(SubmissionTask submissionTask) {
    return peerReviewTaskRepository.findBySubmissionTaskOrderByReviewerId(submissionTask);
  }

  @Override
  public boolean arePeerReviewsAlreadyAssigned(String repoCourseNodeIdent) {
    return !getPeerReviewTasks(repoCourseNodeIdent).isEmpty();
  }

  @Override
  public List<PeerReviewEvaluation> findEvaluationsOfPeerReviewTask(PeerReviewTask peerReviewTask) {
    return peerReviewEvaluationRepository.findByPeerReviewTask(peerReviewTask);
  }

  @Override
  public void saveEvaluationsOfPeerReviewTask(List<PeerReviewEvaluation> peerReviewEvaluations) {
    peerReviewEvaluationRepository.saveAll(peerReviewEvaluations);
  }

  @Override
  public boolean areEvaluationsOfAllPeerReviewTasksOfReviewerComplete(
      String repoCourseNodeIdent, long reviewerIdentityKey) {
    List<PeerReviewTask> peerReviewTasksByRepoCourseNodeIdentAndReviewer =
        getPeerReviewTasksByRepoCourseNodeIdentAndReviewer(
            repoCourseNodeIdent, reviewerIdentityKey);
    for (PeerReviewTask peerReviewTask : peerReviewTasksByRepoCourseNodeIdentAndReviewer) {
      if (!isEvaluationOfPeerReviewTaskComplete(peerReviewTask)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isEvaluationOfPeerReviewTaskComplete(PeerReviewTask peerReviewTask) {
    List<PeerReviewEvaluation> peerReviewEvaluations =
        findEvaluationsOfPeerReviewTask(peerReviewTask);
    if (peerReviewEvaluations.isEmpty()) {
      return false;
    }
    for (PeerReviewEvaluation peerReviewEvaluation : peerReviewEvaluations) {
      if (peerReviewEvaluation.getLikertScaleValue() == 0) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean arePeerReviewTasksFinallySubmitted(
      String repoCourseNodeIdent, long reviewerIdentityKey) {
    return getPeerReviewTasksByRepoCourseNodeIdentAndReviewer(
            repoCourseNodeIdent, reviewerIdentityKey)
        .stream()
        .allMatch(peerReviewTask -> peerReviewTask.getFinalReviewSubmitDate() != null);
  }
}
