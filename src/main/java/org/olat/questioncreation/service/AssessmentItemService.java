/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service;

import java.io.File;
import java.util.Locale;
import org.olat.core.id.Identity;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import uk.ac.ed.ph.jqtiplus.node.result.AssessmentResult;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;
import uk.ac.ed.ph.jqtiplus.xmlutils.locators.ResourceLocator;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
public interface AssessmentItemService {

  boolean hasAnySavedAssessmentItem(
      CourseEnvironment courseEnvironment, QuestionCreationCourseNode courseNode);

  ResolvedAssessmentItem getSavedAssessmentItem(
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      long creatorIdentityKey);

  ResolvedAssessmentItem getSavedAssessmentItem(File itemFile);

  File resolveSavedAssessmentItemFile(
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      long creatorIdentityKey);

  void deleteSavedAssessmentItem(
      CourseEnvironment courseEnvironment, QuestionCreationCourseNode courseNode, long identityKey);

  boolean exportAssessmentItemToQuestionPool(
      Identity authorIdentity,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      Locale locale,
      Identity exporterIdentity);

  File getQTIQuestionFolder(
      CourseEnvironment courseEnvironment, QuestionCreationCourseNode courseNode);

  AssessmentResult getAssessmentResult(
      File assessmentResultFile, ResourceLocator inputResourceLocator);

  File getAssessmentResultFile(
      long reviewerIdentityKey,
      long creatorIdentityKey,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment);

  boolean assessmentFileExists(
      long reviewerIdentityKey,
      PeerReviewTask peerReviewTask,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment);
}
