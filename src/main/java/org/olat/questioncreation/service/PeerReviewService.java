/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service;

import java.util.List;
import org.olat.core.id.Identity;
import org.olat.course.ICourse;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewEvaluation;
import org.olat.questioncreation.data.entity.PeerReviewQuestion;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.data.entity.SubmissionTask;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public interface PeerReviewService {

  List<PeerReviewQuestion> findPeerReviewQuestionsOfCourseNode(String repoCourseNodeIdent);

  void copyPeerReviewQuestions(
      QuestionCreationCourseNode sourceNode,
      ICourse sourceCourse,
      QuestionCreationCourseNode targetNode,
      ICourse targetCourse);

  void resetAndSavePeerReviewQuestionsOfCourseNode(
      Identity creatorIdentity,
      String repoCourseNodeIdent,
      List<PeerReviewQuestion> peerReviewQuestions);

  List<PeerReviewTask> assignAndSavePeerReviews(
      List<SubmissionTask> submittedTasks, int numberOfPeerReviews);

  PeerReviewTask updatePeerReviewTask(PeerReviewTask peerReviewTask);

  List<PeerReviewTask> getPeerReviewTasks(String repoCourseNodeIdent);

  List<PeerReviewTask> getPeerReviewTasksByRepoCourseNodeIdentAndReviewer(
      String repoCourseNodeIdent, long reviewerIdentityKey);

  List<PeerReviewTask> getPeerReviewTasksBySubmissionTask(SubmissionTask submissionTask);

  boolean arePeerReviewsAlreadyAssigned(String repoCourseNodeIdent);

  List<PeerReviewEvaluation> findEvaluationsOfPeerReviewTask(PeerReviewTask peerReviewTask);

  void saveEvaluationsOfPeerReviewTask(List<PeerReviewEvaluation> peerReviewEvaluations);

  boolean areEvaluationsOfAllPeerReviewTasksOfReviewerComplete(
      String repoCourseNodeIdent, long reviewerIdentityKey);

  boolean isEvaluationOfPeerReviewTaskComplete(PeerReviewTask peerReviewTask);

  boolean arePeerReviewTasksFinallySubmitted(String repoCourseNodeIdent, long reviewerIdentityKey);
}
