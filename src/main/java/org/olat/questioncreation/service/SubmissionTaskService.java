/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.olat.questioncreation.data.entity.SubmissionTask;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
public interface SubmissionTaskService {

  void save(
      long creatorIdentityKey,
      String repoCourseNodeIdent,
      Integer submissionTaskIndex,
      Date finalSubmitDate,
      boolean isExportToPoolAllowed);

  List<SubmissionTask> findByRepoCourseNodeIdent(String repoCourseNodeIdent);

  Optional<SubmissionTask> findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
      long identityKey, String repoCourseNodeIdent, Integer submissionTaskIndex);
}
