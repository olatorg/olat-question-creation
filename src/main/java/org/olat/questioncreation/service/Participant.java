/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service;

import java.util.Locale;
import lombok.Getter;
import org.olat.core.gui.translator.Translator;
import org.olat.core.id.Identity;
import org.olat.core.id.UserConstants;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Getter
public class Participant {

  private final String username;
  private final String lastname;
  private final String firstname;
  private final String matriculationNr;
  private final Identity identity;
  private final QuestionStatus questionStatus;
  private final QTI21QuestionType questionType;
  private final boolean isExportToPoolAllowedForTask;

  public Participant(
      Identity identity,
      QuestionStatus questionStatus,
      QTI21QuestionType questionType,
      boolean isExportToPoolAllowedForTask) {
    username = identity.getUser().getNickName();
    lastname = identity.getUser().getLastName();
    firstname = identity.getUser().getFirstName();
    matriculationNr =
        identity.getUser().getProperty(UserConstants.INSTITUTIONALUSERIDENTIFIER, null);
    this.identity = identity;
    this.questionStatus = questionStatus;
    this.questionType = questionType;
    this.isExportToPoolAllowedForTask = isExportToPoolAllowedForTask;
  }

  public String getTranslatedQuestionStatus(Locale locale) {
    return QuestionCreationUIHelper.getTranslatedStatus(getQuestionStatus(), locale);
  }

  public String getTranslatedQuestionType(Locale locale) {
    if (questionType != null) {
      return QuestionCreationUIHelper.getLabelForQTIQuestion(questionType, locale);
    }
    return "-";
  }

  public Object getTranslatedExportText(Translator translator) {
    if (isStatusSubmissionDoneAndAfter() && isExportToPoolAllowedForTask) {
      return translator.translate("run.coach.participants.table.row.export");
    } else {
      return "";
    }
  }

  public boolean isSearchMatching(String searchString) {
    if (searchString == null || searchString.isEmpty()) {
      return true;
    }
    return isValueMatching(searchString, username)
        || isValueMatching(searchString, lastname)
        || isValueMatching(searchString, firstname)
        || isValueMatching(searchString, matriculationNr);
  }

  private static boolean isValueMatching(String searchString, String value) {
    return value != null && value.toLowerCase().contains(searchString.toLowerCase());
  }

  public boolean isStatusSubmissionDoneAndAfter() {
    return questionStatus != QuestionStatus.UNPUBLISHED
        && questionStatus != QuestionStatus.SUBMISSION_IN_PROGRESS
        && questionStatus != QuestionStatus.SUBMISSION_DEADLINE_MISSED;
  }

  @Getter
  public enum QuestionStatus {
    UNPUBLISHED("run.coach.participants.question.status.unpublished"),
    SUBMISSION_IN_PROGRESS("run.coach.participants.question.status.submission"),
    SUBMISSION_DONE("run.coach.participants.question.status.submission.done"),
    SUBMISSION_DEADLINE_MISSED("run.coach.participants.question.status.submission.missed"),
    PEER_REVIEW_ASSIGNED("run.coach.participants.question.status.peerreview.assigned"),
    PEER_REVIEW_DONE("run.coach.participants.question.status.peerreview.done"),
    PEER_REVIEW_DEADLINE_MISSED("run.coach.participants.question.status.peerreview.missed");

    private final String i18Key;

    QuestionStatus(String i18Key) {
      this.i18Key = i18Key;
    }
  }
}
