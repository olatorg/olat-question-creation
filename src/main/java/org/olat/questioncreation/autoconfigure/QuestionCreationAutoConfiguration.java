/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.autoconfigure;

import jakarta.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.extensions.action.GenericActionExtension;
import org.olat.core.gui.control.creator.AutoCreator;
import org.olat.core.util.coordinate.CoordinatorManager;
import org.olat.questioncreation.QuestionCreationAssessmentHandler;
import org.olat.questioncreation.QuestionCreationCourseNodeConfiguration;
import org.olat.questioncreation.QuestionCreationLearningPathNodeHandler;
import org.olat.questioncreation.QuestionCreationModule;
import org.olat.questioncreation.ui.admin.QuestionCreationAdminMainController;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@AutoConfiguration
@EnableConfigurationProperties(QuestionCreationProperties.class)
@ConditionalOnProperty(
    prefix = "olat.questioncreation",
    name = "enabled",
    havingValue = "true",
    matchIfMissing = true)
@ComponentScan("org.olat.questioncreation")
@EnableJpaRepositories("org.olat.questioncreation.data.repository")
@Slf4j
public class QuestionCreationAutoConfiguration {

  @PostConstruct
  public void init() {
    log.info("Question Creation module is enabled");
  }

  @Bean
  public QuestionCreationModule questionCreationModule(CoordinatorManager coordinatorManager) {
    return new QuestionCreationModule(coordinatorManager);
  }

  @Bean
  public QuestionCreationCourseNodeConfiguration questionCreationCourseNodeConfiguration(
      QuestionCreationModule questionCreationModule) {
    return new QuestionCreationCourseNodeConfiguration(questionCreationModule);
  }

  @Bean
  public QuestionCreationLearningPathNodeHandler questionCreationLearningPathNodeHandler() {
    return new QuestionCreationLearningPathNodeHandler();
  }

  @Bean
  public QuestionCreationAssessmentHandler questionCreationAssessmentHandler() {
    return new QuestionCreationAssessmentHandler();
  }

  @Bean(initMethod = "initExtensionPoints")
  public GenericActionExtension uzhQuestionCreationAdminExtension() {
    GenericActionExtension extension = new GenericActionExtension();
    List<String> extensionPoints = new ArrayList<>();
    extensionPoints.add("org.olat.admin.SystemAdminMainController");
    extension.setExtensionPoints(extensionPoints);
    AutoCreator controller = new AutoCreator();
    controller.setClassName(QuestionCreationAdminMainController.class.getName());
    extension.setActionController(controller);
    extension.setParentTreeNodeIdentifier("uzhParent");
    extension.setNavigationKey("uzhQuestionCreation");
    extension.setTranslationPackage("org.olat.questioncreation.ui.admin");
    extension.setI18nActionKey("menu.uzh.questioncreation");
    extension.setI18nDescriptionKey("menu.uzh.questioncreation.alt");
    return extension;
  }
}
