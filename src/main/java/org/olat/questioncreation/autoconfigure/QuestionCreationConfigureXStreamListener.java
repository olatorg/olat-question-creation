/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.autoconfigure;

import lombok.extern.slf4j.Slf4j;
import org.olat.core.util.xml.XStreamHelper;
import org.olat.course.CourseXStreamAliases;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.springframework.boot.context.event.ApplicationContextInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
public class QuestionCreationConfigureXStreamListener
    implements ApplicationListener<ApplicationContextInitializedEvent> {

  private static final Class<?>[] ALLOWED_TYPES = {QuestionCreationCourseNode.class};
  private static final String[] ALIASES = {"QuestionCreationCourseNode"};

  @Override
  public void onApplicationEvent(@NonNull ApplicationContextInitializedEvent event) {
    log.info("Configure XStream for Question Creation course node");
    XStreamHelper.getCloneXStream().allowTypes(ALLOWED_TYPES);
    for (String alias : ALIASES) {
      CourseXStreamAliases.getReadCourseXStream().alias(alias, QuestionCreationCourseNode.class);
      CourseXStreamAliases.getWriteCourseXStream().alias(alias, QuestionCreationCourseNode.class);
    }
  }
}
