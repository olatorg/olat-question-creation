/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.CoreSpringFactory;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.stack.BreadcrumbPanel;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.generic.messages.MessageUIFactory;
import org.olat.core.gui.control.generic.tabbable.TabbableController;
import org.olat.core.gui.translator.Translator;
import org.olat.core.id.Identity;
import org.olat.core.id.Organisation;
import org.olat.core.id.Roles;
import org.olat.core.util.Util;
import org.olat.core.util.ValidationStatus;
import org.olat.course.ICourse;
import org.olat.course.editor.ConditionAccessEditConfig;
import org.olat.course.editor.CourseEditorEnv;
import org.olat.course.editor.NodeEditController;
import org.olat.course.editor.StatusDescription;
import org.olat.course.editor.importnodes.ImportSettings;
import org.olat.course.export.CourseEnvironmentMapper;
import org.olat.course.nodes.AbstractAccessableCourseNode;
import org.olat.course.nodes.CourseNode;
import org.olat.course.nodes.GenericCourseNode;
import org.olat.course.nodes.StatusDescriptionHelper;
import org.olat.course.nodes.TitledWrapperHelper;
import org.olat.course.run.navigation.NodeRunConstructionResult;
import org.olat.course.run.userview.CourseNodeSecurityCallback;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.course.run.userview.VisibilityFilter;
import org.olat.modules.ModuleConfiguration;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.service.QuestionCreationDeletionService;
import org.olat.questioncreation.ui.edit.QuestionCreationEditController;
import org.olat.questioncreation.ui.edit.QuestionCreationPeerReviewAssignmentEditController;
import org.olat.questioncreation.ui.run.QuestionCreationRunController;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntryImportExportLinkEnum;
import org.olat.repository.ui.author.copy.wizard.CopyCourseContext;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
public class QuestionCreationCourseNode extends AbstractAccessableCourseNode {

  public static final String CFG_KEY_TASK_TITLE = "questioncreation.task.title";
  public static final String CFG_KEY_TASK_DESCRIPTION = "questioncreation.task.description";
  public static final String CFG_KEY_TASK_DATE_START = "questioncreation.task.date.start";
  public static final String CFG_KEY_TASK_DATE_DUE = "questioncreation.task.date.due";
  public static final String CFG_KEY_TASK_QTI_TYPES = "questioncreation.task.qti.types";

  public static final String CFG_KEY_PEERREVIEW_DATE_DUE = "questioncreation.peerreview.date.due";
  public static final String CFG_KEY_PEERREVIEW_QUESTIONS_SAVED =
      "questioncreation.peerreview.questions.saved";
  public static final String CFG_KEY_PEERREVIEW_ASSIGNMENT_NR =
      "questioncreation.peerreview.assignment.nr";

  static final String TYPE = "questionCreation";
  static final String ICON = "o_icon_qual_preview";

  protected QuestionCreationCourseNode() {
    super(TYPE);
  }

  @Override
  public NodeRunConstructionResult createNodeRunConstructionResult(
      UserRequest userRequest,
      WindowControl windowControl,
      UserCourseEnvironment userCourseEnvironment,
      CourseNodeSecurityCallback securityCallback,
      String nodecmd,
      VisibilityFilter visibilityFilter) {
    Controller controller;
    Translator translator =
        Util.createPackageTranslator(GenericCourseNode.class, userRequest.getLocale());
    if (userCourseEnvironment.isCourseReadOnly()) {
      String title = translator.translate("freezenoaccess.title");
      String message = translator.translate("freezenoaccess.message");
      controller = MessageUIFactory.createInfoMessage(userRequest, windowControl, title, message);
    } else {
      Roles roles = userRequest.getUserSession().getRoles();
      if (roles.isGuestOnly()) {
        String title = translator.translate("guestnoaccess.title");
        String message = translator.translate("guestnoaccess.message");
        controller = MessageUIFactory.createInfoMessage(userRequest, windowControl, title, message);
      } else {
        controller =
            new QuestionCreationRunController(
                userRequest, windowControl, userCourseEnvironment, this);
      }
    }
    return new NodeRunConstructionResult(
        TitledWrapperHelper.getWrapper(userRequest, windowControl, controller, this, ICON));
  }

  @Override
  public TabbableController createEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      BreadcrumbPanel stackPanel,
      ICourse course,
      UserCourseEnvironment userCourseEnvironment) {
    QuestionCreationEditController controller =
        new QuestionCreationEditController(userRequest, windowControl, userCourseEnvironment, this);
    CourseNode chosenNode =
        course
            .getEditorTreeModel()
            .getCourseNode(userCourseEnvironment.getCourseEditorEnv().getCurrentCourseNodeId());
    return new NodeEditController(
        userRequest,
        windowControl,
        stackPanel,
        course,
        chosenNode,
        userCourseEnvironment,
        controller);
  }

  @Override
  public StatusDescription isConfigValid() {
    if (oneClickStatusCache == null) {
      oneClickStatusCache = StatusDescriptionHelper.sort(validateConfiguration());
    }
    return oneClickStatusCache.length > 0 ? oneClickStatusCache[0] : StatusDescription.NOERROR;
  }

  @Override
  public StatusDescription[] isConfigValid(CourseEditorEnv cev) {
    oneClickStatusCache = StatusDescriptionHelper.sort(validateConfiguration());
    return oneClickStatusCache;
  }

  public int getNumberOfPeerReviews() {
    return getModuleConfiguration()
        .getIntegerSafe(
            CFG_KEY_PEERREVIEW_ASSIGNMENT_NR,
            Integer.parseInt(
                QuestionCreationPeerReviewAssignmentEditController
                    .ASSESSMENT_NUMBER_DEFAULT_SELECTION));
  }

  private List<StatusDescription> validateConfiguration() {
    List<StatusDescription> statusDescriptions = new ArrayList<>();

    if (!validateTaskConfiguration()) {
      statusDescriptions.add(
          createStatusErrorDescription(
              "questioncreation.error.task.configuration",
              QuestionCreationEditController.PANE_TAB_QUESTION_CREATION_TASK_CONFIG));
    }

    validatePeerReviewConfiguration(statusDescriptions);

    return statusDescriptions;
  }

  private boolean validateTaskConfiguration() {
    ModuleConfiguration moduleConfiguration = getModuleConfiguration();

    if (moduleConfiguration.getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_TITLE) == null) {
      return false;
    }
    if (moduleConfiguration.getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION)
        == null) {
      return false;
    }

    Date taskDueDate =
        moduleConfiguration.getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
    if (taskDueDate == null) {
      return false;
    }
    Date taskStartDate =
        moduleConfiguration.getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START);
    if (taskStartDate != null && !taskDueDate.after(taskStartDate)) {
      return false;
    }

    List<String> qtiTypes =
        moduleConfiguration.getList(
            QuestionCreationCourseNode.CFG_KEY_TASK_QTI_TYPES, String.class);
    return !qtiTypes.isEmpty();
  }

  private void validatePeerReviewConfiguration(List<StatusDescription> statusDescriptions) {
    if (getModuleConfiguration().getDateValue(CFG_KEY_PEERREVIEW_DATE_DUE) == null) {
      statusDescriptions.add(
          createStatusErrorDescription(
              "questioncreation.error.peerreview.missing.dueDate",
              QuestionCreationEditController.PANE_TAB_QUESTION_CREATION_PEER_REVIEW));
    }
    if (!getModuleConfiguration().getBooleanSafe(CFG_KEY_PEERREVIEW_QUESTIONS_SAVED)) {
      statusDescriptions.add(
          createStatusErrorDescription(
              "questioncreation.error.peerreview.missing.question",
              QuestionCreationEditController.PANE_TAB_QUESTION_CREATION_PEER_REVIEW));
    }
  }

  private StatusDescription createStatusErrorDescription(String key, String tab) {
    String[] params = new String[] {getShortTitle()};
    StatusDescription statusDescription =
        new StatusDescription(
            ValidationStatus.ERROR,
            key,
            key,
            params,
            QuestionCreationCourseNode.class.getPackage().getName());
    statusDescription.setDescriptionForUnit(getIdent());
    statusDescription.setActivateableViewIdentifier(tab);
    return statusDescription;
  }

  @Override
  public RepositoryEntry getReferencedRepositoryEntry() {
    return null;
  }

  @Override
  public boolean needsReferenceToARepositoryEntry() {
    return false;
  }

  @Override
  public ConditionAccessEditConfig getAccessEditConfig() {
    return ConditionAccessEditConfig.regular(true);
  }

  @Override
  public void cleanupOnDelete(ICourse course) {
    super.cleanupOnDelete(course);
    QuestionCreationDeletionService questionCreationDeletionService =
        CoreSpringFactory.getBean(QuestionCreationDeletionService.class);
    questionCreationDeletionService.deleteAllQuestionCreationCourseNodeData(course, this);
  }

  @Override
  public CourseNode createInstanceForCopy(boolean isNewTitle, ICourse course, Identity author) {
    // copy course node within a course
    log.debug("createInstanceForCopy: start");
    QuestionCreationCourseNode targetNode =
        (QuestionCreationCourseNode) super.createInstanceForCopy(isNewTitle, course, author);
    copyQuestionCreationData(this, course, targetNode, course);
    return targetNode;
  }

  private void copyQuestionCreationData(
      QuestionCreationCourseNode sourceNode,
      ICourse sourceCourse,
      QuestionCreationCourseNode targetNode,
      ICourse targetCourse) {
    log.debug("copyQuestionCreationData: start");
    PeerReviewService peerReviewService = CoreSpringFactory.getImpl(PeerReviewService.class);
    peerReviewService.copyPeerReviewQuestions(sourceNode, sourceCourse, targetNode, targetCourse);
  }

  @Override
  public void postCopy(
      CourseEnvironmentMapper envMapper,
      Processing processType,
      ICourse course,
      ICourse sourceCourse,
      CopyCourseContext context) {
    // copy a course
    log.debug("postCopy: start");
    super.postCopy(envMapper, processType, course, sourceCourse, context);
    log.debug("postCopy: processType={}", processType);
    if (processType.equals(Processing.editor)) {
      log.debug("postCopy: copyQuestionCreationData");
      copyQuestionCreationData(this, sourceCourse, this, course);
    }
  }

  @Override
  public void exportNode(
      File fExportDirectory, ICourse course, RepositoryEntryImportExportLinkEnum withReferences) {
    log.debug("exportNode: start");
    super.exportNode(fExportDirectory, course, withReferences);
  }

  @Override
  public void importNode(
      File importDirectory,
      ICourse course,
      Identity owner,
      Organisation organisation,
      Locale locale,
      RepositoryEntryImportExportLinkEnum withReferences) {
    // import a course
    log.debug("importNode: start");
    super.importNode(importDirectory, course, owner, organisation, locale, withReferences);
  }

  @Override
  public void postImport(
      File importDirectory,
      ICourse course,
      CourseEnvironmentMapper envMapper,
      Processing processType) {
    // post import a course
    log.debug("postImport: start");
    super.postImport(importDirectory, course, envMapper, processType);
  }

  @Override
  public void postImportCourseNodes(
      ICourse course,
      CourseNode sourceCourseNode,
      ICourse sourceCourse,
      ImportSettings settings,
      CourseEnvironmentMapper envMapper) {
    // import of specific course nodes
    log.debug("postImportCourseNodes: start");
    super.postImportCourseNodes(course, sourceCourseNode, sourceCourse, settings, envMapper);
    copyQuestionCreationData(
        (QuestionCreationCourseNode) sourceCourseNode, sourceCourse, this, course);
  }
}
