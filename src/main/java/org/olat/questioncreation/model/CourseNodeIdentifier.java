/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.model;

import org.olat.course.nodes.CourseNode;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.repository.RepositoryEntry;

/**
 * @author Mateja Culjak
 * @since 1.1
 */
public class CourseNodeIdentifier {

  public static String getIdentifierFor(
      CourseEnvironment courseEnvironment, CourseNode courseNode) {
    RepositoryEntry repoEntry = courseEnvironment.getCourseGroupManager().getCourseEntry();
    return buildIdentifierString(courseNode, repoEntry);
  }

  private static String buildIdentifierString(CourseNode courseNode, RepositoryEntry repoEntry) {
    return repoEntry.getResourceableId() + "-" + courseNode.getIdent();
  }
}
