/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation;

import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.olat.core.gui.translator.Translator;
import org.olat.core.util.Util;
import org.olat.course.nodes.AbstractCourseNodeConfiguration;
import org.olat.course.nodes.CourseNode;
import org.olat.course.nodes.CourseNodeGroup;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@RequiredArgsConstructor
public class QuestionCreationCourseNodeConfiguration extends AbstractCourseNodeConfiguration {

  private final QuestionCreationModule questionCreationModule;

  @Override
  public String getAlias() {
    return QuestionCreationCourseNode.TYPE;
  }

  @Override
  public String getGroup() {
    return CourseNodeGroup.assessment.name();
  }

  @Override
  public CourseNode getInstance() {
    return new QuestionCreationCourseNode();
  }

  @Override
  public String getLinkText(Locale locale) {
    Translator translator =
        Util.createPackageTranslator(QuestionCreationCourseNodeConfiguration.class, locale);
    return translator.translate("questioncreation.edit.title");
  }

  @Override
  public String getIconCSSClass() {
    return QuestionCreationCourseNode.ICON;
  }

  @Override
  public boolean isEnabled() {
    return questionCreationModule.isEnabled();
  }
}
