/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.dao;

import java.util.List;
import org.olat.core.commons.persistence.DB;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.springframework.stereotype.Component;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Component
public class SubmissionTaskDao {

  private final DB dbInstance;

  public SubmissionTaskDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public List<Long> findSubmissionTaskIdByRepoCourseNodeIdent(String repoCourseNodeIdent) {
    return dbInstance
        .getCurrentEntityManager()
        .createNamedQuery(
            SubmissionTask.FIND_SUBMISSION_TASK_ID_BY_REPO_COURSE_NODE_IDENT, Long.class)
        .setParameter("repoCourseNodeIdent", repoCourseNodeIdent)
        .getResultList();
  }

  public int deleteByRepoCourseNodeIdent(String repoCourseNodeIdent) {
    return dbInstance
        .getCurrentEntityManager()
        .createNamedQuery(SubmissionTask.DELETE_BY_REPO_COURSE_NODE_IDENT)
        .setParameter("repoCourseNodeIdent", repoCourseNodeIdent)
        .executeUpdate();
  }
}
