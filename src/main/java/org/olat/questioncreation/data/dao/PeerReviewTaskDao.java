/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.dao;

import java.util.List;
import org.olat.core.commons.persistence.DB;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.springframework.stereotype.Component;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Component("questionCreationPeerReviewTaskDao")
public class PeerReviewTaskDao {

  private final DB dbInstance;

  public PeerReviewTaskDao(DB dbInstance) {
    this.dbInstance = dbInstance;
  }

  public List<Long> findPeerReviewTaskIdBySubmissionTaskIdIn(List<Long> submissionTaskIds) {
    return dbInstance
        .getCurrentEntityManager()
        .createNamedQuery(
            PeerReviewTask.FIND_PEER_REVIEW_TASK_ID_BY_SUBMISSION_TASK_ID_IN, Long.class)
        .setParameter("submissionTaskIds", submissionTaskIds)
        .getResultList();
  }

  public int deleteBySubmissionTaskIdIn(List<Long> submissionTaskIds) {
    return dbInstance
        .getCurrentEntityManager()
        .createNamedQuery(PeerReviewTask.DELETE_BY_SUBMISSION_TASK_ID_IN)
        .setParameter("submissionTaskIds", submissionTaskIds)
        .executeUpdate();
  }
}
