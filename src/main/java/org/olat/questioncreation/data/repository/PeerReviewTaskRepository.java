/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.repository;

import java.util.List;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Repository("questionCreationPeerReviewTaskRepository")
public interface PeerReviewTaskRepository extends JpaRepository<PeerReviewTask, Long> {

  @Query(
      "SELECT p FROM SubmissionTask s join QuestionCreationPeerReviewTask p on p.task = s WHERE s.repoCourseNodeIdent = ?1")
  List<PeerReviewTask> findBySubmissionTaskRepoCourseNodeIdent(String repoCourseNodeIdent);

  @Query(
      "SELECT p from SubmissionTask s JOIN QuestionCreationPeerReviewTask p ON p.task = s where s.repoCourseNodeIdent = ?1 AND p.reviewer.id = ?2 ORDER BY p.task.id")
  List<PeerReviewTask> findBySubmissionTaskRepoCourseNodeIdentAndReviewerId(
      String repoCourseNodeIdent, long reviewerId);

  @Query("SELECT p FROM QuestionCreationPeerReviewTask p WHERE p.task = ?1 ORDER BY p.reviewer.id")
  List<PeerReviewTask> findBySubmissionTaskOrderByReviewerId(SubmissionTask submissionTask);
}
