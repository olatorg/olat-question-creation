/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.entity;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import jakarta.persistence.*;
import java.util.Date;
import javaslang.Tuple3;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Entity
@Table(name = "submission_task", schema = "qc")
@Getter
@Setter
@NamedQueries({
  @NamedQuery(
      name = SubmissionTask.FIND_SUBMISSION_TASK_ID_BY_REPO_COURSE_NODE_IDENT,
      query =
          "SELECT s.id FROM SubmissionTask s WHERE s.repoCourseNodeIdent = :repoCourseNodeIdent"),
  @NamedQuery(
      name = SubmissionTask.DELETE_BY_REPO_COURSE_NODE_IDENT,
      query = "DELETE FROM SubmissionTask s WHERE s.repoCourseNodeIdent = :repoCourseNodeIdent")
})
public class SubmissionTask extends BaseEntityWithId<Tuple3<String, Long, Integer>> {

  public static final String FIND_SUBMISSION_TASK_ID_BY_REPO_COURSE_NODE_IDENT =
      "SubmissionTask.findSubmissionTaskIdByRepoCourseNodeIdent";
  public static final String DELETE_BY_REPO_COURSE_NODE_IDENT =
      "SubmissionTask.deleteByRepoCourseNodeIdent";

  @Column(name = "repo_course_node_ident", nullable = false)
  private String repoCourseNodeIdent;

  @ManyToOne
  @JoinColumn(name = "fk_creator", nullable = false)
  private SimpleIdentity creator;

  @Column(name = "submission_task_index", nullable = false)
  private Integer submissionTaskIndex;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "final_submit_date", nullable = false)
  private Date finalSubmitDate;

  @Column(name = "is_export_to_pool_allowed")
  private Boolean isExportToPoolAllowed;

  @Override
  public Tuple3<String, Long, Integer> getBusinessKey() {
    return new Tuple3<>(repoCourseNodeIdent, creator.getId(), submissionTaskIndex);
  }
}
