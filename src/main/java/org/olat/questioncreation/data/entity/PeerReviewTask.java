/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.entity;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import jakarta.persistence.*;
import java.util.Date;
import javaslang.Tuple2;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Entity(name = "QuestionCreationPeerReviewTask")
@Table(name = "peer_review_task", schema = "qc")
@Getter
@Setter
@NamedQueries({
  @NamedQuery(
      name = PeerReviewTask.FIND_PEER_REVIEW_TASK_ID_BY_SUBMISSION_TASK_ID_IN,
      query =
          "SELECT p.id FROM QuestionCreationPeerReviewTask p WHERE p.task.id IN (:submissionTaskIds)"),
  @NamedQuery(
      name = PeerReviewTask.DELETE_BY_SUBMISSION_TASK_ID_IN,
      query =
          "DELETE FROM QuestionCreationPeerReviewTask p WHERE p.task.id IN (:submissionTaskIds)")
})
public class PeerReviewTask extends BaseEntityWithId<Tuple2<Long, Long>> {

  public static final String DELETE_BY_SUBMISSION_TASK_ID_IN =
      "PeerReviewTask.deleteBySubmissionTaskIdIn";
  public static final String FIND_PEER_REVIEW_TASK_ID_BY_SUBMISSION_TASK_ID_IN =
      "PeerReviewTask.findPeerReviewTaskIdBySubmissionTaskIdIn";

  @ManyToOne
  @JoinColumn(name = "fk_task", nullable = false)
  private SubmissionTask task;

  @ManyToOne
  @JoinColumn(name = "fk_reviewer", nullable = false)
  private SimpleIdentity reviewer;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "assign_date", nullable = false)
  private Date assignDate;

  @Column(name = "review_reasoning")
  private String reviewReasoning;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "final_review_submit_date")
  private Date finalReviewSubmitDate;

  public PeerReviewTask() {}

  public PeerReviewTask(SubmissionTask task, SimpleIdentity reviewer, Date assignDate) {
    this.task = task;
    this.reviewer = reviewer;
    this.assignDate = assignDate;
  }

  @Override
  public Tuple2<Long, Long> getBusinessKey() {
    return new Tuple2<>(task.getId(), reviewer.getId());
  }
}
