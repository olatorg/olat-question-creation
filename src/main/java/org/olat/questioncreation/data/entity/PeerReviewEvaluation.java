/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.entity;

import jakarta.persistence.*;
import javaslang.Tuple2;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Entity
@Table(name = "peer_review_evaluation", schema = "qc")
@Getter
@Setter
@NamedQueries({
  @NamedQuery(
      name = PeerReviewEvaluation.DELETE_BY_PEER_REVIEW_TASK_ID_IN,
      query =
          "DELETE FROM PeerReviewEvaluation p WHERE p.peerReviewTask.id IN (:peerReviewTaskIds)")
})
public class PeerReviewEvaluation extends BaseEntityWithId<Tuple2<Long, Long>> {

  public static final String DELETE_BY_PEER_REVIEW_TASK_ID_IN =
      "PeerReviewEvaluation.deleteByPeerReviewTaskIdIn";

  @ManyToOne
  @JoinColumn(name = "fk_peer_review_task", nullable = false)
  private PeerReviewTask peerReviewTask;

  @ManyToOne
  @JoinColumn(name = "fk_peer_review_question", nullable = false)
  private PeerReviewQuestion peerReviewQuestion;

  @Column(name = "likert_scale_value", nullable = false)
  private int likertScaleValue;

  @Override
  public Tuple2<Long, Long> getBusinessKey() {
    return new Tuple2<>(peerReviewTask.getId(), peerReviewQuestion.getId());
  }
}
