/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntityWithId<T> extends BaseEntity<T> {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(
      name = "system-uuid",
      strategy = "enhanced-sequence",
      parameters = {
        @org.hibernate.annotations.Parameter(
            name = "sequence_name",
            value = "hibernate_unique_key"),
        @org.hibernate.annotations.Parameter(name = "force_table_use", value = "true"),
        @org.hibernate.annotations.Parameter(name = "optimizer", value = "legacy-hilo"),
        @org.hibernate.annotations.Parameter(name = "value_column", value = "next_hi"),
        @org.hibernate.annotations.Parameter(name = "increment_size", value = "32767"),
        @org.hibernate.annotations.Parameter(name = "initial_value", value = "32767")
      })
  private Long id;
}
