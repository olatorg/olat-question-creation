/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.entity;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import jakarta.persistence.*;
import javaslang.Tuple2;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Entity
@Table(name = "peer_review_question", schema = "qc")
@Getter
@Setter
@NamedQueries({
  @NamedQuery(
      name = PeerReviewQuestion.DELETE_BY_REPO_COURSE_NODE_IDENT,
      query = "DELETE FROM PeerReviewQuestion p WHERE p.repoCourseNodeIdent = :repoCourseNodeIdent")
})
public class PeerReviewQuestion extends BaseEntityWithId<Tuple2<String, Integer>> {

  public static final String DELETE_BY_REPO_COURSE_NODE_IDENT =
      "PeerReviewQuestion.deleteByRepoCourseNodeIdent";

  @Column(name = "repo_course_node_ident", nullable = false)
  private String repoCourseNodeIdent;

  @ManyToOne
  @JoinColumn(name = "fk_creator", nullable = false)
  private SimpleIdentity creator;

  @Column(name = "question_nr", nullable = false)
  private int questionNumber;

  @Column(name = "question_text", nullable = false)
  private String questionText;

  @Override
  public Tuple2<String, Integer> getBusinessKey() {
    return new Tuple2<>(repoCourseNodeIdent, questionNumber);
  }
}
