/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity<T> {

  @Version private int version;

  @NotNull
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "creation_date", nullable = false)
  private Date creationDate;

  @NotNull
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "last_modified", nullable = false)
  private Date lastModified;

  @PrePersist
  public void onPersist() {
    Date now = new Date();
    if (getCreationDate() == null) {
      setCreationDate(now);
    }
    setLastModified(now);
  }

  @PreUpdate
  public void onUpdate() {
    setLastModified(new Date());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!this.getClass().isInstance(obj)) {
      return false;
    }
    BaseEntity<?> other = (BaseEntity<?>) obj;
    return Objects.equals(getBusinessKey(), other.getBusinessKey());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getBusinessKey());
  }

  public abstract T getBusinessKey();
}
