/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.admin;

import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.MultipleSelectionElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.questioncreation.QuestionCreationModule;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
public class QuestionCreationEnabledController extends FormBasicController {
  private static final String ON_KEY = "on";

  private MultipleSelectionElement enabledSelection;

  @Autowired
  @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
  private QuestionCreationModule questionCreationModule;

  public QuestionCreationEnabledController(UserRequest userRequest, WindowControl windowControl) {
    super(userRequest, windowControl);
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller listener, UserRequest userRequest) {
    setFormTitle("questioncreation.admin.title");
    String[] onKeys = new String[] {ON_KEY};
    String[] onValues = new String[] {translate("questioncreation.admin.enable.label")};
    enabledSelection =
        uifactory.addCheckboxesHorizontal(
            "questioncreation.admin.enable.value",
            "questioncreation.admin.enable.value",
            formLayout,
            onKeys,
            onValues);
    enabledSelection.select(ON_KEY, questionCreationModule.isEnabled());
    enabledSelection.addActionListener(FormEvent.ONCHANGE);
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    // empty
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    log.debug("formInnerEvent controller source={}, event={}", source, event);
    if (source == enabledSelection) {
      questionCreationModule.setEnabled(enabledSelection.isAtLeastSelected(1));
    }
  }
}
