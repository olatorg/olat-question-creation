/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.admin;

import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;

/**
 * @author Christian Guretzki
 * @since 1.0
 */
@Slf4j
public class QuestionCreationAdminMainController extends BasicController {

  public QuestionCreationAdminMainController(UserRequest userRequest, WindowControl windowControl) {
    super(userRequest, windowControl);
    VelocityContainer container = createVelocityContainer("admin_main");

    QuestionCreationEnabledController questionCreationEnabledController =
        new QuestionCreationEnabledController(userRequest, windowControl);
    listenTo(questionCreationEnabledController);
    container.put(
        "questionCreationEnabledController",
        questionCreationEnabledController.getInitialComponent());

    LimitedAccessAdminController limitedAccessAdminController =
        new LimitedAccessAdminController(userRequest, windowControl);
    listenTo(limitedAccessAdminController);
    container.put(
        "limitedAccessAdminController", limitedAccessAdminController.getInitialComponent());

    putInitialPanel(container);
  }

  @Override
  protected void event(UserRequest ureq, Controller source, Event event) {
    // empty
  }

  @Override
  protected void event(UserRequest userRequest, Component component, Event event) {
    // empty
  }
}
