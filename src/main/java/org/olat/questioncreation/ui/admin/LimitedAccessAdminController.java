/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.admin;

import lombok.extern.slf4j.Slf4j;
import org.olat.NewControllerFactory;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.elements.MultipleSelectionElement;
import org.olat.core.gui.components.form.flexible.elements.StaticTextElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.generic.closablewrapper.CloseableModalController;
import org.olat.group.BusinessGroup;
import org.olat.group.BusinessGroupService;
import org.olat.group.ui.NewBGController;
import org.olat.questioncreation.QuestionCreationModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

/**
 * @author Christian Guretzki
 * @since 1.0
 */
@Slf4j
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class LimitedAccessAdminController extends FormBasicController {
  private static final String ON_KEY = "on";

  private MultipleSelectionElement enabledLimitedAccessSelection;
  private FormLink createGroupLink;
  private NewBGController groupCreateController;
  private CloseableModalController cmc;
  private StaticTextElement limitAccessGroupElement;
  private FormLink removeGroupLink;
  private FormLink manageGroupLink;

  @Autowired private QuestionCreationModule questionCreationModule;
  @Autowired @Lazy private BusinessGroupService businessGroupService;

  public LimitedAccessAdminController(UserRequest userRequest, WindowControl windowControl) {
    super(userRequest, windowControl);
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller listener, UserRequest userRequest) {
    initLimitAccessEnabled(formLayout);
    initLimitAccessGroupSection(formLayout);
  }

  private void initLimitAccessGroupSection(FormItemContainer formLayout) {
    if (existsLimitedAccessGroup(questionCreationModule.getLimitedAccessGroupKey())) {
      initLimitAccessGroup(formLayout);
    }
    createGroupLink = uifactory.addFormLink("create.group.link", formLayout);
    createGroupLink.setI18nKey("create.group.link");
    createGroupLink.setIconLeftCSS("o_icon o_icon-fw o_icon_add");
    createGroupLink.setVisible(
        questionCreationModule.isLimitedAccessEnabled()
            && questionCreationModule.getLimitedAccessGroupKey() == null);
  }

  private void initLimitAccessGroup(FormItemContainer formLayout) {
    BusinessGroup limitedAccessGroup =
        businessGroupService.loadBusinessGroup(questionCreationModule.getLimitedAccessGroupKey());
    limitAccessGroupElement =
        uifactory.addStaticTextElement(
            "limitAccessGroup",
            "limit.access.group.label",
            formatLimitAccessGroup(limitedAccessGroup),
            formLayout);
    limitAccessGroupElement.setVisible(questionCreationModule.isLimitedAccessEnabled());
    limitAccessGroupElement.setHelpTextKey("limit.access.group.tooltip", null);
    manageGroupLink = uifactory.addFormLink("manage.group.link", formLayout);
    manageGroupLink.setI18nKey("manage.group.link");
    manageGroupLink.setIconLeftCSS("o_icon o_icon-fw o_cmembers_icon");
    manageGroupLink.setVisible(questionCreationModule.isLimitedAccessEnabled());
    removeGroupLink = uifactory.addFormLink("remove.group.link", formLayout);
    removeGroupLink.setI18nKey("remove.group.link");
    removeGroupLink.setIconLeftCSS("o_icon o_icon-fw o_icon_delete");
    removeGroupLink.setVisible(questionCreationModule.isLimitedAccessEnabled());
  }

  private String formatLimitAccessGroup(BusinessGroup limitedAccessGroup) {
    if (limitedAccessGroup != null) {
      return limitedAccessGroup.getName() + " (" + limitedAccessGroup.getKey() + ")";
    } else {
      return "-";
    }
  }

  private boolean existsLimitedAccessGroup(Long limitedAccessGroupKey) {
    if (limitedAccessGroupKey == null) {
      return false;
    }
    BusinessGroup limitedAccessGroup =
        businessGroupService.loadBusinessGroup(limitedAccessGroupKey);
    return limitedAccessGroup != null;
  }

  private void initLimitAccessEnabled(FormItemContainer formLayout) {
    String[] onLimitedAccessKeys = new String[] {ON_KEY};
    String[] onLimitedAccessValues =
        new String[] {translate("questioncreation.admin.enable.limited.access.label")};
    enabledLimitedAccessSelection =
        uifactory.addCheckboxesHorizontal(
            "questioncreation.admin.enable.limited.access.value",
            "questioncreation.admin.enable.limited.access.value",
            formLayout,
            onLimitedAccessKeys,
            onLimitedAccessValues);
    enabledLimitedAccessSelection.select(ON_KEY, questionCreationModule.isLimitedAccessEnabled());
    enabledLimitedAccessSelection.addActionListener(FormEvent.ONCHANGE);
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    // empty
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    log.debug("formInnerEvent controller source={}, event={}", source, event);
    if (source == enabledLimitedAccessSelection) {
      questionCreationModule.setLimitedAccessEnabled(
          enabledLimitedAccessSelection.isAtLeastSelected(1));
      updateVisibleBasedOn(enabledLimitedAccessSelection.isAtLeastSelected(1));
    } else if (source == createGroupLink) {
      // user wants to create a new group -> show group create form
      removeAsListenerAndDispose(cmc);
      removeAsListenerAndDispose(groupCreateController);

      groupCreateController = new NewBGController(ureq, getWindowControl(), null, true, null);
      listenTo(groupCreateController);

      removeAsListenerAndDispose(cmc);
      cmc =
          new CloseableModalController(
              getWindowControl(), "close", groupCreateController.getInitialComponent());
      listenTo(cmc);
      cmc.activate();
    } else if (source == removeGroupLink) {
      questionCreationModule.removeLimitedAccessGroupKey();
      enabledLimitedAccessSelection.select(ON_KEY, false);
      questionCreationModule.setLimitedAccessEnabled(false);
      removeAccessGroupElements(flc);
    } else if (source == manageGroupLink) {
      launchManageGroupController(ureq);
    }
  }

  private void removeAccessGroupElements(FormLayoutContainer flc) {
    flc.remove(limitAccessGroupElement);
    flc.remove(removeGroupLink);
    flc.remove(manageGroupLink);
  }

  private void launchManageGroupController(UserRequest ureq) {
    ureq.getUserSession()
        .putEntry("wild_card_" + questionCreationModule.getLimitedAccessGroupKey(), Boolean.TRUE);
    String businessPath =
        "[BusinessGroup:" + questionCreationModule.getLimitedAccessGroupKey() + "]";
    NewControllerFactory.getInstance().launch(businessPath, ureq, getWindowControl());
  }

  private void updateVisibleBasedOn(boolean isVisible) {
    if (createGroupLink != null) {
      createGroupLink.setVisible(
          isVisible && questionCreationModule.getLimitedAccessGroupKey() == null);
    }
    if (limitAccessGroupElement != null) {
      limitAccessGroupElement.setVisible(isVisible);
      manageGroupLink.setVisible(isVisible);
      removeGroupLink.setVisible(isVisible);
    }
  }

  @Override
  protected void event(UserRequest ureq, Controller source, Event event) {
    log.debug("event: START controller={}, event.getCommand()={}", source, event.getCommand());
    if (source == groupCreateController && event == Event.DONE_EVENT) {
      log.debug("Created Group.Key={}", groupCreateController.getCreatedGroup().getKey());
      cmc.deactivate();
      questionCreationModule.setLimitedAccessGroupKey(
          groupCreateController.getCreatedGroup().getKey());
      questionCreationModule.setLimitedAccessEnabled(
          enabledLimitedAccessSelection.isAtLeastSelected(1));
      updateVisibleBasedOn(questionCreationModule.isLimitedAccessEnabled());
      initLimitAccessGroup(flc);
      enabledLimitedAccessSelection.getRootForm().submit(ureq);
    }
  }
}
