/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui;

import java.util.Date;
import java.util.Locale;
import org.olat.core.util.Formatter;
import org.olat.core.util.Util;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.ims.qti21.pool.QTI21AssessmentItemFactory;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.service.Participant.QuestionStatus;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public final class QuestionCreationUIHelper {

  public static final String FORM_MANDATORY_TEXT_KEY = "new.form.mandatory";
  public static final String BUTTON_SAVE_TEXT_KEY = "submit";

  private QuestionCreationUIHelper() {}

  public static String getLabelForQTIQuestion(QTI21QuestionType qtiQuestionType, Locale locale) {
    return new QTI21AssessmentItemFactory(qtiQuestionType)
        .getLabel(locale)
        .replaceFirst("^QTI 2.1 ", "");
  }

  public static String getTranslatedStatus(QuestionStatus questionStatus, Locale locale) {
    return Util.createPackageTranslator(QuestionCreationUIHelper.class, locale)
        .translate(questionStatus.getI18Key());
  }

  public static boolean isBeforeStartDate(QuestionCreationCourseNode questionCreationCourseNode) {
    Date startDate =
        questionCreationCourseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START);
    return startDate != null && new Date().before(startDate);
  }

  public static boolean isAfterTaskDueDate(QuestionCreationCourseNode questionCreationCourseNode) {
    Date dueDate =
        questionCreationCourseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
    return dueDate != null && new Date().after(dueDate);
  }

  public static boolean isAfterPeerReviewDueDate(
      QuestionCreationCourseNode questionCreationCourseNode) {
    Date dueDate =
        questionCreationCourseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE);
    return dueDate != null && new Date().after(dueDate);
  }

  public static String formatDate(Date dateValue, Locale locale) {
    if (dateValue != null) {
      Formatter format = Formatter.getInstance(locale);
      return format.formatDateAndTime(dateValue);
    } else {
      return "-";
    }
  }
}
