/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.edit;

import java.util.Date;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.DateChooser;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.editor.NodeEditController;
import org.olat.modules.ModuleConfiguration;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class QuestionCreationPeerReviewWorkflowEditController extends FormBasicController {

  private final ModuleConfiguration configuration;

  private DateChooser dueDate;

  public QuestionCreationPeerReviewWorkflowEditController(
      UserRequest userRequest, WindowControl windowControl, QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    this.configuration = courseNode.getModuleConfiguration();
    initForm(userRequest);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    setFormTitle("questioncreation.edit.peerreview.workflow.header");
    this.setFormContextHelp("uzh_additions/question_creation");

    dueDate =
        uifactory.addDateChooser(
            "questioncreation.edit.peerreview.workflow.date.due",
            configuration.getDateValue(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE),
            formLayout);
    dueDate.setDateChooserTimeEnabled(true);
    dueDate.setMandatory(true);

    uifactory.addFormSubmitButton(QuestionCreationUIHelper.BUTTON_SAVE_TEXT_KEY, formLayout);
  }

  @Override
  protected boolean validateFormLogic(UserRequest userRequest) {
    dueDate.clearError();
    if (dueDate.isEmpty(QuestionCreationUIHelper.FORM_MANDATORY_TEXT_KEY)) {
      return false;
    }

    Date peerReviewDueDate = dueDate.getDate();
    Date taskDueDate = configuration.getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
    if (taskDueDate == null || !taskDueDate.before(peerReviewDueDate)) {
      String taskDueDateString =
          taskDueDate == null
              ? getTranslator()
                  .translate("questioncreation.edit.peerreview.workflow.date.due.taskDueNotSet")
              : QuestionCreationUIHelper.formatDate(taskDueDate, getLocale());
      dueDate.setErrorKey(
          "questioncreation.edit.peerreview.workflow.date.due.notAfterTaskDue", taskDueDateString);
      return false;
    }

    return true;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    configuration.setDateValue(
        QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE, dueDate.getDate());
    fireEvent(ureq, NodeEditController.NODECONFIG_CHANGED_EVENT);
  }
}
