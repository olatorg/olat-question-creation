/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.edit;

import static org.olat.questioncreation.QuestionCreationCourseNode.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.DateChooser;
import org.olat.core.gui.components.form.flexible.elements.MultipleSelectionElement;
import org.olat.core.gui.components.form.flexible.elements.RichTextElement;
import org.olat.core.gui.components.form.flexible.elements.TextElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.elements.richText.TextMode;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.editor.NodeEditController;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.modules.ModuleConfiguration;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class QuestionCreationTaskEditController extends FormBasicController {

  static final Event TASK_DATE_DUE_CHANGED_EVENT = new Event("taskduedatechanged");

  private static final List<QTI21QuestionType> SUPPORTED_QUESTION_TYPES =
      List.of(
          QTI21QuestionType.sc,
          QTI21QuestionType.mc,
          QTI21QuestionType.kprim,
          QTI21QuestionType.essay);

  private static final int TITLE_MAX_LENGTH = 128;

  private final CourseEnvironment courseEnvironment;
  private final QuestionCreationCourseNode courseNode;

  private TextElement titleEl;
  private RichTextElement descriptionEl;
  private DateChooser startDate;
  private DateChooser dueDate;
  private MultipleSelectionElement questionTypeSelectionEl;
  private String[] questionTypeNames;

  @Autowired private AssessmentItemService assessmentItemService;

  public QuestionCreationTaskEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    this.courseEnvironment = courseEnvironment;
    this.courseNode = courseNode;
    initForm(userRequest);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    setFormTitle("questioncreation.edit.task.header");
    this.setFormContextHelp("uzh_additions/question_creation");

    ModuleConfiguration configuration = courseNode.getModuleConfiguration();

    // add the title input text element
    titleEl =
        uifactory.addTextElement(
            "questioncreation.edit.task.displaytitle",
            TITLE_MAX_LENGTH,
            configuration.getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_TITLE),
            formLayout);
    titleEl.setMandatory(true);
    titleEl.setCheckVisibleLength(true);

    descriptionEl =
        uifactory.addRichTextElementForStringData(
            "questioncreation.edit.task.description",
            "questioncreation.edit.task.description",
            courseNode.getDescription(),
            10,
            -1,
            false,
            null,
            null,
            formLayout,
            ureq.getUserSession(),
            getWindowControl());
    descriptionEl.setValue(
        configuration.getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION));
    descriptionEl.setMandatory(true);
    descriptionEl.setMaxLength(4000);
    descriptionEl.getEditorConfiguration().setSimplestTextModeAllowed(TextMode.multiLine);

    startDate =
        uifactory.addDateChooser(
            "questioncreation.edit.task.date.start",
            configuration.getDateValue(CFG_KEY_TASK_DATE_START),
            formLayout);
    startDate.setDateChooserTimeEnabled(true);
    startDate.setMandatory(false);
    dueDate =
        uifactory.addDateChooser(
            "questioncreation.edit.task.date.due",
            configuration.getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE),
            formLayout);
    dueDate.setDateChooserTimeEnabled(true);
    dueDate.setMandatory(true);

    questionTypeNames =
        SUPPORTED_QUESTION_TYPES.stream().map(QTI21QuestionType::name).toArray(String[]::new);
    String[] questionTypeLabels =
        SUPPORTED_QUESTION_TYPES.stream()
            .map(type -> QuestionCreationUIHelper.getLabelForQTIQuestion(type, ureq.getLocale()))
            .toArray(String[]::new);
    questionTypeSelectionEl =
        uifactory.addCheckboxesVertical(
            "questioncreation.edit.task.qti.types",
            formLayout,
            questionTypeNames,
            questionTypeLabels,
            1);
    questionTypeSelectionEl.setMandatory(true);
    questionTypeSelectionEl.setHelpTextKey("questioncreation.edit.task.qti.types.mandatory", null);
    courseNode
        .getModuleConfiguration()
        .getList(CFG_KEY_TASK_QTI_TYPES, String.class)
        .forEach(qtiType -> questionTypeSelectionEl.select(qtiType, true));
    questionTypeSelectionEl.setEnabled(
        !assessmentItemService.hasAnySavedAssessmentItem(courseEnvironment, courseNode));

    uifactory.addFormSubmitButton(QuestionCreationUIHelper.BUTTON_SAVE_TEXT_KEY, formLayout);
  }

  @Override
  protected boolean validateFormLogic(UserRequest userRequest) {
    boolean allOk = super.validateFormLogic(userRequest);

    allOk &= !titleEl.isEmpty(QuestionCreationUIHelper.FORM_MANDATORY_TEXT_KEY);
    allOk &= !descriptionEl.isEmpty(QuestionCreationUIHelper.FORM_MANDATORY_TEXT_KEY);
    allOk &= !dueDate.isEmpty(QuestionCreationUIHelper.FORM_MANDATORY_TEXT_KEY);
    if (startDate.getDate() != null
        && dueDate.getDate() != null
        && !dueDate.getDate().after(startDate.getDate())) {
      dueDate.setErrorKey("questioncreation.edit.task.date.dueNotAfterStart");
      allOk = false;
    }
    questionTypeSelectionEl.clearError();
    if (!questionTypeSelectionEl.isAtLeastSelected(1)) {
      questionTypeSelectionEl.setErrorKey("questioncreation.edit.task.qti.types.mandatory");
      allOk = false;
    }

    return allOk;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    courseNode
        .getModuleConfiguration()
        .setStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_TITLE, titleEl.getValue());

    courseNode
        .getModuleConfiguration()
        .setStringValue(
            QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION, descriptionEl.getValue());

    courseNode
        .getModuleConfiguration()
        .setDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START, startDate.getDate());

    Date configuredTaskDueDate =
        courseNode.getModuleConfiguration().getDateValue(CFG_KEY_TASK_DATE_DUE);
    if (!dueDate.getDate().equals(configuredTaskDueDate)) {
      courseNode
          .getModuleConfiguration()
          .setDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE, dueDate.getDate());
      fireEvent(ureq, TASK_DATE_DUE_CHANGED_EVENT);
    }

    List<String> selectedQuestionTypeKeysInOriginalOrder = new ArrayList<>();
    for (String questionTypeName : questionTypeNames) {
      if (questionTypeSelectionEl.getSelectedKeys().contains(questionTypeName)) {
        selectedQuestionTypeKeysInOriginalOrder.add(questionTypeName);
      }
    }
    courseNode
        .getModuleConfiguration()
        .setList(CFG_KEY_TASK_QTI_TYPES, new ArrayList<>(selectedQuestionTypeKeysInOriginalOrder));
    fireEvent(ureq, NodeEditController.NODECONFIG_CHANGED_EVENT);
  }
}
