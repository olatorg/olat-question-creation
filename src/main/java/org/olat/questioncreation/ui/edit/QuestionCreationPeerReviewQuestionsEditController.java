/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.edit;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.elements.TextAreaElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.CodeHelper;
import org.olat.course.editor.NodeEditController;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewQuestion;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class QuestionCreationPeerReviewQuestionsEditController extends FormBasicController {

  private static final String COMMAND_REMOVE = "rm";
  private static final String COMMAND_ADD = "add";
  private static final String COMMAND_UP = "up";
  private static final String COMMAND_DOWN = "down";

  private final QuestionCreationCourseNode courseNode;
  private final String repoCourseNodeIdent;

  private FormLayoutContainer reviewQuestionsContainer;
  private List<ReviewQuestionRow> reviewQuestionRows;
  private final boolean peerReviewsAlreadyAssigned;

  @Autowired private PeerReviewService peerReviewService;

  public QuestionCreationPeerReviewQuestionsEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent,
      boolean peerReviewsAlreadyAssigned) {
    super(userRequest, windowControl);
    this.courseNode = courseNode;
    this.repoCourseNodeIdent = repoCourseNodeIdent;
    this.peerReviewsAlreadyAssigned = peerReviewsAlreadyAssigned;
    initForm(userRequest);
  }

  private PeerReviewQuestion createNewPeerReviewQuestion() {
    PeerReviewQuestion peerReviewQuestion = new PeerReviewQuestion();
    peerReviewQuestion.setRepoCourseNodeIdent(repoCourseNodeIdent);
    return peerReviewQuestion;
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    setFormTitle("questioncreation.edit.peerreview.questions.header");
    this.setFormContextHelp("uzh_additions/question_creation");

    setFormDescription("questioncreation.edit.peerreview.questions.info");

    String page = velocity_root + "/review_questions.html";
    reviewQuestionsContainer =
        FormLayoutContainer.createCustomFormLayout("reviewQuestions", getTranslator(), page);
    reviewQuestionsContainer.setRootForm(mainForm);
    formLayout.add(reviewQuestionsContainer);

    reviewQuestionRows = new ArrayList<>();
    List<PeerReviewQuestion> persistedPeerReviewQuestions =
        peerReviewService.findPeerReviewQuestionsOfCourseNode(repoCourseNodeIdent);
    for (PeerReviewQuestion persistedPeerReviewTask : persistedPeerReviewQuestions) {
      reviewQuestionRows.add(createReviewQuestionRow(persistedPeerReviewTask));
    }
    if (reviewQuestionRows.isEmpty()) {
      reviewQuestionRows.add(createReviewQuestionRow(createNewPeerReviewQuestion()));
    }

    reviewQuestionsContainer.contextPut("reviewQuestionRows", reviewQuestionRows);
    recalculateQuestionLinks();

    uifactory.addFormSubmitButton(QuestionCreationUIHelper.BUTTON_SAVE_TEXT_KEY, formLayout);
  }

  private ReviewQuestionRow createReviewQuestionRow(PeerReviewQuestion peerReviewQuestion) {
    long uniqueId = CodeHelper.getRAMUniqueID();
    TextAreaElement valueEl =
        uifactory.addTextAreaElement(
            "review.question-" + uniqueId,
            null,
            -1,
            3,
            80,
            true,
            false,
            peerReviewQuestion.getQuestionText(),
            reviewQuestionsContainer);
    valueEl.setFormLayout(FormLayoutContainer.FormLayout.LAYOUT_INPUTGROUP.layout());
    valueEl.setMandatory(true);
    valueEl.setEnabled(!peerReviewsAlreadyAssigned);

    FormLink removeLink =
        createAndAddCommandLink(
            uniqueId,
            COMMAND_REMOVE,
            "o_icon_delete",
            "questioncreation.edit.peerreview.questions.cmd.rm.tooltip");
    FormLink addLink =
        createAndAddCommandLink(
            uniqueId,
            COMMAND_ADD,
            "o_icon_add",
            "questioncreation.edit.peerreview.questions.cmd.add.tooltip");
    FormLink upLink =
        createAndAddCommandLink(
            uniqueId,
            COMMAND_UP,
            "o_icon_move_up",
            "questioncreation.edit.peerreview.questions.cmd.up.tooltip");
    FormLink downLink =
        createAndAddCommandLink(
            uniqueId,
            COMMAND_DOWN,
            "o_icon_move_down",
            "questioncreation.edit.peerreview.questions.cmd.down.tooltip");

    return new ReviewQuestionRow(
        uniqueId, valueEl, removeLink, addLink, upLink, downLink, peerReviewQuestion);
  }

  private FormLink createAndAddCommandLink(
      long questionUniqueId, String command, String icon, String tooltip) {
    FormLink commandLink =
        uifactory.addFormLink(
            command + "-" + questionUniqueId,
            command,
            "",
            null,
            reviewQuestionsContainer,
            Link.NONTRANSLATED);
    commandLink.setIconLeftCSS("o_icon o_icon-lg " + icon);
    commandLink.setEnabled(!peerReviewsAlreadyAssigned);
    commandLink.setTooltip(translate(tooltip));
    reviewQuestionsContainer.add(commandLink);
    return commandLink;
  }

  @Override
  protected boolean validateFormLogic(UserRequest userRequest) {
    boolean valid = true;
    for (ReviewQuestionRow reviewQuestionRow : reviewQuestionRows) {
      reviewQuestionRow.getValueEl().clearError();
      if (reviewQuestionRow.getValueEl().getValue().isBlank()) {
        reviewQuestionRow
            .getValueEl()
            .setErrorKey("questioncreation.edit.peerreview.questions.text.mandatory");
        valid = false;
      }
    }

    return valid;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    List<PeerReviewQuestion> peerReviewQuestions = new ArrayList<>();
    for (ReviewQuestionRow reviewQuestionRow : reviewQuestionRows) {
      PeerReviewQuestion peerReviewQuestion = reviewQuestionRow.getPeerReviewQuestion();
      peerReviewQuestion.setQuestionText(reviewQuestionRow.getValueEl().getValue());
      peerReviewQuestions.add(peerReviewQuestion);
    }
    peerReviewService.resetAndSavePeerReviewQuestionsOfCourseNode(
        getIdentity(), repoCourseNodeIdent, peerReviewQuestions);

    courseNode
        .getModuleConfiguration()
        .setBooleanEntry(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_QUESTIONS_SAVED, true);
    fireEvent(ureq, NodeEditController.NODECONFIG_CHANGED_EVENT);
  }

  @Override
  protected void formInnerEvent(UserRequest userRequest, FormItem source, FormEvent event) {
    if (source instanceof FormLink button) {
      String cmd = button.getCmd();
      int uniqueId = Integer.parseInt(source.getName().split("-")[1]);
      int index =
          reviewQuestionRows.indexOf(
              reviewQuestionRows.stream()
                  .filter(reviewQuestionRow -> reviewQuestionRow.uniqueId == uniqueId)
                  .findFirst()
                  .orElseThrow());
      if (COMMAND_REMOVE.equals(cmd)) {
        doRemoveSimpleChoice(index);
      } else if (COMMAND_ADD.equals(cmd)) {
        doAddReviewQuestion(index);
      } else if (COMMAND_UP.equals(cmd)) {
        doMoveSimpleChoiceUp(index);
      } else if (COMMAND_DOWN.equals(cmd)) {
        doMoveSimpleChoiceDown(index);
      }
    }
  }

  private void doAddReviewQuestion(int indexToAdd) {
    reviewQuestionRows.add(indexToAdd + 1, createReviewQuestionRow(createNewPeerReviewQuestion()));
    recalculateQuestionLinks();
    flc.setDirty(true);
  }

  private void doRemoveSimpleChoice(int indexToRemove) {
    reviewQuestionRows.remove(indexToRemove);
    recalculateQuestionLinks();
    flc.setDirty(true);
  }

  private void doMoveSimpleChoiceUp(int indexToMoveUp) {
    ReviewQuestionRow objectToMoveUp = reviewQuestionRows.remove(indexToMoveUp);
    reviewQuestionRows.add(--indexToMoveUp, objectToMoveUp);
    recalculateQuestionLinks();
    flc.setDirty(true);
  }

  private void doMoveSimpleChoiceDown(int indexToMoveDown) {
    ReviewQuestionRow objectToMoveDown = reviewQuestionRows.remove(indexToMoveDown);
    reviewQuestionRows.add(++indexToMoveDown, objectToMoveDown);
    recalculateQuestionLinks();
    flc.setDirty(true);
  }

  private void recalculateQuestionLinks() {
    int numOfChoices = reviewQuestionRows.size();
    boolean canRemove = reviewQuestionRows.size() > 1;
    for (int i = 0; i < numOfChoices; i++) {
      ReviewQuestionRow reviewQuestionRow = reviewQuestionRows.get(i);
      reviewQuestionRow.getRemoveLink().setEnabled(canRemove && !peerReviewsAlreadyAssigned);
      reviewQuestionRow.getUpLink().setVisible(i != 0);
      reviewQuestionRow.getDownLink().setVisible(i < (numOfChoices - 1));
    }
  }

  @Getter
  public static final class ReviewQuestionRow {
    private final long uniqueId;
    private final TextAreaElement valueEl;
    private final FormLink upLink;
    private final FormLink addLink;
    private final FormLink downLink;
    private final FormLink removeLink;
    private final PeerReviewQuestion peerReviewQuestion;

    public ReviewQuestionRow(
        long uniqueId,
        TextAreaElement valueEl,
        FormLink removeLink,
        FormLink addLink,
        FormLink upLink,
        FormLink downLink,
        PeerReviewQuestion peerReviewQuestion) {
      this.uniqueId = uniqueId;
      this.valueEl = valueEl;
      this.removeLink = removeLink;
      removeLink.setUserObject(this);
      this.addLink = addLink;
      addLink.setUserObject(this);
      this.upLink = upLink;
      upLink.setUserObject(this);
      this.downLink = downLink;
      downLink.setUserObject(this);
      this.peerReviewQuestion = peerReviewQuestion;
    }
  }
}
