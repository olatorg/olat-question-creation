/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.edit;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.olat.basesecurity.GroupRoles;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.tabbedpane.TabbedPane;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.generic.tabbable.ActivateableTabbableDefaultController;
import org.olat.core.id.Identity;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.group.BusinessGroup;
import org.olat.group.BusinessGroupService;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.QuestionCreationModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class QuestionCreationEditController extends ActivateableTabbableDefaultController {

  public static final String PANE_TAB_QUESTION_CREATION_TASK_CONFIG =
      "questioncreation.edit.task.tab";
  public static final String PANE_TAB_QUESTION_CREATION_PEER_REVIEW =
      "questioncreation.edit.peerreview.tab";

  private static final String[] PANE_KEYS = {
    PANE_TAB_QUESTION_CREATION_TASK_CONFIG, PANE_TAB_QUESTION_CREATION_PEER_REVIEW
  };
  private final boolean isAdministrator;

  private QuestionCreationTaskEditController questionCreationTaskEditController;
  private QuestionCreationPeerReviewEditController questionCreationPeerReviewEditController;
  private TabbedPane tabbedPane;
  @Autowired private QuestionCreationModule questionCreationModule;
  @Autowired @Lazy private BusinessGroupService businessGroupService;

  public QuestionCreationEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    isAdministrator = userRequest.getUserSession().getRoles().isAdministrator();
    if (isAccessAllowed(getIdentity())) {
      questionCreationTaskEditController =
          new QuestionCreationTaskEditController(
              userRequest, windowControl, userCourseEnvironment.getCourseEnvironment(), courseNode);
      listenTo(questionCreationTaskEditController);
      questionCreationPeerReviewEditController =
          new QuestionCreationPeerReviewEditController(
              userRequest, windowControl, courseNode, userCourseEnvironment.getCourseEnvironment());
      questionCreationPeerReviewEditController.listenTo(questionCreationTaskEditController);
      listenTo(questionCreationPeerReviewEditController);
    } else {
      showError("questioncreation.error.access");
    }
  }

  @Override
  public String[] getPaneKeys() {
    return PANE_KEYS;
  }

  @Override
  public TabbedPane getTabbedPane() {
    return tabbedPane;
  }

  @Override
  protected void event(UserRequest userRequest, Component component, Event event) {
    // Nothing to do here
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    fireEvent(userRequest, event);
  }

  @Override
  public void addTabs(TabbedPane tabbedPane) {
    this.tabbedPane = tabbedPane;
    if (isAccessAllowed(getIdentity())) {
      tabbedPane.addTab(
          translate(PANE_TAB_QUESTION_CREATION_TASK_CONFIG), questionCreationTaskEditController);
      tabbedPane.addTab(
          translate(PANE_TAB_QUESTION_CREATION_PEER_REVIEW),
          questionCreationPeerReviewEditController);
    } else {
      for (int i = 0; i < tabbedPane.getTabCount(); i++) {
        tabbedPane.setEnabled(i, false);
      }
    }
  }

  private boolean isAccessAllowed(Identity identity) {
    log.debug(
        "isAccessAllowed: identity={},isLimitedAccessEnabled={}, isAdministrator={}",
        identity,
        questionCreationModule.isLimitedAccessEnabled(),
        isAdministrator);
    if (!questionCreationModule.isLimitedAccessEnabled() || isAdministrator) {
      return true;
    }
    if (questionCreationModule.getLimitedAccessGroupKey() == null) {
      return false;
    }
    BusinessGroup limitedAccessGroup =
        businessGroupService.loadBusinessGroup(questionCreationModule.getLimitedAccessGroupKey());
    if (limitedAccessGroup == null) {
      return false; // limitedAccessGroup was not found but key exists => no access
    }
    List<Identity> scanMembers =
        businessGroupService.getMembers(limitedAccessGroup, GroupRoles.participant.name());
    return scanMembers.contains(identity);
  }
}
