/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.edit;

import static org.olat.questioncreation.QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_ASSIGNMENT_NR;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.SingleSelection;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.editor.NodeEditController;
import org.olat.modules.ModuleConfiguration;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class QuestionCreationPeerReviewAssignmentEditController extends FormBasicController {

  public static final String ASSESSMENT_NUMBER_DEFAULT_SELECTION = "3";

  private static final String[] ASSESSMENT_NUMBER_SELECTIONS =
      new String[] {"1", "2", "3", "4", "5", "6"};

  private final ModuleConfiguration configuration;
  private final boolean peerReviewsAlreadyAssigned;

  private SingleSelection nrOfAssignments;

  public QuestionCreationPeerReviewAssignmentEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      QuestionCreationCourseNode courseNode,
      boolean peerReviewsAlreadyAssigned) {
    super(userRequest, windowControl);
    this.configuration = courseNode.getModuleConfiguration();
    this.peerReviewsAlreadyAssigned = peerReviewsAlreadyAssigned;
    initForm(userRequest);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    setFormTitle("questioncreation.edit.peerreview.assignment.header");
    this.setFormContextHelp("uzh_additions/question_creation");

    nrOfAssignments =
        uifactory.addDropdownSingleselect(
            "questioncreation.edit.peerreview.assignment.number",
            formLayout,
            ASSESSMENT_NUMBER_SELECTIONS,
            ASSESSMENT_NUMBER_SELECTIONS);
    nrOfAssignments.setEnabled(!peerReviewsAlreadyAssigned);
    nrOfAssignments.select(
        this.configuration.getStringValue(
            CFG_KEY_PEERREVIEW_ASSIGNMENT_NR, ASSESSMENT_NUMBER_DEFAULT_SELECTION),
        true);

    uifactory.addFormSubmitButton(QuestionCreationUIHelper.BUTTON_SAVE_TEXT_KEY, formLayout);
  }

  @Override
  protected void formOK(UserRequest ureq) {
    this.configuration.setStringValue(
        CFG_KEY_PEERREVIEW_ASSIGNMENT_NR, this.nrOfAssignments.getSelectedValue());
    fireEvent(ureq, NodeEditController.NODECONFIG_CHANGED_EVENT);
  }
}
