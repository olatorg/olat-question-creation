/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.edit;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.course.editor.NodeEditController;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.PeerReviewService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class QuestionCreationPeerReviewEditController extends BasicController {

  private final QuestionCreationPeerReviewWorkflowEditController
      questionCreationPeerReviewWorkflowEditController;
  private final QuestionCreationPeerReviewQuestionsEditController
      questionCreationPeerReviewQuestionsEditController;
  private final QuestionCreationPeerReviewAssignmentEditController
      questionCreationPeerReviewAssignmentEditController;

  @Autowired private PeerReviewService peerReviewService;

  public QuestionCreationPeerReviewEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment) {
    super(userRequest, windowControl);
    String repoCourseNodeIdent =
        CourseNodeIdentifier.getIdentifierFor(courseEnvironment, courseNode);
    boolean peerReviewsAlreadyAssigned =
        peerReviewService.arePeerReviewsAlreadyAssigned(repoCourseNodeIdent);
    VelocityContainer container = createVelocityContainer("edit_peer_review");

    questionCreationPeerReviewWorkflowEditController =
        new QuestionCreationPeerReviewWorkflowEditController(
            userRequest, windowControl, courseNode);
    listenTo(questionCreationPeerReviewWorkflowEditController);
    container.put(
        "workflow", questionCreationPeerReviewWorkflowEditController.getInitialComponent());

    questionCreationPeerReviewQuestionsEditController =
        new QuestionCreationPeerReviewQuestionsEditController(
            userRequest,
            windowControl,
            courseNode,
            repoCourseNodeIdent,
            peerReviewsAlreadyAssigned);
    listenTo(questionCreationPeerReviewQuestionsEditController);
    container.put(
        "questions", questionCreationPeerReviewQuestionsEditController.getInitialComponent());

    questionCreationPeerReviewAssignmentEditController =
        new QuestionCreationPeerReviewAssignmentEditController(
            userRequest, windowControl, courseNode, peerReviewsAlreadyAssigned);
    questionCreationPeerReviewAssignmentEditController.listenTo(this);
    listenTo(questionCreationPeerReviewAssignmentEditController);
    container.put(
        "assignment", questionCreationPeerReviewAssignmentEditController.getInitialComponent());

    putInitialPanel(container);
  }

  @Override
  protected void event(UserRequest userRequest, Component source, Event event) {
    // Nothing to do here
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if ((event == NodeEditController.NODECONFIG_CHANGED_EVENT
            && (source == questionCreationPeerReviewWorkflowEditController
                || source == questionCreationPeerReviewQuestionsEditController
                || source == questionCreationPeerReviewAssignmentEditController))
        || (event == QuestionCreationTaskEditController.TASK_DATE_DUE_CHANGED_EVENT
            && source instanceof QuestionCreationTaskEditController)) {
      fireEvent(userRequest, event);
    }
  }
}
