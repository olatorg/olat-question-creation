/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.submission;

import java.io.File;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.core.util.Formatter;
import org.olat.core.util.vfs.LocalFolderImpl;
import org.olat.core.util.vfs.VFSContainer;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.ims.qti21.QTI21Service;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.ims.qti21.model.xml.*;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.service.AssessmentItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import uk.ac.ed.ph.jqtiplus.node.item.AssessmentItem;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class QTIQuestionEditorController extends BasicController {

  private final Link backLink;
  private final QuestionCreationAssessmentItemEditorController editorCtrl;
  @Autowired @Lazy private QTI21Service qtiService;
  @Autowired private AssessmentItemService assessmentItemService;

  public QTIQuestionEditorController(
      UserRequest ureq,
      WindowControl wControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      long identityKey,
      String selectedQtiType,
      boolean isFinallySubmitted) {
    super(ureq, wControl);
    VelocityContainer container = createVelocityContainer("qti_question_editor");
    backLink = LinkFactory.createLink("run.qti.question.editor.back", container, this);
    String taskDescription =
        courseNode
            .getModuleConfiguration()
            .getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION);
    String taskDescriptionWithLatexFormattedFormulas =
        Formatter.formatLatexFormulas(taskDescription);
    container.contextPut("taskDescription", taskDescriptionWithLatexFormattedFormulas);

    // load saved QTI question if there is one:
    QTI21QuestionType qtiType = QTI21QuestionType.valueOf(selectedQtiType);
    AssessmentItemBuilder itemBuilder = AssessmentItemBuilderFactory.get(qtiType, ureq.getLocale());

    File itemFile =
        assessmentItemService.resolveSavedAssessmentItemFile(
            courseEnvironment, courseNode, identityKey);
    ResolvedAssessmentItem resolvedAssessmentItem =
        assessmentItemService.getSavedAssessmentItem(itemFile);
    // create a QTI question AssessmentItem instance if it does not exist:
    if (resolvedAssessmentItem == null) {
      createAndPersistAssessmentItem(itemBuilder, itemFile, courseNode);
      resolvedAssessmentItem = assessmentItemService.getSavedAssessmentItem(itemFile);
    }

    // show question details:
    File folder = itemFile.getParentFile();
    VFSContainer vfscontainer = new LocalFolderImpl(folder);
    editorCtrl =
        new QuestionCreationAssessmentItemEditorController(
            ureq,
            getWindowControl(),
            resolvedAssessmentItem,
            folder,
            vfscontainer,
            itemFile,
            false,
            isFinallySubmitted);
    listenTo(editorCtrl);
    container.put("editor", editorCtrl.getInitialComponent());
    putInitialPanel(container);
  }

  private void createAndPersistAssessmentItem(
      AssessmentItemBuilder itemBuilder, File itemFile, QuestionCreationCourseNode courseNode) {
    AssessmentItem assessmentItem = itemBuilder.getAssessmentItem();
    String taskTitle =
        courseNode
            .getModuleConfiguration()
            .getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_TITLE);
    assessmentItem.setTitle(taskTitle);
    // persist question in an XML file:
    log.debug(
        "Save assessment item in file: {}, {}", assessmentItem.getIdentifier(), itemFile.toPath());
    qtiService.persistAssessmentObject(itemFile, assessmentItem);
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (source == editorCtrl && event == Event.BACK_EVENT) {
      fireEvent(userRequest, Event.CLOSE_EVENT);
    }
  }

  @Override
  protected void event(UserRequest userRequest, Component source, Event event) {
    if (source == backLink) {
      fireEvent(userRequest, Event.CLOSE_EVENT);
    }
  }
}
