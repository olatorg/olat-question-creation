/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant;

import java.util.stream.Collectors;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.core.util.Formatter;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;

public class DisplayTaskRunController extends BasicController {

  public DisplayTaskRunController(
      UserRequest ureq, WindowControl wControl, QuestionCreationCourseNode courseNode) {
    super(ureq, wControl);
    VelocityContainer container = createVelocityContainer("display_task_run");

    if (QuestionCreationUIHelper.isBeforeStartDate(courseNode)) {
      container.contextPut("isBeforeStartDate", Boolean.TRUE);
      container.contextPut(
          "isBeforeStartDateMessage",
          translate(
              "run.task.is.before.start",
              QuestionCreationUIHelper.formatDate(
                  courseNode
                      .getModuleConfiguration()
                      .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START),
                  getLocale())));
      container.contextPut("taskCssClass", "o_notavailable");
    } else {
      if (QuestionCreationUIHelper.isAfterTaskDueDate(courseNode)) {
        container.contextPut("isAfterTaskDueDate", Boolean.TRUE);
      }
      String description =
          courseNode
              .getModuleConfiguration()
              .getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION);
      String descriptionWithLatexFormattedFormulas = Formatter.formatLatexFormulas(description);
      container.contextPut("description", descriptionWithLatexFormattedFormulas);
      String allowedQTITypeLabelsAsString =
          courseNode
              .getModuleConfiguration()
              .getList(QuestionCreationCourseNode.CFG_KEY_TASK_QTI_TYPES, String.class)
              .stream()
              .map(
                  type ->
                      QuestionCreationUIHelper.getLabelForQTIQuestion(
                          QTI21QuestionType.valueOf(type), ureq.getLocale()))
              .collect(Collectors.joining(", "));
      container.contextPut("allowedQTITypeLabelsAsString", allowedQTITypeLabelsAsString);
      container.contextPut("taskCssClass", "o_done");
    }

    putInitialPanel(container);
  }

  @Override
  protected void event(UserRequest ureq, Component source, Event event) {
    // not used
  }
}
