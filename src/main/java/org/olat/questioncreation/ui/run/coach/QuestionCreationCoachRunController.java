/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.components.segmentedview.SegmentViewComponent;
import org.olat.core.gui.components.segmentedview.SegmentViewEvent;
import org.olat.core.gui.components.segmentedview.SegmentViewFactory;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.run.coach.administration.CoachRunAdministrationController;
import org.olat.questioncreation.ui.run.coach.participants.CoachRunParticipantsController;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class QuestionCreationCoachRunController extends BasicController {

  private final VelocityContainer container;
  private final SegmentViewComponent segmentView;
  private final Link participantsLink;
  private final CoachRunParticipantsController participantsController;
  private final Link administrationLink;
  private final CoachRunAdministrationController administrationController;

  public QuestionCreationCoachRunController(
      UserRequest userRequest,
      WindowControl windowControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    container = createVelocityContainer("coach_run");
    segmentView = SegmentViewFactory.createSegmentView("segmentView", container, this);

    participantsLink = LinkFactory.createLink("run.coach.participants.tab", container, this);
    segmentView.addSegment(participantsLink, true);
    participantsController =
        new CoachRunParticipantsController(
            userRequest, windowControl, userCourseEnvironment, courseNode);

    administrationLink = LinkFactory.createLink("run.coach.administration.tab", container, this);
    segmentView.addSegment(administrationLink, false);
    administrationController =
        new CoachRunAdministrationController(
            userRequest, windowControl, userCourseEnvironment, courseNode);

    doOpenParticipants(userRequest);
    putInitialPanel(container);
  }

  @Override
  protected void event(UserRequest userRequest, Component source, Event event) {
    if (source == segmentView && event instanceof SegmentViewEvent segmentViewEvent) {
      String segmentCName = segmentViewEvent.getComponentName();
      Component clickedLink = container.getComponent(segmentCName);
      if (clickedLink == participantsLink) {
        doOpenParticipants(userRequest);
      } else if (clickedLink == administrationLink) {
        doOpenAdministration();
      }
    }
  }

  private void doOpenParticipants(UserRequest userRequest) {
    participantsController.refreshData(userRequest);
    container.put("segment", participantsController.getInitialComponent());
  }

  private void doOpenAdministration() {
    container.put("segment", administrationController.getInitialComponent());
  }
}
