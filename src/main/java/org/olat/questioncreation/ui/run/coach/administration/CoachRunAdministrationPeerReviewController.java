/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.administration;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.data.repository.SimpleUserRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.components.panel.SimpleStackedPanel;
import org.olat.core.gui.components.table.DefaultColumnDescriptor;
import org.olat.core.gui.components.table.TableController;
import org.olat.core.gui.components.table.TableDataModel;
import org.olat.core.gui.components.table.TableEvent;
import org.olat.core.gui.components.table.TableGuiConfiguration;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.olat.questioncreation.ui.run.coach.CoachParticipantViewController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CoachRunAdministrationPeerReviewController extends BasicController {

  private static final String ACTION_SELECT_CREATOR = "select_creator";
  private static final String ACTION_SELECT_REVIEWER_PREFIX = "select_reviewer_";

  private final CourseEnvironment courseEnvironment;
  private final String repoCourseNodeIdent;
  private final QuestionCreationCourseNode courseNode;
  private final SimpleStackedPanel mainPanel;
  private final VelocityContainer vcMain;
  private final Link assignPeerReviewButton;

  private TableController tableCtr;
  private PeerReviewAssignmentDataModel model;
  private CoachParticipantViewController coachParticipantViewController;

  @Autowired private SubmissionTaskService submissionTaskService;
  @Autowired private PeerReviewService peerReviewService;
  @Autowired @Lazy private SimpleUserRepository simpleUserRepository;

  CoachRunAdministrationPeerReviewController(
      UserRequest userRequest,
      WindowControl windowControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    this.courseNode = courseNode;
    this.courseEnvironment = userCourseEnvironment.getCourseEnvironment();
    this.repoCourseNodeIdent = CourseNodeIdentifier.getIdentifierFor(courseEnvironment, courseNode);

    mainPanel = new SimpleStackedPanel("peerreviewPanel");
    vcMain = createVelocityContainer("coach_administration_peerreview");

    boolean isSubmitDeadlineOver = QuestionCreationUIHelper.isAfterTaskDueDate(courseNode);
    vcMain.contextPut(
        "isSubmitDeadlineOver", QuestionCreationUIHelper.isAfterTaskDueDate(courseNode));
    if (!isSubmitDeadlineOver) {
      vcMain.contextPut("submitDeadlineNotOverYetWarning", getSubmitDeadlineNotOverYetWarning());
    }

    boolean isAssignmentPossible = isAssignmentPossible();
    vcMain.contextPut("isAssignmentPossible", isAssignmentPossible);
    if (!isAssignmentPossible) {
      vcMain.contextPut("assignmentNotPossibleWarning", getAssignmentNotPossibleWarning());
    }

    assignPeerReviewButton =
        LinkFactory.createButton("run.coach.administration.peerreview.button", vcMain, this);
    assignPeerReviewButton.setEnabled(false);
    List<PeerReviewTask> peerReviewTasks =
        peerReviewService.getPeerReviewTasks(repoCourseNodeIdent);
    if (peerReviewTasks.isEmpty()) {
      vcMain.contextPut("isAssigned", Boolean.FALSE);
      if (userCourseEnvironment.isAdmin()) {
        vcMain.contextPut(
            "assignmentInfoMessage", translate("run.coach.administration.peerreview.info.owner"));
        assignPeerReviewButton.setEnabled(true);
      } else {
        vcMain.contextPut(
            "assignmentInfoMessage", translate("run.coach.administration.peerreview.info.coach"));
      }
    } else {
      doShowAssignmentTable(userRequest, peerReviewTasks);
    }

    mainPanel.setContent(vcMain);
    putInitialPanel(mainPanel);
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (source == tableCtr && (event instanceof TableEvent selectionEvent)) {
      SimpleUser selectedUser = getSelectedUser(selectionEvent);
      coachParticipantViewController =
          new CoachParticipantViewController(
              userRequest,
              this.getWindowControl(),
              courseEnvironment,
              courseNode,
              repoCourseNodeIdent,
              selectedUser);
      this.listenTo(coachParticipantViewController);
      this.mainPanel.pushContent(coachParticipantViewController.getInitialComponent());
    } else if (source == coachParticipantViewController) {
      mainPanel.popContent();
    }
  }

  private SimpleUser getSelectedUser(TableEvent selectionEvent) {
    PeerReviewAssignment selectedRow = model.getObject(selectionEvent.getRowId());
    SimpleUser selectedUser = null;
    if (selectionEvent.getActionId().equals(ACTION_SELECT_CREATOR)) {
      selectedUser = selectedRow.creator();
    } else if (selectionEvent.getActionId().startsWith(ACTION_SELECT_REVIEWER_PREFIX)) {
      int reviewerIndex =
          Integer.parseInt(
              selectionEvent.getActionId().substring(ACTION_SELECT_REVIEWER_PREFIX.length()));
      // reviewerIndex is 1-based
      selectedUser = selectedRow.assignedPeerReviewers().get(reviewerIndex - 1);
    }
    return selectedUser;
  }

  @Override
  protected void event(UserRequest ureq, Component source, Event event) {
    if (source == assignPeerReviewButton) {
      List<PeerReviewTask> peerReviewTasks =
          peerReviewService.assignAndSavePeerReviews(
              submissionTaskService.findByRepoCourseNodeIdent(repoCourseNodeIdent),
              courseNode.getNumberOfPeerReviews());
      doShowAssignmentTable(ureq, peerReviewTasks);
    }
  }

  private String getSubmitDeadlineNotOverYetWarning() {
    Date taskDueDate =
        courseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
    return getTranslator()
        .translate(
            "run.coach.administration.peerreview.info.dueDate",
            QuestionCreationUIHelper.formatDate(taskDueDate, getLocale()));
  }

  private boolean isAssignmentPossible() {
    int numberOfPeerReviews = courseNode.getNumberOfPeerReviews();
    List<SubmissionTask> submittedTasks =
        submissionTaskService.findByRepoCourseNodeIdent(repoCourseNodeIdent);
    return submittedTasks.size() >= numberOfPeerReviews + 1;
  }

  private String getAssignmentNotPossibleWarning() {
    return getTranslator()
        .translate(
            "run.coach.administration.peerreview.info.nrOfSubmissions",
            String.valueOf(courseNode.getNumberOfPeerReviews() + 1));
  }

  private void doShowAssignmentTable(
      UserRequest userRequest, List<PeerReviewTask> peerReviewTasks) {
    vcMain.contextPut("isAssigned", Boolean.TRUE);
    vcMain.contextPut(
        "assignedMessage",
        translate(
            "run.coach.administration.peerreview.info.success",
            QuestionCreationUIHelper.formatDate(
                peerReviewTasks.get(0).getAssignDate(), getLocale())));
    initAssignedTableController(userRequest, peerReviewTasks);
    vcMain.put("assignmentTable", tableCtr.getInitialComponent());
  }

  private record PeerReviewAssignment(SimpleUser creator, List<SimpleUser> assignedPeerReviewers) {}

  private void initAssignedTableController(
      UserRequest userRequest, List<PeerReviewTask> peerReviewTasks) {

    List<PeerReviewAssignment> peerReviewAssignments =
        convertToPeerReviewAssignmentList(peerReviewTasks);

    TableGuiConfiguration tableConfig = new TableGuiConfiguration();
    tableConfig.setDownloadOffered(true);
    tableConfig.setPreferencesOffered(true, "assignedTable");
    tableConfig.setSortingEnabled(true);
    tableConfig.setDisplayRowCount(false);
    tableConfig.setPageingEnabled(true);
    tableConfig.setResultsPerPage(20);

    tableCtr =
        new TableController(
            tableConfig,
            userRequest,
            getWindowControl(),
            null,
            null,
            null,
            null,
            false,
            getTranslator());
    tableCtr.addColumnDescriptor(
        new DefaultColumnDescriptor(
            "run.coach.administration.peerreview.assignment.table.column.creator",
            0,
            ACTION_SELECT_CREATOR,
            getLocale()));
    for (int reviewerIndex = 1;
        reviewerIndex <= courseNode.getNumberOfPeerReviews();
        reviewerIndex++) {
      String reviewerColumnName =
          translate("run.coach.administration.peerreview.assignment.table.column.reviewer")
              + " "
              + reviewerIndex;
      DefaultColumnDescriptor reviewerColumnDescriptor =
          new DefaultColumnDescriptor(
              reviewerColumnName,
              reviewerIndex,
              ACTION_SELECT_REVIEWER_PREFIX + reviewerIndex,
              getLocale());
      reviewerColumnDescriptor.setTranslateHeaderKey(false);
      tableCtr.addColumnDescriptor(reviewerColumnDescriptor);
    }
    listenTo(tableCtr);
    model =
        new PeerReviewAssignmentDataModel(
            courseNode.getNumberOfPeerReviews(), peerReviewAssignments);
    tableCtr.setTableDataModel(model);
  }

  private List<PeerReviewAssignment> convertToPeerReviewAssignmentList(
      List<PeerReviewTask> peerReviewTasks) {
    List<Long> peerReviewIdentityKeys = new ArrayList<>();
    for (PeerReviewTask peerReviewTask : peerReviewTasks) {
      peerReviewIdentityKeys.add(peerReviewTask.getTask().getCreator().getId());
      peerReviewIdentityKeys.add(peerReviewTask.getReviewer().getId());
    }
    Map<Long, SimpleUser> peerReviewUsersByIdentityId =
        simpleUserRepository.findByIdentityIds(peerReviewIdentityKeys).stream()
            .collect(
                Collectors.toMap(
                    simpleUser -> simpleUser.getSimpleIdentity().getId(), Function.identity()));
    return peerReviewTasks.stream()
        .collect(Collectors.groupingBy(PeerReviewTask::getTask))
        .entrySet()
        .stream()
        .map(
            entry ->
                new PeerReviewAssignment(
                    peerReviewUsersByIdentityId.get(entry.getKey().getCreator().getId()),
                    entry.getValue().stream()
                        .map(
                            peerReviewTask ->
                                peerReviewUsersByIdentityId.get(
                                    peerReviewTask.getReviewer().getId()))
                        .toList()))
        .toList();
  }

  private static final class PeerReviewAssignmentDataModel
      implements TableDataModel<PeerReviewAssignment> {
    private final int columnCount;
    private List<PeerReviewAssignment> peerReviewAssignments;

    private PeerReviewAssignmentDataModel(
        int columnCount, List<PeerReviewAssignment> peerReviewTasks) {
      this.columnCount = columnCount;
      this.peerReviewAssignments = peerReviewTasks;
    }

    @Override
    public int getColumnCount() {
      return columnCount;
    }

    @Override
    public int getRowCount() {
      return peerReviewAssignments == null ? 0 : peerReviewAssignments.size();
    }

    @Override
    public Object getValueAt(int row, int col) {
      PeerReviewAssignment peerReviewAssignment = peerReviewAssignments.get(row);
      if (col == 0) {
        return peerReviewAssignment.creator.getNickname();
      }
      return peerReviewAssignment.assignedPeerReviewers.get(col - 1).getNickname();
    }

    @Override
    public PeerReviewAssignment getObject(int row) {
      return peerReviewAssignments.get(row);
    }

    @Override
    public void setObjects(List<PeerReviewAssignment> peerReviewAssignments) {
      this.peerReviewAssignments = peerReviewAssignments;
    }

    @Override
    public PeerReviewAssignmentDataModel createCopyWithEmptyList() {
      return new PeerReviewAssignmentDataModel(columnCount, new ArrayList<>());
    }
  }
}
