/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.peerreview;

import ch.uzh.olat.lms.openolat.data.entity.SimpleIdentity;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.elements.RichTextElement;
import org.olat.core.gui.components.form.flexible.elements.SingleSelection;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.components.form.flexible.impl.elements.SingleSelectionImpl;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.Util;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewEvaluation;
import org.olat.questioncreation.data.entity.PeerReviewQuestion;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.olat.questioncreation.ui.run.participant.submission.QuestionCreationSingleChoiceEditorController;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PeerReviewEvaluationController extends FormBasicController {

  private final QuestionCreationCourseNode courseNode;
  private final CourseEnvironment courseEnvironment;
  private final int peerReviewTaskIndex;
  private final SimpleIdentity reviewerIdentity;
  private final boolean readOnly;
  private PeerReviewTask peerReviewTask;
  private boolean assessmentResultFileExists;
  private PeerReviewQuestionEvaluationTableModel tableDataModel;
  private RichTextElement peerReviewReasoning;
  private FlexiTableElement peerReviewLikertScaleTable;

  @Autowired private AssessmentItemService assessmentItemService;
  @Autowired private PeerReviewService peerReviewService;

  protected PeerReviewEvaluationController(
      UserRequest userRequest,
      WindowControl windowControl,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment,
      int peerReviewTaskIndex,
      PeerReviewTask peerReviewTask,
      boolean readOnly) {
    super(userRequest, windowControl, "peer_review_evaluation");
    this.courseNode = courseNode;
    this.courseEnvironment = courseEnvironment;
    this.peerReviewTask = peerReviewTask;
    this.peerReviewTaskIndex = peerReviewTaskIndex;
    this.reviewerIdentity = peerReviewTask.getReviewer();
    this.readOnly = readOnly;
    this.assessmentResultFileExists =
        assessmentItemService.assessmentFileExists(
            reviewerIdentity.getId(), peerReviewTask, courseNode, courseEnvironment);

    initForm(userRequest);
  }

  @SuppressWarnings("DuplicatedCode")
  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    flc.contextPut(
        "peerReviewEvaluationTitle",
        translate("run.peerreview.evaluation.title", String.valueOf(peerReviewTaskIndex + 1)));
    flc.contextPut("assessmentResultFileExists", assessmentResultFileExists);

    if (assessmentResultFileExists) {
      FormLayoutContainer peerReviewEvaluationForm =
          FormLayoutContainer.createVerticalFormLayout("peerReviewEvaluationForm", getTranslator());
      peerReviewEvaluationForm.setRootForm(mainForm);
      formLayout.add(peerReviewEvaluationForm);
      formLayout.add("peerReviewEvaluationForm", peerReviewEvaluationForm);

      initLikertScaleTable(peerReviewEvaluationForm);
      uifactory.addSpacerElement("spacer", peerReviewEvaluationForm, true);
      peerReviewReasoning =
          uifactory.addRichTextElementForStringData(
              "run.peerreview.evaluation.reasoning",
              "run.peerreview.evaluation.reasoning",
              peerReviewTask.getReviewReasoning(),
              5,
              -1,
              false,
              null,
              null,
              peerReviewEvaluationForm,
              ureq.getUserSession(),
              getWindowControl());
      peerReviewReasoning.setEnabled(!readOnly);
      peerReviewReasoning.setMandatory(true);

      uifactory.addFormSubmitButton(QuestionCreationUIHelper.BUTTON_SAVE_TEXT_KEY, formLayout);

      // Submit Button
      FormLayoutContainer buttonsContainer =
          FormLayoutContainer.createHorizontalFormLayout("buttons", getTranslator());
      buttonsContainer.setRootForm(mainForm);
      buttonsContainer.setElementCssClass("o_sel_choices_save");
      buttonsContainer.setVisible(!readOnly);
      formLayout.add(buttonsContainer);
      formLayout.add("buttons", buttonsContainer);
      if (!readOnly) {
        FormLayoutContainer horizontalFormLayout =
            uifactory.addHorizontalFormLayout("buttons-horizontal", null, buttonsContainer);
        uifactory.addFormSubmitButton("submit", horizontalFormLayout);
        FormLink saveAndBackButton =
            uifactory.addFormLink("saveAndBack", null, null, horizontalFormLayout, Link.BUTTON);
        saveAndBackButton.setPrimary(true);
        saveAndBackButton.setTranslator(
            Util.createPackageTranslator(
                QuestionCreationSingleChoiceEditorController.class, getLocale()));
        saveAndBackButton.setI18nKey("run.qti.question.editor.save_and_back");
      }
    }
  }

  @Override
  protected boolean validateFormLogic(UserRequest userRequest) {
    boolean allOk = super.validateFormLogic(userRequest);
    peerReviewLikertScaleTable.clearError();
    for (PeerReviewQuestionEvaluationRow questionEvaluationRow : tableDataModel.getObjects()) {
      if (questionEvaluationRow.getSelectedLikertValue() == 0) {
        allOk = false;
        peerReviewLikertScaleTable.setErrorKey(QuestionCreationUIHelper.FORM_MANDATORY_TEXT_KEY);
        break;
      }
    }
    allOk &= !peerReviewReasoning.isEmpty(QuestionCreationUIHelper.FORM_MANDATORY_TEXT_KEY);
    return allOk;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    List<PeerReviewEvaluation> peerReviewEvaluations = new ArrayList<>();
    for (PeerReviewQuestionEvaluationRow questionEvaluationRow : tableDataModel.getObjects()) {
      PeerReviewEvaluation peerReviewEvaluation = questionEvaluationRow.getPeerReviewEvaluation();
      peerReviewEvaluation.setLikertScaleValue(questionEvaluationRow.getSelectedLikertValue());
      peerReviewEvaluations.add(peerReviewEvaluation);
    }
    peerReviewService.saveEvaluationsOfPeerReviewTask(peerReviewEvaluations);
    tableDataModel.setObjects(getPeerReviewQuestionEvaluationTableRows());

    peerReviewTask.setReviewReasoning(peerReviewReasoning.getValue());
    peerReviewTask = peerReviewService.updatePeerReviewTask(peerReviewTask);
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (source instanceof PeerReviewQuestionAnswerController && event == Event.DONE_EVENT) {
      this.assessmentResultFileExists =
          assessmentItemService.assessmentFileExists(
              reviewerIdentity.getId(), peerReviewTask, courseNode, courseEnvironment);
      initForm(flc, this, userRequest);
    }
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    if (source instanceof FormLink button) {
      String cmd = button.getCmd();
      if ("saveAndBack".equals(cmd) && validateFormLogic(ureq)) {
        formOK(ureq);
        fireEvent(ureq, Event.BACK_EVENT);
      }
    }

    if (source instanceof SingleSelection && event.getCommand().equals("ONCLICK")) {
      // find newly selected RadioButton and disable all other in same row
      for (PeerReviewQuestionEvaluationRow row : tableDataModel.getObjects()) {
        for (LikertValue likertValue : LikertValue.values()) {
          if (row.getLikertValueSelections().get(likertValue) == source) {
            deselectOtherLikertValues(row, likertValue);
            break;
          }
        }
      }
    }
    super.formInnerEvent(ureq, source, event);
  }

  private void deselectOtherLikertValues(
      PeerReviewQuestionEvaluationRow row, LikertValue selectedLikertValue) {
    for (LikertValue likertValue : LikertValue.values()) {
      if (likertValue != selectedLikertValue) {
        row.getLikertValueSelections().get(likertValue).select("ison", false);
      }
    }
  }

  private void initLikertScaleTable(FormItemContainer formLayout) {
    FlexiTableColumnModel tableColumnModel =
        FlexiTableDataModelFactory.createFlexiTableColumnModel();
    tableColumnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel("run.peerreview.evaluation.table.col.question", 0));
    tableColumnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            "run.peerreview.evaluation.table.col.likert.scale.disagree.strongly", 1));
    tableColumnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            "run.peerreview.evaluation.table.col.likert.scale.disagree", 2));
    tableColumnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel("run.peerreview.evaluation.table.col.likert.scale.neutral", 3));
    tableColumnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel("run.peerreview.evaluation.table.col.likert.scale.agree", 4));
    tableColumnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            "run.peerreview.evaluation.table.col.likert.scale.agree.strongly", 5));
    tableColumnModel.getColumnModel(0).setColumnCssClass("col-sm-7");
    for (int col = 1; col <= 5; col++) {
      tableColumnModel.getColumnModel(col).setAlignment(FlexiColumnModel.ALIGNMENT_CENTER);
      tableColumnModel.getColumnModel(col).setColumnCssClass("col-sm-1");
    }

    tableDataModel = new PeerReviewQuestionEvaluationTableModel(tableColumnModel);
    tableDataModel.setObjects(getPeerReviewQuestionEvaluationTableRows());

    peerReviewLikertScaleTable =
        uifactory.addTableElement(
            getWindowControl(),
            "peerReviewTable",
            tableDataModel,
            24,
            true,
            getTranslator(),
            formLayout);
    peerReviewLikertScaleTable.setNumOfRowsEnabled(false);
    peerReviewLikertScaleTable.setCustomizeColumns(false);
  }

  private List<PeerReviewQuestionEvaluationRow> getPeerReviewQuestionEvaluationTableRows() {
    List<PeerReviewQuestionEvaluationRow> rows = new ArrayList<>();
    List<PeerReviewQuestion> peerReviewQuestions =
        peerReviewService.findPeerReviewQuestionsOfCourseNode(
            CourseNodeIdentifier.getIdentifierFor(courseEnvironment, courseNode));
    List<PeerReviewEvaluation> evaluationsOfPeerReviewTask =
        peerReviewService.findEvaluationsOfPeerReviewTask(peerReviewTask);
    for (PeerReviewQuestion peerReviewQuestion : peerReviewQuestions) {
      PeerReviewEvaluation persistedPeerReviewEvaluation =
          evaluationsOfPeerReviewTask.stream()
              .filter(
                  peerReviewEvaluation ->
                      peerReviewEvaluation.getPeerReviewQuestion().equals(peerReviewQuestion))
              .findFirst()
              .orElse(null);
      if (persistedPeerReviewEvaluation == null) {
        persistedPeerReviewEvaluation = new PeerReviewEvaluation();
        persistedPeerReviewEvaluation.setPeerReviewTask(peerReviewTask);
        persistedPeerReviewEvaluation.setPeerReviewQuestion(peerReviewQuestion);
      }
      rows.add(
          new PeerReviewQuestionEvaluationRow(peerReviewQuestion, persistedPeerReviewEvaluation));
    }
    return rows;
  }

  private class PeerReviewQuestionEvaluationTableModel
      extends DefaultFlexiTableDataModel<PeerReviewQuestionEvaluationRow> {

    public PeerReviewQuestionEvaluationTableModel(FlexiTableColumnModel tableColumnModel) {
      super(tableColumnModel);
    }

    @Override
    public Object getValueAt(int row, int col) {
      PeerReviewQuestionEvaluationRow entry = getObject(row);
      return switch (col) {
        case 0 -> entry.getReviewQuestionText();
        case 1 -> entry.getLikertValueSelections().get(LikertValue.DISAGREE_STRONGLY);
        case 2 -> entry.getLikertValueSelections().get(LikertValue.DISAGREE);
        case 3 -> entry.getLikertValueSelections().get(LikertValue.NEUTRAL);
        case 4 -> entry.getLikertValueSelections().get(LikertValue.AGREE);
        case 5 -> entry.getLikertValueSelections().get(LikertValue.AGREE_STRONGLY);
        default -> entry;
      };
    }
  }

  @Getter
  private class PeerReviewQuestionEvaluationRow {
    private final String reviewQuestionText;
    private final Map<LikertValue, SingleSelection> likertValueSelections =
        new EnumMap<>(LikertValue.class);
    private final PeerReviewEvaluation peerReviewEvaluation;

    private PeerReviewQuestionEvaluationRow(
        PeerReviewQuestion peerReviewQuestion, PeerReviewEvaluation peerReviewEvaluation) {
      reviewQuestionText = peerReviewQuestion.getQuestionText();
      this.peerReviewEvaluation = peerReviewEvaluation;
      for (LikertValue likertValue : LikertValue.values()) {
        likertValueSelections.put(
            likertValue,
            createSingleSelection(
                peerReviewQuestion.getQuestionNumber(), likertValue, peerReviewEvaluation));
      }
    }

    private SingleSelection createSingleSelection(
        int reviewQuestionNr, LikertValue likertValue, PeerReviewEvaluation peerReviewEvaluation) {
      SingleSelection singleSelection =
          new SingleSelectionImpl(
              "likert_value_" + likertValue.code + "_" + reviewQuestionNr, getLocale());
      String[] keys = new String[] {"ison"};
      String[] values = new String[] {""};
      singleSelection.setKeysAndValues(keys, values, null);
      singleSelection.addActionListener(FormEvent.ONCLICK);
      if (peerReviewEvaluation.getLikertScaleValue() == likertValue.code) {
        singleSelection.select("ison", true);
      }
      singleSelection.setEnabled(!readOnly);
      return singleSelection;
    }

    int getSelectedLikertValue() {
      for (LikertValue likertValue : LikertValue.values()) {
        if (likertValueSelections.get(likertValue).isKeySelected("ison")) {
          return likertValue.code;
        }
      }
      return 0;
    }
  }

  private enum LikertValue {
    DISAGREE_STRONGLY(1),
    DISAGREE(2),
    NEUTRAL(3),
    AGREE(4),
    AGREE_STRONGLY(5);

    private final int code;

    LikertValue(int value) {
      this.code = value;
    }
  }
}
