/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.administration;

import java.util.stream.Collectors;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.Formatter;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class CoachRunAdministrationTaskController extends FormBasicController {

  private final QuestionCreationCourseNode courseNode;

  CoachRunAdministrationTaskController(
      UserRequest userRequest, WindowControl windowControl, QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    this.courseNode = courseNode;
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller listener, UserRequest userRequest) {
    setFormTitle("run.coach.administration.task.header");
    setFormContextHelp("uzh_additions/question_creation");

    String title =
        courseNode
            .getModuleConfiguration()
            .getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_TITLE);
    uifactory.addStaticTextElement("run.coach.administration.task.title", title, formLayout);

    String description =
        courseNode
            .getModuleConfiguration()
            .getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION);
    String descriptionWithLatexFormattedFormulas = Formatter.formatLatexFormulas(description);
    uifactory.addStaticTextElement(
        "run.coach.administration.task.description",
        descriptionWithLatexFormattedFormulas,
        formLayout);

    uifactory.addSpacerElement("spacer1", formLayout, false);

    String allowedQTITypeLabelsAsString =
        courseNode
            .getModuleConfiguration()
            .getList(QuestionCreationCourseNode.CFG_KEY_TASK_QTI_TYPES, String.class)
            .stream()
            .map(
                type ->
                    QuestionCreationUIHelper.getLabelForQTIQuestion(
                        QTI21QuestionType.valueOf(type), userRequest.getLocale()))
            .collect(Collectors.joining(", "));
    uifactory.addStaticTextElement(
        "run.coach.administration.task.qti.types", allowedQTITypeLabelsAsString, formLayout);

    uifactory.addSpacerElement("spacer2", formLayout, true);
  }

  @Override
  protected void formOK(UserRequest ureq) {
    /* unused */
  }
}
