/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.peerreview;

import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.core.util.Formatter;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
public class PeerReviewDetailsController extends BasicController {

  private final Link backLink;
  private final PeerReviewEvaluationController peerReviewEvaluationController;

  public PeerReviewDetailsController(
      UserRequest userRequest,
      WindowControl windowControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      List<PeerReviewTask> peerReviewTasks,
      int peerReviewTaskIndex,
      boolean readonly) {
    super(userRequest, windowControl);
    VelocityContainer container = createVelocityContainer("peer_review_details");
    backLink = LinkFactory.createLink("run.peerreview.details.back", container, this);
    container.contextPut(
        "peerReviewDetailsIntroduction", translate("run.peerreview.details.introduction"));
    String taskDescription =
        courseNode
            .getModuleConfiguration()
            .getStringValue(QuestionCreationCourseNode.CFG_KEY_TASK_DESCRIPTION);
    String taskDescriptionWithLatexFormattedFormulas =
        Formatter.formatLatexFormulas(taskDescription);
    container.contextPut("taskDescription", taskDescriptionWithLatexFormattedFormulas);

    PeerReviewQuestionAnswerController peerReviewQuestionAnswerController =
        new PeerReviewQuestionAnswerController(
            userRequest,
            windowControl,
            courseEnvironment,
            courseNode,
            peerReviewTaskIndex,
            peerReviewTasks.get(peerReviewTaskIndex),
            readonly);
    listenTo(peerReviewQuestionAnswerController);
    container.put(
        "peerReviewQuestionAnswer", peerReviewQuestionAnswerController.getInitialComponent());

    peerReviewEvaluationController =
        new PeerReviewEvaluationController(
            userRequest,
            windowControl,
            courseNode,
            courseEnvironment,
            peerReviewTaskIndex,
            peerReviewTasks.get(peerReviewTaskIndex),
            readonly);
    listenTo(peerReviewEvaluationController);
    peerReviewEvaluationController.listenTo(peerReviewQuestionAnswerController);
    container.put("peerReviewEvaluation", peerReviewEvaluationController.getInitialComponent());
    putInitialPanel(container);
  }

  @Override
  protected void event(UserRequest userRequest, Component source, Event event) {
    if (source == backLink) {
      fireEvent(userRequest, Event.CLOSE_EVENT);
    }
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (source == peerReviewEvaluationController && event == Event.BACK_EVENT) {
      fireEvent(userRequest, Event.CLOSE_EVENT);
    }
  }
}
