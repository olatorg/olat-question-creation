/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.participants;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import ch.uzh.olat.lms.openolat.data.repository.SimpleUserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableSearchEvent;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SelectionEvent;
import org.olat.core.gui.components.form.flexible.impl.elements.table.tab.FlexiTableFilterTabEvent;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.generic.modal.DialogBoxController;
import org.olat.core.gui.control.generic.modal.DialogBoxUIFactory;
import org.olat.core.id.Identity;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.Participant;
import org.olat.questioncreation.service.ParticipantService;
import org.olat.questioncreation.ui.run.coach.CoachParticipantViewController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
@Slf4j
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CoachRunParticipantsListController extends FormBasicController {

  private static final String ACTION_DETAILS = "details";
  private static final String ACTION_EXPORT = "export";

  @Autowired private ParticipantService participantService;
  @Autowired @Lazy private SimpleUserRepository simpleUserRepository;
  @Autowired private AssessmentItemService assessmentItemService;

  private final UserCourseEnvironment userCourseEnvironment;
  private final QuestionCreationCourseNode courseNode;
  private final CourseEnvironment courseEnvironment;
  private final String repoCourseNodeIdent;

  private List<Participant> participants;
  private FlexiTableElement participantsTable;
  private CoachRunParticipantsTableModel participantsTableModel;
  private DialogBoxController confirmExport;
  private FormLink bulkExportButton;
  private Set<Integer> selectedRowIds;

  private CoachParticipantViewController coachParticipantViewController;

  CoachRunParticipantsListController(
      UserRequest ureq,
      WindowControl wControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(ureq, wControl, "coach_participants_list");
    this.userCourseEnvironment = userCourseEnvironment;
    this.courseNode = courseNode;
    this.courseEnvironment = userCourseEnvironment.getCourseEnvironment();
    this.repoCourseNodeIdent = CourseNodeIdentifier.getIdentifierFor(courseEnvironment, courseNode);
    initForm(ureq);
  }

  public void refreshData(UserRequest userRequest) {
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller controller, UserRequest userRequest) {
    participants =
        participantService.getParticipants(
            getIdentity(), userCourseEnvironment, courseNode, repoCourseNodeIdent);
    initParticipantsTable(userRequest, formLayout);
  }

  private void initParticipantsTable(UserRequest userRequest, FormItemContainer formLayout) {
    FlexiTableColumnModel columnModel = FlexiTableDataModelFactory.createFlexiTableColumnModel();
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(CoachRunParticipantsTableModel.Columns.LOGINNAME));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            CoachRunParticipantsTableModel.Columns.LASTNAME, ACTION_DETAILS));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            CoachRunParticipantsTableModel.Columns.FIRSTNAME, ACTION_DETAILS));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(CoachRunParticipantsTableModel.Columns.MATRICULATION_NR));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(CoachRunParticipantsTableModel.Columns.STATUS));
    columnModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(CoachRunParticipantsTableModel.Columns.QUESTION_TYPE));
    if (userCourseEnvironment.getIdentityEnvironment().getRoles().isAuthor()
        || userCourseEnvironment.isAdmin()) {
      DefaultFlexiColumnModel exportColumnModel =
          new DefaultFlexiColumnModel(CoachRunParticipantsTableModel.Columns.EXPORT, ACTION_EXPORT);
      exportColumnModel.setHeaderTooltip(
          translate("run.coach.participants.table.header.export.tooltip"));
      columnModel.addFlexiColumnModel(exportColumnModel);
    }

    participantsTableModel =
        new CoachRunParticipantsTableModel(columnModel, getTranslator(), getLocale());
    participantsTable =
        uifactory.addTableElement(
            getWindowControl(),
            "participantsTable",
            participantsTableModel,
            10,
            false,
            getTranslator(),
            formLayout);
    participantsTable.setSearchEnabled(true);
    participantsTable.setExportEnabled(true);
    participantsTable.setSelectAllEnable(true);
    participantsTable.setMultiSelect(true);
    participantsTable.setAndLoadPersistedPreferences(
        userRequest, "question-creation-coach-participants");
    bulkExportButton =
        uifactory.addFormLink("run.coach.participants.table.bulk.export", formLayout, Link.BUTTON);
    if (userCourseEnvironment.getIdentityEnvironment().getRoles().isAuthor()
        || userCourseEnvironment.isAdmin()) {
      participantsTable.addBatchButton(bulkExportButton);
    }
    updateTableContent();
  }

  @Override
  public void event(UserRequest userRequest, Controller source, Event event) {
    log.debug(
        "event: controller={} , event={}, event.command={}", source, event, event.getCommand());
    if (source == coachParticipantViewController) {
      this.initialPanel.popContent();
    }
    if (source == confirmExport && DialogBoxUIFactory.isYesEvent(event)) {
      processConfirmedExportEvent(userRequest);
    }
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    // not used
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    log.debug(
        "formInnerEvent: controller={} , event={}, event.command={}",
        source,
        event,
        event.getCommand());
    if (source == participantsTable) {
      processTableEvent(ureq, event);
    } else if (source == bulkExportButton) {
      selectedRowIds = participantsTable.getMultiSelectedIndex();
      openExportDialog(ureq);
    }
  }

  private void processTableEvent(UserRequest ureq, FormEvent event) {
    if (event instanceof SelectionEvent selectionEvent) {
      if (event.getCommand().equals(ACTION_DETAILS)) {
        Long selectedIdentityKey =
            participantsTableModel
                .getObjects()
                .get(selectionEvent.getIndex())
                .getIdentity()
                .getKey();
        SimpleUser participant =
            simpleUserRepository.findByIdentityId(selectedIdentityKey).orElseThrow();
        coachParticipantViewController =
            new CoachParticipantViewController(
                ureq,
                this.getWindowControl(),
                courseEnvironment,
                courseNode,
                repoCourseNodeIdent,
                participant);

        this.listenTo(coachParticipantViewController);
        this.initialPanel.pushContent(coachParticipantViewController.getInitialComponent());
      } else if (event.getCommand().equals(ACTION_EXPORT)) {
        selectedRowIds = Set.of(selectionEvent.getIndex());
        openExportDialog(ureq);
      }
    } else if (event instanceof FlexiTableSearchEvent
        || event instanceof FlexiTableFilterTabEvent) {
      updateTableContent();
    }
  }

  private void openExportDialog(UserRequest userRequest) {
    if (confirmExport != null) {
      confirmExport.dispose();
    }
    List<String> buttonLabels = new ArrayList<>();
    buttonLabels.add(translate("run.coach.participants.export.dialog.ok"));
    buttonLabels.add(translate("run.coach.participants.export.dialog.cancel"));
    confirmExport =
        activateGenericDialog(
            userRequest,
            translate("run.coach.participants.export.dialog.title"),
            translate("run.coach.participants.export.dialog.text"),
            buttonLabels,
            confirmExport);
  }

  private void updateTableContent() {
    participantsTableModel.setObjects(
        participants.stream()
            .filter(
                participant ->
                    participant.isSearchMatching(participantsTable.getQuickSearchString()))
            .toList());
    participantsTable.reset(true, true, true);
  }

  private void processConfirmedExportEvent(UserRequest userRequest) {
    log.debug("confirmedExportEvent, selectedRowIds={}", selectedRowIds.toString());
    List<Participant> selectedParticipants = new ArrayList<>();
    for (Integer rowId : selectedRowIds) {
      Participant participant = participantsTableModel.getObject(rowId);
      if (participant == null
          || !participant.isStatusSubmissionDoneAndAfter()
          || !participant.isExportToPoolAllowedForTask()) {
        showWarning("run.coach.participants.export.error.question.not.available");
        return; // do not do bulk export if one of the selected questions is not available
      }
      selectedParticipants.add(participant);
    }
    int exportedSuccessfully = 0;
    for (Participant selectedParticipant : selectedParticipants) {
      if (doExport(userRequest, selectedParticipant)) {
        exportedSuccessfully++;
      }
    }
    if (exportedSuccessfully == selectedRowIds.size()) {
      showInfo("run.coach.participants.table.export.success", String.valueOf(exportedSuccessfully));
    } else {
      showWarning("run.coach.participants.export.error");
    }
    updateTableContent();
  }

  private boolean doExport(UserRequest userRequest, Participant participant) {
    log.debug(
        "export participant question, participantId={}, repoCourseNodeIdent={}",
        participant.getIdentity().getKey(),
        repoCourseNodeIdent);
    Identity identity = participant.getIdentity();
    return assessmentItemService.exportAssessmentItemToQuestionPool(
        identity,
        courseEnvironment,
        courseNode,
        userRequest.getLocale(),
        userRequest.getIdentity());
  }
}
