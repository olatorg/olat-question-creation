/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach;

import ch.uzh.olat.lms.openolat.data.entity.SimpleUser;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.components.panel.SimpleStackedPanel;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.olat.questioncreation.ui.run.participant.DisplayTaskRunController;
import org.olat.questioncreation.ui.run.participant.feedback.FeedbackRunController;
import org.olat.questioncreation.ui.run.participant.feedback.SelectFeedbackTableEvent;
import org.olat.questioncreation.ui.run.participant.peerreview.PeerReviewDetailsController;
import org.olat.questioncreation.ui.run.participant.peerreview.PeerReviewParticipantRunController;
import org.olat.questioncreation.ui.run.participant.peerreview.SelectPeerReviewParticipantTableEvent;
import org.olat.questioncreation.ui.run.participant.submission.QTIQuestionEditorController;
import org.olat.questioncreation.ui.run.participant.submission.SelectSubmissionTableEvent;

public class CoachParticipantViewController extends BasicController {
  private final CourseEnvironment courseEnvironment;
  private final QuestionCreationCourseNode courseNode;
  private final long identityKey;
  private final SimpleStackedPanel mainPanel;
  private final VelocityContainer mainContent;
  private final Link backLink;

  private CoachParticipantSubmissionController coachParticipantSubmissionController;
  private QTIQuestionEditorController qtiQuestionEditorController;
  private final PeerReviewParticipantRunController peerReviewParticipantRunController;
  private PeerReviewDetailsController peerReviewDetailsController;
  private final FeedbackRunController feedbackRunController;

  public CoachParticipantViewController(
      UserRequest userRequest,
      WindowControl windowControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent,
      SimpleUser participant) {
    super(userRequest, windowControl);
    this.courseEnvironment = courseEnvironment;
    this.courseNode = courseNode;
    this.identityKey = participant.getSimpleIdentity().getId();

    mainContent = this.createVelocityContainer("coach_participant_view");
    backLink = LinkFactory.createLink("back.to.participant.overview", mainContent, this);
    backLink.setIconLeftCSS("o_icon o_icon_back");
    mainContent.contextPut("reviewUserHeader", getReviewUserHeader(participant));
    mainContent.contextPut("reviewUserMail", participant.getEmail());

    DisplayTaskRunController displayTaskRunController =
        new DisplayTaskRunController(userRequest, windowControl, courseNode);
    mainContent.put("displayTaskRunController", displayTaskRunController.getInitialComponent());

    if (!QuestionCreationUIHelper.isBeforeStartDate(courseNode)) {
      coachParticipantSubmissionController =
          new CoachParticipantSubmissionController(
              userRequest,
              windowControl,
              courseEnvironment,
              courseNode,
              repoCourseNodeIdent,
              identityKey);
      mainContent.put(
          "submissionRunReadOnlyController",
          coachParticipantSubmissionController.getInitialComponent());
      listenTo(coachParticipantSubmissionController);
    }

    peerReviewParticipantRunController =
        new PeerReviewParticipantRunController(
            userRequest,
            windowControl,
            courseEnvironment,
            courseNode,
            repoCourseNodeIdent,
            identityKey,
            true);
    listenTo(peerReviewParticipantRunController);
    mainContent.put(
        "peerReviewParticipantRunController",
        peerReviewParticipantRunController.getInitialComponent());

    feedbackRunController =
        new FeedbackRunController(
            userRequest, windowControl, courseNode, repoCourseNodeIdent, identityKey);
    listenTo(feedbackRunController);
    mainContent.put("feedbackRunController", feedbackRunController.getInitialComponent());

    mainPanel = new SimpleStackedPanel("coachParticipantViewPanel");
    mainPanel.setContent(mainContent);
    putInitialPanel(mainPanel);
  }

  private String getReviewUserHeader(SimpleUser participant) {
    return participant.getLastName()
        + " "
        + participant.getFirstName()
        + " ("
        + participant.getNickname()
        + ")";
  }

  @Override
  protected void event(UserRequest userRequest, Component component, Event event) {
    if (component == backLink) {
      this.fireEvent(userRequest, Event.CLOSE_EVENT);
    }
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (source == coachParticipantSubmissionController
        && event instanceof SelectSubmissionTableEvent selectSubmissionTableEvent) {
      String selectedQtiType = selectSubmissionTableEvent.getSelectedQtiType();
      boolean isFinallySubmitted = selectSubmissionTableEvent.isFinallySubmitted();
      qtiQuestionEditorController =
          new QTIQuestionEditorController(
              userRequest,
              this.getWindowControl(),
              courseEnvironment,
              courseNode,
              identityKey,
              selectedQtiType,
              isFinallySubmitted);
      this.listenTo(qtiQuestionEditorController);
      mainPanel.pushContent(qtiQuestionEditorController.getInitialComponent());
    } else if (source == peerReviewParticipantRunController
        && event instanceof SelectPeerReviewParticipantTableEvent peerReviewParticipantTableEvent) {
      peerReviewDetailsController =
          new PeerReviewDetailsController(
              userRequest,
              this.getWindowControl(),
              courseEnvironment,
              courseNode,
              peerReviewParticipantTableEvent.getPeerReviewTasks(),
              peerReviewParticipantTableEvent.getIndex(),
              true);
      this.listenTo(peerReviewDetailsController);
      mainPanel.pushContent(peerReviewDetailsController.getInitialComponent());
    } else if (source == feedbackRunController
        && event instanceof SelectFeedbackTableEvent selectFeedbackTableEvent) {
      peerReviewDetailsController =
          new PeerReviewDetailsController(
              userRequest,
              getWindowControl(),
              courseEnvironment,
              courseNode,
              selectFeedbackTableEvent.getPeerReviewTasks(),
              selectFeedbackTableEvent.getIndex(),
              true);
      this.listenTo(peerReviewDetailsController);
      mainPanel.pushContent(peerReviewDetailsController.getInitialComponent());
    } else if ((source == qtiQuestionEditorController || source == peerReviewDetailsController)
        && event == Event.CLOSE_EVENT) {
      mainPanel.popContent();
      mainPanel.setContent(mainContent);
    }
  }
}
