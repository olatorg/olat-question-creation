/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.submission;

import static org.olat.ims.qti21.model.xml.AssessmentItemFactory.appendDefaultItemBody;
import static org.olat.ims.qti21.model.xml.AssessmentItemFactory.appendDefaultOutcomeDeclarations;
import static org.olat.ims.qti21.model.xml.AssessmentItemFactory.appendExtendedTextInteraction;
import static org.olat.ims.qti21.model.xml.AssessmentItemFactory.createExtendedTextResponseDeclaration;
import static org.olat.ims.qti21.model.xml.AssessmentItemFactory.createResponseProcessing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.apache.logging.log4j.Logger;
import org.olat.core.gui.render.StringOutput;
import org.olat.core.logging.Tracing;
import org.olat.core.util.StringHelper;
import org.olat.ims.qti21.QTI21Constants;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.ims.qti21.model.xml.AssessmentItemFactory;
import org.olat.ims.qti21.model.xml.interactions.LobAssessmentItemBuilder;
import uk.ac.ed.ph.jqtiplus.node.content.ItemBody;
import uk.ac.ed.ph.jqtiplus.node.content.basic.Block;
import uk.ac.ed.ph.jqtiplus.node.item.AssessmentItem;
import uk.ac.ed.ph.jqtiplus.node.item.interaction.ExtendedTextInteraction;
import uk.ac.ed.ph.jqtiplus.node.item.response.declaration.ResponseDeclaration;
import uk.ac.ed.ph.jqtiplus.node.item.response.processing.ResponseProcessing;
import uk.ac.ed.ph.jqtiplus.serialization.QtiSerializer;
import uk.ac.ed.ph.jqtiplus.types.Identifier;

/**
 * Adapted from @see {@link org.olat.ims.qti21.model.xml.interactions.EssayAssessmentItemBuilder}
 *
 * <p>- added getLabel(), setLabel()
 *
 * @author Mateja Culjak
 * @since 1.1
 */
@Getter
public class QuestionCreationEssayAssessmentItemBuilder extends LobAssessmentItemBuilder {

  private static final Logger log =
      Tracing.createLoggerFor(QuestionCreationEssayAssessmentItemBuilder.class);
  private static final String CSS_ESSAY_STANDARDTEXT = "essay-standardtext";

  private ExtendedTextInteraction extendedTextInteraction;

  public QuestionCreationEssayAssessmentItemBuilder(String title, QtiSerializer qtiSerializer) {
    super(createAssessmentItem(title), qtiSerializer);
  }

  public QuestionCreationEssayAssessmentItemBuilder(
      AssessmentItem assessmentItem, QtiSerializer qtiSerializer) {
    super(assessmentItem, qtiSerializer);
  }

  private static AssessmentItem createAssessmentItem(String title) {
    AssessmentItem assessmentItem =
        AssessmentItemFactory.createAssessmentItem(QTI21QuestionType.essay, title);

    // define the response
    Identifier responseDeclarationId = Identifier.assumedLegal("RESPONSE_1");
    ResponseDeclaration responseDeclaration =
        createExtendedTextResponseDeclaration(assessmentItem, responseDeclarationId);
    assessmentItem
        .getNodeGroups()
        .getResponseDeclarationGroup()
        .getResponseDeclarations()
        .add(responseDeclaration);

    // outcomes
    appendDefaultOutcomeDeclarations(assessmentItem, 1.0d);

    ItemBody itemBody = appendDefaultItemBody(assessmentItem);
    appendExtendedTextInteraction(itemBody, responseDeclarationId);

    // response processing
    ResponseProcessing responseProcessing =
        createResponseProcessing(assessmentItem, responseDeclarationId);
    assessmentItem
        .getNodeGroups()
        .getResponseProcessingGroup()
        .setResponseProcessing(responseProcessing);
    return assessmentItem;
  }

  @Override
  public QTI21QuestionType getQuestionType() {
    return QTI21QuestionType.essay;
  }

  /**
   * @return A copy of the list of blocks which make the question. The list is a copy and
   *     modification will not be persisted.
   */
  public List<Block> getQuestionBlocks() {
    List<Block> blocks = assessmentItem.getItemBody().getBlocks();
    List<Block> questionBlocks = new ArrayList<>(blocks.size());
    for (Block block : blocks) {
      if (block instanceof ExtendedTextInteraction) {
        break;
      } else {
        questionBlocks.add(block);
      }
    }
    return questionBlocks;
  }

  @Override
  public void extract() {
    super.extract();
    extractExtendedTextInteraction();
  }

  private void extractExtendedTextInteraction() {
    try (StringOutput sb = new StringOutput()) {
      List<Block> blocks = assessmentItem.getItemBody().getBlocks();
      for (Block block : blocks) {
        if (block instanceof ExtendedTextInteraction) {
          extendedTextInteraction = (ExtendedTextInteraction) block;
          responseIdentifier = extendedTextInteraction.getResponseIdentifier();
          break;
        } else {
          serializeJqtiObject(block, sb);
        }
      }
      question = sb.toString();
    } catch (IOException e) {
      log.error("", e);
    }
  }

  public String getPlaceholder() {
    return extendedTextInteraction.getPlaceholderText();
  }

  public void setPlaceholder(String placeholder) {
    if (StringHelper.containsNonWhitespace(placeholder)) {
      extendedTextInteraction.setPlaceholderText(placeholder);
    } else {
      extendedTextInteraction.setPlaceholderText(null);
    }
  }

  public String getLabel() {
    return extendedTextInteraction.getLabel();
  }

  public void setLabel(String label) {
    if (StringHelper.containsNonWhitespace(label)) {
      extendedTextInteraction.setLabel(label);
    } else {
      extendedTextInteraction.setLabel(null);
    }
  }

  public Integer getExpectedLength() {
    return extendedTextInteraction.getExpectedLength();
  }

  public void setExpectedLength(Integer length) {
    extendedTextInteraction.setExpectedLength(length);
  }

  public Integer getExpectedLines() {
    return extendedTextInteraction.getExpectedLines();
  }

  public void setExpectedLines(Integer lines) {
    extendedTextInteraction.setExpectedLines(lines);
  }

  public Integer getMinStrings() {
    return extendedTextInteraction.getMinStrings();
  }

  public void setMinStrings(Integer minStrings) {
    extendedTextInteraction.setMinStrings(minStrings);
  }

  public Integer getMaxStrings() {
    return extendedTextInteraction.getMaxStrings();
  }

  public void setMaxStrings(Integer maxStrings) {
    extendedTextInteraction.setMaxStrings(maxStrings);
  }

  public boolean isRichTextFormating() {
    List<String> classes = extendedTextInteraction.getClassAttr();
    return classes != null && classes.contains(QTI21Constants.CSS_ESSAY_RICHTEXT);
  }

  public void setRichTextFormating(boolean enabled) {
    enableClassFeature(enabled, QTI21Constants.CSS_ESSAY_RICHTEXT);
  }

  public boolean isStandardTextFormating() {
    List<String> classes = extendedTextInteraction.getClassAttr();
    return classes != null && classes.contains(CSS_ESSAY_STANDARDTEXT);
  }

  public void setStandardTextFormating(boolean enabled) {
    enableClassFeature(enabled, CSS_ESSAY_STANDARDTEXT);
  }

  public boolean isCopyPasteDisabled() {
    List<String> classes = extendedTextInteraction.getClassAttr();
    return classes != null && classes.contains(QTI21Constants.CSS_ESSAY_DISABLE_COPYPASTE);
  }

  public void setCopyPasteDisabled(boolean copyPasteDisabled) {
    enableClassFeature(copyPasteDisabled, QTI21Constants.CSS_ESSAY_DISABLE_COPYPASTE);
  }

  private void enableClassFeature(boolean enable, String feature) {
    List<String> cssClassses = extendedTextInteraction.getClassAttr();
    cssClassses = cssClassses == null ? new ArrayList<>() : new ArrayList<>(cssClassses);
    if (enable) {
      if (!cssClassses.contains(feature)) {
        cssClassses.add(feature);
      }
    } else {
      cssClassses.remove(feature);
    }
    extendedTextInteraction.setClassAttr(cssClassses);
  }

  @Override
  protected void buildResponseAndOutcomeDeclarations() {
    ResponseDeclaration responseDeclaration =
        createExtendedTextResponseDeclaration(assessmentItem, responseIdentifier);
    assessmentItem.getResponseDeclarations().add(responseDeclaration);
  }

  @Override
  protected void buildItemBody() {
    // remove current blocks
    List<Block> blocks = assessmentItem.getItemBody().getBlocks();
    blocks.clear();

    // add question
    getHtmlHelper().appendHtml(assessmentItem.getItemBody(), question);

    // add interaction
    blocks.add(extendedTextInteraction);
  }
}
