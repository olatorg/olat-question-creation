/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.peerreview;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SelectionEvent;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.generic.modal.DialogBoxController;
import org.olat.core.gui.control.generic.modal.DialogBoxUIFactory;
import org.olat.core.util.DateUtils;
import org.olat.core.util.Formatter;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.olat.questioncreation.ui.run.participant.peerreview.PeerReviewParticipantTableDataModel.PeerReviewParticipantTableColumns;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PeerReviewParticipantRunController extends FormBasicController {

  private static final String ACTION_SELECT = "select";
  private static final String CSS_CLASS_TEMPLATE_KEY = "peerReviewParticipantCssClass";
  private static final String CSS_CLASS_NOT_AVAILABLE = "o_notavailable";
  private static final String CSS_CLASS_ACTIVE = "o_active";
  private static final String CSS_CLASS_DONE = "o_done ";

  private final CourseEnvironment courseEnvironment;
  private final QuestionCreationCourseNode courseNode;
  private final long reviewerIdentityKey;
  private final boolean coachParticipantMode;
  private final String repoCourseNodeIdent;
  private FlexiTableElement peerReviewParticipantTableController;
  private List<PeerReviewTask> peerReviewTasksOfParticipant;
  private Link finalConfirmationButton;
  private DialogBoxController finalConfirmationDialog;
  private boolean isFinallySubmitted = false;

  @Autowired private PeerReviewService peerReviewService;
  @Autowired private SubmissionTaskService submissionTaskService;
  @Autowired private AssessmentItemService assessmentItemService;

  public PeerReviewParticipantRunController(
      UserRequest ureq,
      WindowControl wControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent,
      long reviewerIdentityKey,
      boolean coachParticipantMode) {
    super(ureq, wControl, "peer_review_participant_run");
    this.courseEnvironment = courseEnvironment;
    this.courseNode = courseNode;
    this.reviewerIdentityKey = reviewerIdentityKey;
    this.coachParticipantMode = coachParticipantMode;
    this.repoCourseNodeIdent = repoCourseNodeIdent;
    initForm(ureq);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    flc.contextPut("isCoachParticipantMode", coachParticipantMode);
    boolean isAfterTaskDueDate = QuestionCreationUIHelper.isAfterTaskDueDate(courseNode);
    boolean hasSubmittedTask =
        submissionTaskService
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
                reviewerIdentityKey, repoCourseNodeIdent, 0)
            .isPresent();

    if (isAfterTaskDueDate && !hasSubmittedTask) {
      flc.contextPut("isMissedSubmissionTaskDueDate", true);
      flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_NOT_AVAILABLE);
      return;
    }

    boolean isPeerReviewAssignmentDone =
        peerReviewService.arePeerReviewsAlreadyAssigned(repoCourseNodeIdent);
    flc.contextPut("isPeerReviewAssignmentDone", isPeerReviewAssignmentDone);
    if (!isPeerReviewAssignmentDone) {
      flc.contextPut(
          "peerReviewAssignmentNotDoneMessage",
          translate("run.peerreview.participant.assignment.not.done"));
      flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_NOT_AVAILABLE);
      return;
    }

    boolean isAfterPeerReviewDueDate =
        QuestionCreationUIHelper.isAfterPeerReviewDueDate(courseNode);
    isFinallySubmitted =
        peerReviewService.arePeerReviewTasksFinallySubmitted(
            repoCourseNodeIdent, reviewerIdentityKey);
    flc.contextPut("isFinallySubmitted", isFinallySubmitted);

    if (isAfterPeerReviewDueDate) {
      flc.contextPut("isAfterPeerReviewDueDate", Boolean.TRUE);
      flc.contextPut(
          "peerReviewDueDate",
          QuestionCreationUIHelper.formatDate(
              courseNode
                  .getModuleConfiguration()
                  .getDateValue(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE),
              getLocale()));
      if (!isFinallySubmitted) {
        flc.contextPut(
            "isAfterPeerReviewDueDateMessage",
            translate("run.peerreview.participant.missed.peer.review.due.date"));
        flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_NOT_AVAILABLE);
        return;
      }
    }

    peerReviewTasksOfParticipant =
        peerReviewService.getPeerReviewTasksByRepoCourseNodeIdentAndReviewer(
            repoCourseNodeIdent, reviewerIdentityKey);
    if (isFinallySubmitted) {
      Date finalSubmissionDate = peerReviewTasksOfParticipant.get(0).getFinalReviewSubmitDate();
      flc.contextPut(
          "finalSubmissionDate",
          QuestionCreationUIHelper.formatDate(finalSubmissionDate, getLocale()));
    }
    Date dueDate =
        courseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE);
    String daysUntilDueDate = String.valueOf(Math.abs(DateUtils.countDays(dueDate, new Date())));
    Formatter formatter = Formatter.getInstance(getLocale());
    String dueDateString = formatter.formatDateAndTime(dueDate);
    String dayOfWeek = formatter.dayOfWeekName(dueDate);
    flc.contextPut(
        "peerReviewDueDateHeaderMessage",
        translate(
            "run.peerreview.participant.due.date.message",
            daysUntilDueDate,
            dayOfWeek,
            dueDateString));
    flc.contextPut(CSS_CLASS_TEMPLATE_KEY, isFinallySubmitted ? CSS_CLASS_DONE : CSS_CLASS_ACTIVE);
    flc.contextPut("isPeerReviewActive", !isFinallySubmitted);
    initPeerReviewTable(formLayout);

    finalConfirmationButton =
        LinkFactory.createButton(
            "run.peerreview.participant.final.confirmation.button",
            flc.getFormItemComponent(),
            this);
  }

  private void initPeerReviewTable(FormItemContainer formLayout) {
    FlexiTableColumnModel columnsModel = FlexiTableDataModelFactory.createFlexiTableColumnModel();
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(PeerReviewParticipantTableColumns.peerReview));
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(PeerReviewParticipantTableColumns.status, ACTION_SELECT));
    PeerReviewParticipantTableDataModel peerReviewParticipantTableDataModel =
        new PeerReviewParticipantTableDataModel(columnsModel);
    peerReviewParticipantTableDataModel.setObjects(getPeerReviewParticipantTableRows());
    peerReviewParticipantTableController =
        uifactory.addTableElement(
            getWindowControl(),
            "peerReviewParticipantTable",
            peerReviewParticipantTableDataModel,
            getTranslator(),
            formLayout);
    peerReviewParticipantTableController.setCustomizeColumns(false);
    peerReviewParticipantTableController.setNumOfRowsEnabled(false);
  }

  private List<PeerReviewParticipantTableRow> getPeerReviewParticipantTableRows() {
    List<PeerReviewParticipantTableRow> rows = new ArrayList<>();
    for (int i = 0; i < peerReviewTasksOfParticipant.size(); i++) {
      String statusMessage = translate("run.peerreview.participant.table.row.status.todo");
      if (isFinallySubmitted) {
        statusMessage = translate("run.peerreview.participant.table.row.status.done");
      } else if (peerReviewService.isEvaluationOfPeerReviewTaskComplete(
          peerReviewTasksOfParticipant.get(i))) {
        statusMessage = translate("run.peerreview.participant.table.row.status.done.unconfirmed");
      } else if (assessmentItemService.assessmentFileExists(
          reviewerIdentityKey,
          peerReviewTasksOfParticipant.get(i),
          courseNode,
          courseEnvironment)) {
        statusMessage = translate("run.peerreview.participant.table.row.status.inprogress");
      }
      PeerReviewParticipantTableRow row =
          new PeerReviewParticipantTableRow(
              translate("run.peerreview.participant.table.row.peerreview") + " " + (i + 1),
              statusMessage);
      rows.add(row);
    }
    return rows;
  }

  @Override
  public void event(UserRequest userRequest, Component sourceComponent, Event event) {
    if (event instanceof SelectionEvent selectionEvent) {
      FormItem formItem = selectionEvent.getFormItemSource();
      String command = selectionEvent.getCommand();
      int tableRowIndex = selectionEvent.getIndex();
      if (formItem == peerReviewParticipantTableController && command.equals(ACTION_SELECT)) {
        fireEvent(
            userRequest,
            new SelectPeerReviewParticipantTableEvent(peerReviewTasksOfParticipant, tableRowIndex));
      }
    } else if (sourceComponent == finalConfirmationButton) {
      if (!peerReviewService.areEvaluationsOfAllPeerReviewTasksOfReviewerComplete(
          repoCourseNodeIdent, reviewerIdentityKey)) {
        showWarning("run.peerreview.participant.final.confirmation.validation");
        return;
      }
      openFinalConfirmationDialog(userRequest);
    }
  }

  private void openFinalConfirmationDialog(UserRequest ureq) {
    if (finalConfirmationDialog != null) {
      finalConfirmationDialog.dispose();
    }
    List<String> buttonLabels = new ArrayList<>();
    buttonLabels.add(translate("run.peerreview.participant.final.confirmation.dialog.ok"));
    buttonLabels.add(translate("run.peerreview.participant.final.confirmation.dialog.cancel"));
    finalConfirmationDialog =
        activateGenericDialog(
            ureq,
            translate("run.peerreview.participant.final.confirmation.dialog.title"),
            translate("run.peerreview.participant.final.confirmation.dialog.text"),
            buttonLabels,
            finalConfirmationDialog);
  }

  @Override
  public void event(UserRequest userRequest, Controller sourceController, Event event) {
    if (sourceController == finalConfirmationDialog && DialogBoxUIFactory.isYesEvent(event)) {
      Date finalReviewSubmitDate = new Date();
      for (PeerReviewTask peerReviewTask : peerReviewTasksOfParticipant) {
        peerReviewTask.setFinalReviewSubmitDate(finalReviewSubmitDate);
        peerReviewService.updatePeerReviewTask(peerReviewTask);
      }
      isFinallySubmitted = true;
      initForm(userRequest);
      fireEvent(userRequest, Event.DONE_EVENT);
    }
  }

  @Override
  protected void formOK(UserRequest ureq) {
    // not used
  }
}
