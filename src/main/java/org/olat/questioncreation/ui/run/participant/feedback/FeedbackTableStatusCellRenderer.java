/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.feedback;

import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableComponent;
import org.olat.core.gui.components.form.flexible.impl.elements.table.StaticFlexiCellRenderer;
import org.olat.core.gui.components.form.flexible.impl.elements.table.TextFlexiCellRenderer;
import org.olat.core.gui.render.Renderer;
import org.olat.core.gui.render.StringOutput;
import org.olat.core.gui.render.URLBuilder;
import org.olat.core.gui.translator.Translator;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
public class FeedbackTableStatusCellRenderer extends StaticFlexiCellRenderer {

  public FeedbackTableStatusCellRenderer(String action) {
    super(action, new TextFlexiCellRenderer());
  }

  @Override
  public void render(
      Renderer renderer,
      StringOutput target,
      Object cellValue,
      int row,
      FlexiTableComponent source,
      URLBuilder ubu,
      Translator translator) {
    // show select action only for 'available' status of feedback
    if (cellValue.equals(
        translator.translate(
            FeedbackRunController.FEEDBACK_TABLE_STATUS_AVAILABLE_TRANSLATION_KEY))) {
      super.render(renderer, target, cellValue, row, source, ubu, translator);
    } else {
      new TextFlexiCellRenderer().render(renderer, target, cellValue, row, source, ubu, translator);
    }
  }
}
