/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.olat.basesecurity.GroupRoles;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.panel.SimpleStackedPanel;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.core.gui.control.generic.messages.MessageController;
import org.olat.core.gui.control.generic.messages.MessageUIFactory;
import org.olat.core.id.Identity;
import org.olat.course.groupsandrights.CourseGroupManager;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.model.CourseNodeIdentifier;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.ui.run.participant.feedback.FeedbackRunController;
import org.olat.questioncreation.ui.run.participant.feedback.SelectFeedbackTableEvent;
import org.olat.questioncreation.ui.run.participant.peerreview.PeerReviewDetailsController;
import org.olat.questioncreation.ui.run.participant.peerreview.PeerReviewParticipantRunController;
import org.olat.questioncreation.ui.run.participant.peerreview.SelectPeerReviewParticipantTableEvent;
import org.olat.questioncreation.ui.run.participant.submission.QTIQuestionEditorController;
import org.olat.questioncreation.ui.run.participant.submission.SelectSubmissionTableEvent;
import org.olat.questioncreation.ui.run.participant.submission.SubmissionTaskRunController;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class QuestionCreationParticipantRunController extends BasicController {

  private final CourseEnvironment courseEnvironment;
  private final QuestionCreationCourseNode courseNode;
  private final String repoCourseNodeIdent;
  private final long identityKey;
  private final SimpleStackedPanel mainPanel;
  private SubmissionTaskRunController submissionTaskRunController;
  private QTIQuestionEditorController qtiQuestionEditorController;
  private PeerReviewParticipantRunController peerReviewParticipantRunController;
  private PeerReviewDetailsController peerReviewDetailsController;
  private FeedbackRunController feedbackRunController;

  @Autowired private PeerReviewService peerReviewService;
  @Autowired @Lazy private RepositoryService repositoryService;

  public QuestionCreationParticipantRunController(
      UserRequest userRequest,
      WindowControl wControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(userRequest, wControl);
    this.courseEnvironment = userCourseEnvironment.getCourseEnvironment();
    this.courseNode = courseNode;
    this.repoCourseNodeIdent = CourseNodeIdentifier.getIdentifierFor(courseEnvironment, courseNode);
    this.identityKey = userCourseEnvironment.getIdentityEnvironment().getIdentity().getKey();
    mainPanel = new SimpleStackedPanel("questionCreationRunPanel");
    if (isParticipantAsOwner(getIdentity(), userCourseEnvironment)) {
      mainPanel.setContent(initParticipantAsOwnerInfoMessage(userRequest, wControl));
    } else {
      mainPanel.setContent(createAndRefreshContent(userRequest, wControl));
    }
    putInitialPanel(mainPanel);
  }

  private boolean isParticipantAsOwner(
      Identity identity, UserCourseEnvironment userCourseEnvironment) {
    CourseGroupManager cgm = userCourseEnvironment.getCourseEnvironment().getCourseGroupManager();
    RepositoryEntry courseRe = cgm.getCourseEntry();
    List<String> reRoles = repositoryService.getRoles(identity, courseRe);
    return reRoles.contains(GroupRoles.owner.name());
  }

  private Component initParticipantAsOwnerInfoMessage(UserRequest ureq, WindowControl wControl) {
    MessageController previewMessageController =
        MessageUIFactory.createInfoMessage(
            ureq,
            wControl,
            translate("participant.view.owner.title"),
            translate("participant.view.owner.text"));
    return previewMessageController.getInitialComponent();
  }

  private VelocityContainer createAndRefreshContent(
      UserRequest userRequest, WindowControl wControl) {
    VelocityContainer container = createVelocityContainer("participant_run");
    DisplayTaskRunController displayTaskRunController =
        new DisplayTaskRunController(userRequest, wControl, courseNode);
    container.put("displayTaskRunController", displayTaskRunController.getInitialComponent());
    submissionTaskRunController =
        new SubmissionTaskRunController(
            userRequest, wControl, courseNode, courseEnvironment, repoCourseNodeIdent, identityKey);
    listenTo(submissionTaskRunController);
    container.put("submissionTaskRunController", submissionTaskRunController.getInitialComponent());
    peerReviewParticipantRunController =
        new PeerReviewParticipantRunController(
            userRequest,
            wControl,
            courseEnvironment,
            courseNode,
            repoCourseNodeIdent,
            identityKey,
            false);
    listenTo(peerReviewParticipantRunController);
    container.put(
        "peerReviewParticipantRunController",
        peerReviewParticipantRunController.getInitialComponent());
    feedbackRunController =
        new FeedbackRunController(
            userRequest, wControl, courseNode, repoCourseNodeIdent, identityKey);
    listenTo(feedbackRunController);
    container.put("feedbackRunController", feedbackRunController.getInitialComponent());
    return container;
  }

  @Override
  protected void event(UserRequest ureq, Component source, Event event) {
    // not used
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (source == submissionTaskRunController
        && event instanceof SelectSubmissionTableEvent selectSubmissionTableEvent) {
      String selectedQtiType = selectSubmissionTableEvent.getSelectedQtiType();
      boolean isFinallySubmitted = selectSubmissionTableEvent.isFinallySubmitted();
      qtiQuestionEditorController =
          new QTIQuestionEditorController(
              userRequest,
              this.getWindowControl(),
              courseEnvironment,
              courseNode,
              identityKey,
              selectedQtiType,
              isFinallySubmitted);
      this.listenTo(qtiQuestionEditorController);
      mainPanel.pushContent(qtiQuestionEditorController.getInitialComponent());
    } else if (source == peerReviewParticipantRunController
        && event instanceof SelectPeerReviewParticipantTableEvent peerReviewParticipantTableEvent) {
      peerReviewDetailsController =
          new PeerReviewDetailsController(
              userRequest,
              this.getWindowControl(),
              courseEnvironment,
              courseNode,
              peerReviewParticipantTableEvent.getPeerReviewTasks(),
              peerReviewParticipantTableEvent.getIndex(),
              peerReviewService.arePeerReviewTasksFinallySubmitted(
                  repoCourseNodeIdent, userRequest.getIdentity().getKey()));
      this.listenTo(peerReviewDetailsController);
      mainPanel.pushContent(peerReviewDetailsController.getInitialComponent());
    } else if (source == feedbackRunController
        && event instanceof SelectFeedbackTableEvent selectFeedbackTableEvent) {
      peerReviewDetailsController =
          new PeerReviewDetailsController(
              userRequest,
              getWindowControl(),
              courseEnvironment,
              courseNode,
              selectFeedbackTableEvent.getPeerReviewTasks(),
              selectFeedbackTableEvent.getIndex(),
              true);
      this.listenTo(peerReviewDetailsController);
      mainPanel.pushContent(peerReviewDetailsController.getInitialComponent());
    } else if ((source == submissionTaskRunController
            || source == qtiQuestionEditorController
            || source == peerReviewDetailsController)
        && event == Event.CLOSE_EVENT) {
      mainPanel.popContent();
      mainPanel.setContent(createAndRefreshContent(userRequest, getWindowControl()));
    } else if (source == peerReviewParticipantRunController && event == Event.DONE_EVENT) {
      mainPanel.setContent(createAndRefreshContent(userRequest, getWindowControl()));
    }
  }
}
