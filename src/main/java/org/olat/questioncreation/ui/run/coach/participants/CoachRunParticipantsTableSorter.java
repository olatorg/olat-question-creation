/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.participants;

import java.util.Locale;
import org.olat.core.commons.persistence.SortKey;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SortableFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SortableFlexiTableModelDelegate;
import org.olat.questioncreation.service.Participant;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class CoachRunParticipantsTableSorter extends SortableFlexiTableModelDelegate<Participant> {

  public CoachRunParticipantsTableSorter(
      SortKey orderBy, SortableFlexiTableDataModel<Participant> tableModel, Locale locale) {
    super(orderBy, tableModel, locale);
  }
}
