/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.feedback;

import java.util.ArrayList;
import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SelectionEvent;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.service.PeerReviewService;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.olat.questioncreation.ui.run.participant.feedback.FeedbackTableDataModel.FeedbackTableColumns;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class FeedbackRunController extends FormBasicController {

  private static final String FEEDBACK_CSS_CLASS = "feedbackCssClass";
  private static final String ACTION_SELECT = "select";
  static final String FEEDBACK_TABLE_STATUS_AVAILABLE_TRANSLATION_KEY =
      "run.feedback.table.row.status.available";

  private final QuestionCreationCourseNode courseNode;
  private final String repoCourseNodeIdent;
  private final long creatorIdentityKey;
  private FlexiTableElement feedbackTableController;
  private List<PeerReviewTask> peerReviewTasksForSubmission;
  @Autowired private SubmissionTaskService submissionTaskService;
  @Autowired private PeerReviewService peerReviewService;

  public FeedbackRunController(
      UserRequest userRequest,
      WindowControl windowControl,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent,
      long creatorIdentityKey) {
    super(userRequest, windowControl, "feedback_run");
    this.courseNode = courseNode;
    this.repoCourseNodeIdent = repoCourseNodeIdent;
    this.creatorIdentityKey = creatorIdentityKey;
    initForm(userRequest);
  }

  @Override
  protected void initForm(
      FormItemContainer formLayout, Controller listener, UserRequest userRequest) {
    boolean areAllPeerReviewsFinallySubmitted =
        peerReviewService.arePeerReviewTasksFinallySubmitted(
            repoCourseNodeIdent, creatorIdentityKey);
    flc.contextPut("areAllPeerReviewsFinallySubmitted", areAllPeerReviewsFinallySubmitted);
    boolean arePeerReviewsAssigned =
        peerReviewService.arePeerReviewsAlreadyAssigned(repoCourseNodeIdent);
    flc.contextPut("arePeerReviewsAssigned", arePeerReviewsAssigned);
    if (!areAllPeerReviewsFinallySubmitted || !arePeerReviewsAssigned) {
      flc.contextPut(FEEDBACK_CSS_CLASS, "o_notavailable");
      flc.contextPut(
          "isPeerReviewDeadlineMissed",
          QuestionCreationUIHelper.isAfterPeerReviewDueDate(courseNode));
    } else {
      flc.contextPut("isFeedbackActive", true);
      flc.contextPut(FEEDBACK_CSS_CLASS, "o_done");
      initFeedbackTable(formLayout);
    }
  }

  private void initFeedbackTable(FormItemContainer formLayout) {
    FlexiTableColumnModel columnsModel = FlexiTableDataModelFactory.createFlexiTableColumnModel();
    columnsModel.addFlexiColumnModel(new DefaultFlexiColumnModel(FeedbackTableColumns.feedback));
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            FeedbackTableColumns.status, new FeedbackTableStatusCellRenderer(ACTION_SELECT)));
    FeedbackTableDataModel feedbackTableDataModel = new FeedbackTableDataModel(columnsModel);
    List<FeedbackTableRow> feedbackTableRows = getFeedbackTableRows();
    feedbackTableDataModel.setObjects(feedbackTableRows);
    feedbackTableController =
        uifactory.addTableElement(
            getWindowControl(),
            "feedbackTable",
            feedbackTableDataModel,
            getTranslator(),
            formLayout);
    feedbackTableController.setCustomizeColumns(false);
    feedbackTableController.setNumOfRowsEnabled(false);
  }

  private List<FeedbackTableRow> getFeedbackTableRows() {
    List<FeedbackTableRow> feedbackTableRows = new ArrayList<>();
    SubmissionTask submissionTask =
        submissionTaskService
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
                creatorIdentityKey, repoCourseNodeIdent, 0)
            .orElse(null);
    if (submissionTask != null) {
      peerReviewTasksForSubmission =
          peerReviewService.getPeerReviewTasksBySubmissionTask(submissionTask);
      for (int i = 0; i < peerReviewTasksForSubmission.size(); i++) {
        String statusMessage;
        boolean isAfterPeerReviewDate =
            QuestionCreationUIHelper.isAfterPeerReviewDueDate(courseNode);
        boolean isPeerReviewTaskFinallySubmitted =
            peerReviewTasksForSubmission.get(i).getFinalReviewSubmitDate() != null;
        if (isAfterPeerReviewDate && !isPeerReviewTaskFinallySubmitted) {
          statusMessage = translate("run.feedback.table.row.status.missed_deadline");
        } else if (!isPeerReviewTaskFinallySubmitted) {
          statusMessage = translate("run.feedback.table.row.status.not_available");
        } else {
          statusMessage = translate(FEEDBACK_TABLE_STATUS_AVAILABLE_TRANSLATION_KEY);
        }
        FeedbackTableRow row =
            new FeedbackTableRow(
                translate("run.feedback.table.row.feedback") + " " + (i + 1), statusMessage);
        feedbackTableRows.add(row);
      }
    }
    return feedbackTableRows;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    // unused
  }

  @Override
  public void event(UserRequest userRequest, Component sourceComponent, Event event) {
    if (event instanceof SelectionEvent selectionEvent) {
      FormItem formItem = selectionEvent.getFormItemSource();
      String command = selectionEvent.getCommand();
      int tableRowIndex = selectionEvent.getIndex();
      if (formItem == feedbackTableController && command.equals(ACTION_SELECT)) {
        fireEvent(
            userRequest, new SelectFeedbackTableEvent(peerReviewTasksForSubmission, tableRowIndex));
      }
    }
  }
}
