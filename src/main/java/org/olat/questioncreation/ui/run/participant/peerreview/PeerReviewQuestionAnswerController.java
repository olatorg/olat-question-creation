/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.peerreview;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.components.panel.SimpleStackedPanel;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.core.util.Util;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.fileresource.types.ImsQTI21Resource;
import org.olat.fileresource.types.ImsQTI21Resource.PathResourceLocator;
import org.olat.ims.qti21.*;
import org.olat.ims.qti21.manager.audit.DefaultAssessmentSessionAuditLogger;
import org.olat.ims.qti21.model.InMemoryAssessmentTestSession;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.ims.qti21.ui.AssessmentItemDisplayController;
import org.olat.ims.qti21.ui.CandidateSessionContext;
import org.olat.ims.qti21.ui.ResponseInput;
import org.olat.ims.qti21.ui.assessment.TerminatedStaticCandidateSessionContext;
import org.olat.ims.qti21.ui.components.ItemBodyResultFormItem;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.PeerReviewTask;
import org.olat.questioncreation.service.AssessmentItemService;
import org.springframework.beans.factory.annotation.Autowired;
import uk.ac.ed.ph.jqtiplus.node.item.interaction.ExtendedTextInteraction;
import uk.ac.ed.ph.jqtiplus.node.item.interaction.Interaction;
import uk.ac.ed.ph.jqtiplus.node.result.AssessmentResult;
import uk.ac.ed.ph.jqtiplus.node.result.ItemResult;
import uk.ac.ed.ph.jqtiplus.node.result.ItemVariable;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;
import uk.ac.ed.ph.jqtiplus.state.*;
import uk.ac.ed.ph.jqtiplus.types.Identifier;
import uk.ac.ed.ph.jqtiplus.types.StringResponseData;
import uk.ac.ed.ph.jqtiplus.value.FloatValue;
import uk.ac.ed.ph.jqtiplus.value.StringValue;
import uk.ac.ed.ph.jqtiplus.xmlutils.locators.ResourceLocator;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "JavadocReference"})
public class PeerReviewQuestionAnswerController extends BasicController {

  private static final String SCORE_IDENTIFIER = "SCORE";
  private static final String MAX_SCORE_IDENTIFIER = "MAXSCORE";
  private static final String RESPONSE_IDENTIFIER = "RESPONSE_1";
  private static final String KPRIM_RESPONSE_IDENTIFIER = "KPRIM_RESPONSE_1";

  private final VelocityContainer vcMain;
  private final ResolvedAssessmentItem resolvedAssessmentItem;
  private final File assessmentResultFile;
  private final QTI21QuestionType questionType;
  private PeerReviewQuestionAnswerAssessmentItemDisplayController
      peerReviewQuestionAnswerAssessmentItemController;
  private boolean questionPassed = false;
  @Autowired private AssessmentItemService assessmentItemService;

  protected PeerReviewQuestionAnswerController(
      UserRequest userRequest,
      WindowControl windowControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      int peerReviewTaskIndex,
      PeerReviewTask peerReviewTask,
      boolean readonly) {
    super(userRequest, windowControl);
    SimpleStackedPanel mainPanel = new SimpleStackedPanel("peerReviewQuestionAnswerPanel");
    vcMain = createVelocityContainer("peer_review_question_answer");
    vcMain.contextPut(
        "peerReviewQuestionAnswerTitle",
        translate("run.peerreview.question.answer.title", String.valueOf(peerReviewTaskIndex + 1)));
    mainPanel.setContent(vcMain);
    putInitialPanel(mainPanel);

    long reviewerIdentityKey = peerReviewTask.getReviewer().getId();
    long creatorIdentityKey = peerReviewTask.getTask().getCreator().getId();
    File questionCreationFolder =
        assessmentItemService.getQTIQuestionFolder(courseEnvironment, courseNode);
    File assessmentItemFile =
        assessmentItemService.resolveSavedAssessmentItemFile(
            courseEnvironment, courseNode, creatorIdentityKey);
    resolvedAssessmentItem = assessmentItemService.getSavedAssessmentItem(assessmentItemFile);
    questionType = QTI21QuestionType.getType(resolvedAssessmentItem);

    // check if saved assessment result xml exists, and only show question answer if it does exist:
    assessmentResultFile =
        assessmentItemService.getAssessmentResultFile(
            reviewerIdentityKey, creatorIdentityKey, courseNode, courseEnvironment);
    if (assessmentResultFile.exists()) {
      doFeedback(userRequest, assessmentResultFile);
    } else {
      // open question:
      peerReviewQuestionAnswerAssessmentItemController =
          new PeerReviewQuestionAnswerAssessmentItemDisplayController(
              userRequest,
              getWindowControl(),
              resolvedAssessmentItem,
              questionCreationFolder,
              assessmentItemFile,
              assessmentResultFile.getName(),
              questionType,
              readonly);
      listenTo(peerReviewQuestionAnswerAssessmentItemController);
      vcMain.put(
          "peerReviewQuestionAnswerAssessmentItem",
          peerReviewQuestionAnswerAssessmentItemController.getInitialComponent());
    }
  }

  @Override
  protected void event(UserRequest userRequest, Controller source, Event event) {
    if (peerReviewQuestionAnswerAssessmentItemController == source
        && event instanceof QuestionAnswerResponseEvent questionAnswerResponseEvent) {
      this.questionPassed = questionAnswerResponseEvent.isPassed();
      doFeedback(userRequest, assessmentResultFile);
      fireEvent(userRequest, Event.DONE_EVENT);
    }
  }

  @Override
  protected void event(UserRequest ureq, Component source, Event event) {
    // unused
  }

  private void doFeedback(UserRequest userRequest, File assessmentResultFile) {
    ItemSessionState itemSessionState = new ItemSessionState();
    PeerReviewQuestionAnswerFeedbackController peerReviewQuestionAnswerFeedbackController =
        new PeerReviewQuestionAnswerFeedbackController(
            userRequest,
            getWindowControl(),
            resolvedAssessmentItem,
            itemSessionState,
            assessmentResultFile,
            questionPassed,
            assessmentItemService,
            questionType);
    vcMain.put(
        "peerReviewQuestionAnswerFeedback",
        peerReviewQuestionAnswerFeedbackController.getInitialComponent());
    vcMain.contextPut("passed", questionPassed);
  }

  /**
   * Adapted from @see {@link
   * org.olat.course.nodes.practice.ui.PracticeController.PracticeAssessmentItemController}
   */
  private static class PeerReviewQuestionAnswerAssessmentItemDisplayController
      extends AssessmentItemDisplayController {

    private final File questionCreationFolder;
    private final String assessmentResultFilename;
    private final QTI21QuestionType questionType;
    private ItemSessionState itemSessionState;

    private PeerReviewQuestionAnswerAssessmentItemDisplayController(
        UserRequest userRequest,
        WindowControl windowControl,
        ResolvedAssessmentItem resolvedAssessmentItem,
        File questionCreationFolder,
        File assessmentItemFile,
        String assessmentResultFilename,
        QTI21QuestionType questionType,
        boolean readonly) {
      super(
          userRequest,
          windowControl,
          resolvedAssessmentItem,
          questionCreationFolder,
          assessmentItemFile,
          QTI21DeliveryOptions.defaultSettings(),
          new DefaultAssessmentSessionAuditLogger());
      this.questionCreationFolder = questionCreationFolder;
      this.assessmentResultFilename = assessmentResultFilename;
      this.questionType = questionType;
      qtiWorksCtrl.setSubmitI18nKey("submit.check");
      if (readonly) {
        // if readonly, disable the possibility of answering the question:
        this.endSession(userRequest);
      }
    }

    @Override
    protected void collectOutcomeVariablesForItemSession(
        BigDecimal score, BigDecimal maxScore, Boolean passed) {
      if (QTI21QuestionType.essay.equals(questionType)) {
        // essay type doesn't have correct answers and pass/fail defined
        itemSession.setPassed(Boolean.TRUE);
        return;
      }
      if (passed == null) {
        if (score == null) {
          itemSession.setPassed(Boolean.TRUE);
        } else if (maxScore != null) {
          if (score.equals(maxScore)) {
            itemSession.setPassed(Boolean.TRUE);
          } else {
            itemSession.setPassed(Boolean.FALSE);
          }
        } else if (score.doubleValue() <= 0.01d) {
          itemSession.setPassed(Boolean.FALSE);
        } else {
          itemSession.setPassed(Boolean.TRUE);
        }
      } else {
        itemSession.setPassed(passed);
      }
    }

    @Override
    protected ItemSessionState loadItemSessionState() {
      if (itemSessionState == null) {
        itemSessionState = new ItemSessionState();
      }
      return itemSessionState;
    }

    @Override
    public void handleResponses(
        UserRequest userRequest,
        Map<Identifier, ResponseInput> stringResponseMap,
        Map<Identifier, ResponseInput> fileResponseMap,
        String candidateComment) {
      super.handleResponses(userRequest, stringResponseMap, fileResponseMap, candidateComment);
      final Boolean passed = itemSession.getPassed();
      fireEvent(userRequest, new QuestionAnswerResponseEvent(passed != null && passed));
    }

    @Override
    protected AssessmentResult updateSessionFinishedStatus(UserRequest userRequest) {
      AssessmentResult assessmentResult = computeAndRecordItemAssessmentResult(userRequest);
      // move file from tmp to question creation folder:
      File tmpAssessmentResultFile = qtiService.getAssessmentResultFile(candidateSession);
      File newAssessmentResultFile = new File(questionCreationFolder, assessmentResultFilename);
      if (!newAssessmentResultFile.exists()) {
        try {
          Files.move(tmpAssessmentResultFile.toPath(), newAssessmentResultFile.toPath());
        } catch (IOException e) {
          log.error(
              "Unable to move assessment result file to path: {}",
              newAssessmentResultFile.toPath());
        }
      }
      return assessmentResult;
    }
  }

  /**
   * Adapted from @see {@link
   * org.olat.course.nodes.practice.ui.PracticeController.FeedbackController}
   */
  private static class PeerReviewQuestionAnswerFeedbackController extends FormBasicController {

    private final ItemSessionState itemSessionState;
    private final ResolvedAssessmentItem resolvedAssessmentItem;
    private final CandidateSessionContext candidateSessionContext;
    private final ResourceLocator inputResourceLocator;
    private final QTI21QuestionType questionType;
    private boolean questionPassed;

    private PeerReviewQuestionAnswerFeedbackController(
        UserRequest ureq,
        WindowControl wControl,
        ResolvedAssessmentItem resolvedAssessmentItem,
        ItemSessionState itemSessionState,
        File assessmentResultFile,
        boolean questionPassed,
        AssessmentItemService assessmentItemService,
        QTI21QuestionType questionType) {
      super(
          ureq,
          wControl,
          "peer_review_question_answer_feedback",
          Util.createPackageTranslator(AssessmentItemDisplayController.class, ureq.getLocale()));
      this.questionPassed = questionPassed;
      this.itemSessionState = itemSessionState;
      this.resolvedAssessmentItem = resolvedAssessmentItem;
      this.questionType = questionType;
      candidateSessionContext =
          new TerminatedStaticCandidateSessionContext(new InMemoryAssessmentTestSession());
      ResourceLocator fileResourceLocator =
          new PathResourceLocator(assessmentResultFile.getParentFile().toPath());
      inputResourceLocator = ImsQTI21Resource.createResolvingResourceLocator(fileResourceLocator);
      AssessmentResult assessmentResult =
          assessmentItemService.getAssessmentResult(assessmentResultFile, inputResourceLocator);
      setItemSessionStateResults(itemSessionState, assessmentResult);
      if (QTI21QuestionType.essay.equals(questionType)) {
        List<Interaction> interactions =
            resolvedAssessmentItem
                .getItemLookup()
                .extractIfSuccessful()
                .getItemBody()
                .findInteractions();
        for (Interaction interaction : interactions) {
          if (interaction instanceof ExtendedTextInteraction extendedTextInteraction) {
            String sampleSolutionText = extendedTextInteraction.getLabel();
            flc.contextPut("sampleSolutionText", sampleSolutionText);
          }
        }
      }
      initForm(ureq);
    }

    private void setItemSessionStateResults(
        ItemSessionState itemSessionState, AssessmentResult assessmentResult) {
      if (assessmentResult != null) {
        List<ItemResult> itemResultList = assessmentResult.getItemResults();
        BigDecimal score = null;
        BigDecimal maxScore = null;
        for (ItemResult itemResult : itemResultList) {
          for (ItemVariable itemVariable : itemResult.getItemVariables()) {
            if (itemVariable.getIdentifier().toString().equals(SCORE_IDENTIFIER)) {
              score =
                  BigDecimal.valueOf(((FloatValue) itemVariable.getComputedValue()).doubleValue());
            } else if (itemVariable.getIdentifier().toString().equals(MAX_SCORE_IDENTIFIER)) {
              maxScore =
                  BigDecimal.valueOf(((FloatValue) itemVariable.getComputedValue()).doubleValue());
            } else if (itemVariable.getIdentifier().toString().equals(RESPONSE_IDENTIFIER)
                || itemVariable.getIdentifier().toString().equals(KPRIM_RESPONSE_IDENTIFIER)) {
              setResponseValues(itemSessionState, itemVariable);
            }
          }
        }
        setPassedBasedOnScore(score, maxScore);
      }
    }

    private void setResponseValues(ItemSessionState itemSessionState, ItemVariable itemVariable) {
      itemSessionState.setOutcomeValue(
          itemVariable.getIdentifier(), itemVariable.getComputedValue());
      itemSessionState.setResponseValue(
          itemVariable.getIdentifier(), itemVariable.getComputedValue());
      if (QTI21QuestionType.essay.equals(questionType)) {
        itemSessionState.setRawResponseData(
            itemVariable.getIdentifier(),
            new StringResponseData(((StringValue) itemVariable.getComputedValue()).stringValue()));
      }
    }

    private void setPassedBasedOnScore(
        BigDecimal score,
        BigDecimal maxScore) { // same logic as in collectOutcomeVariablesForItemSession() method
      if (QTI21QuestionType.essay.equals(questionType)) {
        questionPassed = true;
        return;
      }
      if (score == null) {
        questionPassed = true;
      } else if (maxScore != null) {
        questionPassed = score.equals(maxScore);
      } else questionPassed = (score.doubleValue() > 0.01d);
    }

    @Override
    protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
      if (formLayout instanceof FormLayoutContainer layoutCont) {
        layoutCont.contextPut("passed", questionPassed);
        String itemTitle = resolvedAssessmentItem.getItemLookup().extractIfSuccessful().getTitle();
        layoutCont.contextPut("itemTitle", itemTitle);
        ItemBodyResultFormItem userFormItem =
            new ItemBodyResultFormItem("userResponseItem", resolvedAssessmentItem);
        initInteractionResultFormItem(userFormItem);
        formLayout.add("userResponseItem", userFormItem);
        // solution:
        ItemBodyResultFormItem solutionFormItem =
            new ItemBodyResultFormItem("solutionItem", resolvedAssessmentItem);
        solutionFormItem.setShowSolution(true);
        solutionFormItem.setReport(true);
        solutionFormItem.setVisible(!questionPassed);
        initInteractionResultFormItem(solutionFormItem);
        formLayout.add("solutionItem", solutionFormItem);
      }
    }

    private void initInteractionResultFormItem(ItemBodyResultFormItem formItem) {
      formItem.setHideFeedbacks(true);
      formItem.setItemSessionState(itemSessionState);
      formItem.setCandidateSessionContext(candidateSessionContext);
      formItem.setResourceLocator(inputResourceLocator);
    }

    @Override
    protected void formOK(UserRequest ureq) {
      // unused
    }
  }
}
