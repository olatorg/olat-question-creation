/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.peerreview;

import java.util.Objects;
import lombok.Getter;
import org.olat.core.gui.control.Event;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Getter
public class QuestionAnswerResponseEvent extends Event {

  boolean passed;

  public QuestionAnswerResponseEvent(boolean passed) {
    super("question-answer-response");
    this.passed = passed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    QuestionAnswerResponseEvent that = (QuestionAnswerResponseEvent) o;
    return Objects.equals(passed, that.passed);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), passed);
  }
}
