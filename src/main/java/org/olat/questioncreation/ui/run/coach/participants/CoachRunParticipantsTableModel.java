/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.participants;

import java.util.List;
import java.util.Locale;
import org.olat.core.commons.persistence.SortKey;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiSortableColumnDef;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SortableFlexiTableDataModel;
import org.olat.core.gui.translator.Translator;
import org.olat.questioncreation.service.Participant;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class CoachRunParticipantsTableModel extends DefaultFlexiTableDataModel<Participant>
    implements SortableFlexiTableDataModel<Participant> {

  private final Translator translator;
  private final Locale locale;

  public CoachRunParticipantsTableModel(
      FlexiTableColumnModel columnModel, Translator translator, Locale locale) {
    super(columnModel);
    this.translator = translator;
    this.locale = locale;
  }

  @Override
  public Object getValueAt(int row, int column) {
    Participant participant = getObject(row);
    return getValueAt(participant, column);
  }

  @Override
  public void sort(SortKey sortKey) {
    if (sortKey != null) {
      List<Participant> rows = new CoachRunParticipantsTableSorter(sortKey, this, null).sort();
      super.setObjects(rows);
    }
  }

  @Override
  public Object getValueAt(Participant participant, int column) {
    return switch (Columns.values()[column]) {
      case LOGINNAME -> participant.getUsername();
      case LASTNAME -> participant.getLastname();
      case FIRSTNAME -> participant.getFirstname();
      case MATRICULATION_NR -> participant.getMatriculationNr();
      case STATUS -> participant.getTranslatedQuestionStatus(locale);
      case QUESTION_TYPE -> participant.getTranslatedQuestionType(locale);
      case EXPORT -> participant.getTranslatedExportText(translator);
    };
  }

  public enum Columns implements FlexiSortableColumnDef {
    LOGINNAME("run.coach.participants.table.header.loginname"),
    LASTNAME("run.coach.participants.table.header.lastName"),
    FIRSTNAME("run.coach.participants.table.header.firstName"),
    MATRICULATION_NR("run.coach.participants.table.header.matriculationNumber"),
    STATUS("run.coach.participants.table.header.status"),
    QUESTION_TYPE("run.coach.participants.table.header.questionType"),
    EXPORT("run.coach.participants.table.header.export");

    private final String i18nHeaderKey;

    Columns(String i18nHeaderKey) {
      this.i18nHeaderKey = i18nHeaderKey;
    }

    @Override
    public String i18nHeaderKey() {
      return i18nHeaderKey;
    }

    @Override
    public boolean sortable() {
      return true;
    }

    @Override
    public String sortKey() {
      return name();
    }
  }
}
