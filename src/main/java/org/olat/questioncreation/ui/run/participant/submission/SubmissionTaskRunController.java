/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.submission;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.elements.MultipleSelectionElement;
import org.olat.core.gui.components.form.flexible.elements.SingleSelection;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SelectionEvent;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.components.link.LinkFactory;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.generic.modal.DialogBoxController;
import org.olat.core.gui.control.generic.modal.DialogBoxUIFactory;
import org.olat.core.util.DateUtils;
import org.olat.core.util.Formatter;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.springframework.beans.factory.annotation.Autowired;
import uk.ac.ed.ph.jqtiplus.node.item.AssessmentItem;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Slf4j
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class SubmissionTaskRunController extends FormBasicController {

  private static final String SUBMISSION_CSS_CLASS = "submissionCssClass";
  private static final String ACTION_SELECT = "select";
  private static final String ACTION_RESET = "reset";

  private final CourseEnvironment courseEnvironment;
  private final QuestionCreationCourseNode courseNode;
  private final long creatorIdentityKey;
  private final String repoCourseNodeIdent;
  private FlexiTableElement submissionsTableController;
  private List<SubmissionsTableRow> submissionsTableRows;
  private Link finalConfirmationButton;
  private DialogBoxController finalConfirmationDialog;
  private DialogBoxController resetDialog;
  private ResolvedAssessmentItem resolvedAssessmentItem;
  private MultipleSelectionElement allowExportToPoolSelection;
  private boolean isFinallySubmitted = false;
  @Autowired private AssessmentItemService assessmentItemService;
  @Autowired private SubmissionTaskService submissionTaskService;

  public SubmissionTaskRunController(
      UserRequest ureq,
      WindowControl wControl,
      QuestionCreationCourseNode courseNode,
      CourseEnvironment courseEnvironment,
      String repoCourseNodeIdent,
      long creatorIdentityKey) {
    super(ureq, wControl, "submission_task_run");
    this.courseNode = courseNode;
    this.courseEnvironment = courseEnvironment;
    this.creatorIdentityKey = creatorIdentityKey;
    this.repoCourseNodeIdent = repoCourseNodeIdent;
    initForm(ureq);
  }

  @Override
  protected void initForm(
      FormItemContainer formItemContainer, Controller controller, UserRequest userRequest) {
    if (QuestionCreationUIHelper.isBeforeStartDate(courseNode)) {
      flc.contextPut("isBeforeStartDate", Boolean.TRUE);
      flc.contextPut(
          "isBeforeStartDateMessage",
          translate(
              "run.submission.is.before.task.start",
              QuestionCreationUIHelper.formatDate(
                  courseNode
                      .getModuleConfiguration()
                      .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START),
                  getLocale())));
      flc.contextPut(SUBMISSION_CSS_CLASS, "o_notavailable");
    } else {
      Optional<SubmissionTask> savedSubmissionTask =
          submissionTaskService.findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
              creatorIdentityKey, repoCourseNodeIdent, 0);
      if (savedSubmissionTask.isPresent()) {
        isFinallySubmitted = true;
        flc.contextPut("isFinallySubmitted", Boolean.TRUE);
        flc.contextPut(SUBMISSION_CSS_CLASS, "o_done");
        Date finalSubmissionDate = savedSubmissionTask.get().getFinalSubmitDate();
        flc.contextPut(
            "finalSubmissionDate",
            QuestionCreationUIHelper.formatDate(finalSubmissionDate, getLocale()));
      }
      boolean isAfterTaskDueDate = QuestionCreationUIHelper.isAfterTaskDueDate(courseNode);
      if (isAfterTaskDueDate) {
        flc.contextPut("isAfterTaskDueDate", Boolean.TRUE);
        flc.contextPut(
            "submissionDueDate",
            QuestionCreationUIHelper.formatDate(
                courseNode
                    .getModuleConfiguration()
                    .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE),
                getLocale()));
        if (!isFinallySubmitted) {
          flc.contextPut(SUBMISSION_CSS_CLASS, "o_notavailable");
          flc.contextPut(
              "isAfterTaskDueDateMessage", translate("run.submission.is.after.task.due.date"));
        }
      } else {
        Date dueDate =
            courseNode
                .getModuleConfiguration()
                .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
        String daysUntilDueDate =
            String.valueOf(Math.abs(DateUtils.countDays(dueDate, new Date())));
        Formatter formatter = Formatter.getInstance(getLocale());
        String dueDateString = formatter.formatDateAndTime(dueDate);
        String dayOfWeek = formatter.dayOfWeekName(dueDate);
        flc.contextPut(
            "submissionDueDateHeaderMessage",
            translate(
                "run.submission.due.date.message", daysUntilDueDate, dayOfWeek, dueDateString));
      }
      initSubmissionsTable(formItemContainer);
      String[] onKeys = new String[] {"on"};
      String[] onValues = new String[] {translate("run.submission.allow.export.checkbox.label")};
      allowExportToPoolSelection =
          uifactory.addCheckboxesHorizontal(
              "run.submission.allow.export.checkbox", null, formItemContainer, onKeys, onValues);
      allowExportToPoolSelection.select("on", true);
      allowExportToPoolSelection.addActionListener(FormEvent.ONCHANGE);
      finalConfirmationButton =
          LinkFactory.createButton(
              "run.submission.final.confirmation.button", flc.getFormItemComponent(), this);
      if (!isFinallySubmitted && !isAfterTaskDueDate) {
        flc.contextPut(SUBMISSION_CSS_CLASS, "o_active");
      }
    }
  }

  private void initSubmissionsTable(FormItemContainer formItemContainer) {
    FlexiTableColumnModel columnsModel = FlexiTableDataModelFactory.createFlexiTableColumnModel();
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(SubmissionsTableDataModel.SubmissionsTableColumns.qtiType));
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            SubmissionsTableDataModel.SubmissionsTableColumns.status, ACTION_SELECT));
    if (!isFinallySubmitted) {
      columnsModel.addFlexiColumnModel(
          new DefaultFlexiColumnModel(
              SubmissionsTableDataModel.SubmissionsTableColumns.resetMessage, ACTION_RESET));
    }
    SubmissionsTableDataModel submissionsTableDataModel =
        new SubmissionsTableDataModel(columnsModel);
    submissionsTableRows = getSubmissionTableRows(formItemContainer);
    submissionsTableDataModel.setObjects(submissionsTableRows);
    submissionsTableController =
        uifactory.addTableElement(
            getWindowControl(),
            "submissionsTable",
            submissionsTableDataModel,
            getTranslator(),
            formItemContainer);
    submissionsTableController.setCustomizeColumns(false);
    submissionsTableController.setNumOfRowsEnabled(false);
  }

  private List<SubmissionsTableRow> getSubmissionTableRows(FormItemContainer formItemContainer) {
    List<SubmissionsTableRow> rows = new ArrayList<>();
    List<String> allowedQTITypeKeys =
        courseNode
            .getModuleConfiguration()
            .getList(QuestionCreationCourseNode.CFG_KEY_TASK_QTI_TYPES, String.class);
    List<String> allowedQTITypeLabels =
        allowedQTITypeKeys.stream()
            .map(
                type ->
                    QuestionCreationUIHelper.getLabelForQTIQuestion(
                        QTI21QuestionType.valueOf(type), getLocale()))
            .toList();
    String[] keys = allowedQTITypeKeys.toArray(new String[0]);
    String[] labels = allowedQTITypeLabels.toArray(new String[0]);

    SingleSelection qtiTypeDropdown =
        uifactory.addDropdownSingleselect(
            "run.submission.qti.type.dropdown", formItemContainer, keys, labels);
    qtiTypeDropdown.setAllowNoSelection(true);
    qtiTypeDropdown.enableNoneSelection(translate("run.submission.qti.dropdown.none_selection"));
    qtiTypeDropdown.addActionListener(FormEvent.ONCHANGE);
    String statusMessage = translate("run.submission.table.row.status.todo");

    resolvedAssessmentItem =
        assessmentItemService.getSavedAssessmentItem(
            courseEnvironment, courseNode, creatorIdentityKey);
    // if assessment item was already created, disable dropdown, pre-select the QTI type, and change
    // the status:
    if (resolvedAssessmentItem != null) {
      qtiTypeDropdown.setEnabled(false);
      AssessmentItem assessmentItem =
          resolvedAssessmentItem.getItemLookup().getRootNodeHolder().getRootNode();
      String identifier = assessmentItem.getIdentifier();
      for (String key : keys) {
        if (identifier.startsWith(key)) {
          qtiTypeDropdown.select(key, true);
        }
      }
      if (isFinallySubmitted) {
        statusMessage = translate("run.submission.table.row.status.done_final");
      } else {
        statusMessage = translate("run.submission.table.row.status.done_unconfirmed");
      }
    }
    SubmissionsTableRow row =
        new SubmissionsTableRow(
            qtiTypeDropdown, statusMessage, translate("run.submission.table.row.reset"));

    rows.add(row);
    return rows;
  }

  @Override
  public void event(UserRequest userRequest, Component sourceComponent, Event event) {
    if (event instanceof SelectionEvent selectionEvent) {
      FormItem formItem = selectionEvent.getFormItemSource();
      String command = selectionEvent.getCommand();
      if (formItem == submissionsTableController) {
        if (command.equals(ACTION_SELECT)) {
          int tableRowIndex = selectionEvent.getIndex();
          SingleSelection qtiTypeSelection = submissionsTableRows.get(tableRowIndex).qtiType();
          if (qtiTypeSelection.isOneSelected()) {
            String selectedKey = qtiTypeSelection.getSelectedKey();
            fireEvent(userRequest, new SelectSubmissionTableEvent(selectedKey, isFinallySubmitted));
          } else {
            showWarning("run.submission.table.missing.qtiType");
          }
        } else if (command.equals(ACTION_RESET)) {
          int tableRowIndex = selectionEvent.getIndex();
          SingleSelection qtiTypeSelection = submissionsTableRows.get(tableRowIndex).qtiType();
          if (qtiTypeSelection.isOneSelected()) {
            openResetDialog(userRequest);
          } else {
            showWarning("run.submission.table.missing.qtiType");
          }
        }
      }
    } else if (sourceComponent == finalConfirmationButton) {
      if (resolvedAssessmentItem != null
          && resolvedAssessmentItem.getItemLookup().wasSuccessful()) {
        openFinalConfirmationDialog(userRequest);
      } else {
        showWarning("run.submission.final.confirmation.error.missing.submission");
      }
    }
  }

  private void openFinalConfirmationDialog(UserRequest ureq) {
    if (finalConfirmationDialog != null) {
      finalConfirmationDialog.dispose();
    }
    List<String> buttonLabels = new ArrayList<>();
    buttonLabels.add(translate("run.submission.final.confirmation.dialog.ok"));
    buttonLabels.add(translate("run.submission.final.confirmation.dialog.cancel"));
    finalConfirmationDialog =
        activateGenericDialog(
            ureq,
            translate("run.submission.final.confirmation.dialog.title"),
            translate("run.submission.final.confirmation.dialog.text"),
            buttonLabels,
            finalConfirmationDialog);
  }

  private void openResetDialog(UserRequest ureq) {
    if (resetDialog != null) {
      resetDialog.dispose();
    }
    List<String> buttonLabels = new ArrayList<>();
    buttonLabels.add(translate("run.submission.reset.dialog.ok"));
    buttonLabels.add(translate("run.submission.reset.dialog.cancel"));
    resetDialog =
        activateGenericDialog(
            ureq,
            translate("run.submission.reset.dialog.title"),
            translate("run.submission.reset.dialog.text"),
            buttonLabels,
            resetDialog);
  }

  @Override
  public void event(UserRequest userRequest, Controller sourceController, Event event) {
    if (sourceController == finalConfirmationDialog && DialogBoxUIFactory.isYesEvent(event)) {
      isFinallySubmitted = true;
      Date finalSubmitDate = new Date();
      boolean isExportToPoolAllowed = allowExportToPoolSelection.isAtLeastSelected(1);
      submissionTaskService.save(
          creatorIdentityKey, repoCourseNodeIdent, 0, finalSubmitDate, isExportToPoolAllowed);
    } else if (sourceController == resetDialog && DialogBoxUIFactory.isYesEvent(event)) {
      assessmentItemService.deleteSavedAssessmentItem(
          courseEnvironment, courseNode, creatorIdentityKey);
    }
    initForm(userRequest);
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    // not used
  }
}
