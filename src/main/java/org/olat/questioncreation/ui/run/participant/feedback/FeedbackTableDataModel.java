/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.feedback;

import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiColumnDef;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@SuppressWarnings("java:S115")
public class FeedbackTableDataModel extends DefaultFlexiTableDataModel<FeedbackTableRow> {

  public FeedbackTableDataModel(FlexiTableColumnModel columnModel) {
    super(columnModel);
  }

  @Override
  public Object getValueAt(int r, int c) {
    FeedbackTableRow row = getObject(r);
    return switch (FeedbackTableColumns.values()[c]) {
      case feedback -> row.feedback();
      case status -> row.status();
    };
  }

  public enum FeedbackTableColumns implements FlexiColumnDef {
    feedback("run.feedback.table.header.feedback"),
    status("run.feedback.table.header.status");

    private final String i18nKey;

    FeedbackTableColumns(String i18nKey) {
      this.i18nKey = i18nKey;
    }

    @Override
    public String i18nHeaderKey() {
      return i18nKey;
    }
  }
}
