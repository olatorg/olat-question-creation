/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FlexiTableElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.DefaultFlexiTableDataModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiColumnDef;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableDataModelFactory;
import org.olat.core.gui.components.form.flexible.impl.elements.table.SelectionEvent;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.DateUtils;
import org.olat.core.util.Formatter;
import org.olat.course.run.environment.CourseEnvironment;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.data.entity.SubmissionTask;
import org.olat.questioncreation.service.AssessmentItemService;
import org.olat.questioncreation.service.SubmissionTaskService;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;
import org.olat.questioncreation.ui.run.participant.submission.SelectSubmissionTableEvent;
import org.olat.questioncreation.ui.run.participant.submission.SubmissionTaskRunController;
import org.springframework.beans.factory.annotation.Autowired;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;

/**
 * @author Oliver Bühler
 * @since 1.0
 *     <p>Inspired by and copied part of {@link SubmissionTaskRunController}
 */
@Slf4j
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CoachParticipantSubmissionController extends FormBasicController {

  private static final String ACTION_SELECT = "select";
  private static final String CSS_CLASS_TEMPLATE_KEY = "submissionCssClass";
  private static final String CSS_CLASS_NOT_AVAILABLE = "o_notavailable";
  private static final String CSS_CLASS_ACTIVE = "o_active";
  private static final String CSS_CLASS_DONE = "o_done";

  private final QuestionCreationCourseNode courseNode;
  private final CourseEnvironment courseEnvironment;
  private final String repoCourseNodeIdent;
  private final long identityKey;

  private boolean isFinallySubmitted = false;
  private FlexiTableElement submissionsTableController;
  private List<CoachSubmissionsTableRow> submissionsTableRows;

  @Autowired private AssessmentItemService assessmentItemService;
  @Autowired private SubmissionTaskService submissionTaskService;

  protected CoachParticipantSubmissionController(
      UserRequest ureq,
      WindowControl wControl,
      CourseEnvironment courseEnvironment,
      QuestionCreationCourseNode courseNode,
      String repoCourseNodeIdent,
      long identityKey) {
    super(ureq, wControl, "coach_participant_submission");
    this.courseNode = courseNode;
    this.courseEnvironment = courseEnvironment;
    this.repoCourseNodeIdent = repoCourseNodeIdent;
    this.identityKey = identityKey;
    initForm(ureq);
  }

  @Override
  @SuppressWarnings("DuplicatedCode")
  protected void initForm(
      FormItemContainer formItemContainer, Controller controller, UserRequest userRequest) {
    if (QuestionCreationUIHelper.isBeforeStartDate(courseNode)) {
      flc.contextPut("isBeforeStartDate", Boolean.TRUE);
      flc.contextPut(
          "isBeforeStartDateMessage",
          translate(
              "run.submission.is.before.task.start",
              QuestionCreationUIHelper.formatDate(
                  courseNode
                      .getModuleConfiguration()
                      .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START),
                  getLocale())));
      flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_NOT_AVAILABLE);
    } else {
      Optional<SubmissionTask> savedSubmissionTask =
          submissionTaskService.findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
              identityKey, repoCourseNodeIdent, 0);
      if (savedSubmissionTask.isPresent()) {
        isFinallySubmitted = true;
        flc.contextPut("isFinallySubmitted", Boolean.TRUE);
        flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_DONE);
        Date finalSubmissionDate = savedSubmissionTask.get().getFinalSubmitDate();
        flc.contextPut(
            "finalSubmissionDate",
            QuestionCreationUIHelper.formatDate(finalSubmissionDate, getLocale()));
      }
      boolean isAfterTaskDueDate = QuestionCreationUIHelper.isAfterTaskDueDate(courseNode);
      if (isAfterTaskDueDate) {
        flc.contextPut("isAfterTaskDueDate", Boolean.TRUE);
        flc.contextPut(
            "submissionDueDate",
            QuestionCreationUIHelper.formatDate(
                courseNode
                    .getModuleConfiguration()
                    .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE),
                getLocale()));
        if (!isFinallySubmitted) {
          flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_NOT_AVAILABLE);
          flc.contextPut(
              "isAfterTaskDueDateMessage", translate("run.submission.is.after.task.due.date"));
        }
      } else {
        Date dueDate =
            courseNode
                .getModuleConfiguration()
                .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
        String daysUntilDueDate =
            String.valueOf(Math.abs(DateUtils.countDays(dueDate, new Date())));
        Formatter formatter = Formatter.getInstance(getLocale());
        String dueDateString = formatter.formatDateAndTime(dueDate);
        String dayOfWeek = formatter.dayOfWeekName(dueDate);
        flc.contextPut(
            "submissionDueDateHeaderMessage",
            translate(
                "run.submission.due.date.message", daysUntilDueDate, dayOfWeek, dueDateString));
      }
      initSubmissionsTable(formItemContainer);
      if (!isFinallySubmitted && !isAfterTaskDueDate) {
        flc.contextPut(CSS_CLASS_TEMPLATE_KEY, CSS_CLASS_ACTIVE);
      }
    }
  }

  private void initSubmissionsTable(FormItemContainer formItemContainer) {
    FlexiTableColumnModel columnsModel = FlexiTableDataModelFactory.createFlexiTableColumnModel();
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            CoachSubmissionsTableDataModel.CoachSubmissionsTableColumns.qtiType));
    columnsModel.addFlexiColumnModel(
        new DefaultFlexiColumnModel(
            CoachSubmissionsTableDataModel.CoachSubmissionsTableColumns.status, ACTION_SELECT));
    CoachSubmissionsTableDataModel coachSubmissionsTableDataModel =
        new CoachSubmissionsTableDataModel(columnsModel);
    submissionsTableRows = getSubmissionTableRows();
    coachSubmissionsTableDataModel.setObjects(submissionsTableRows);
    submissionsTableController =
        uifactory.addTableElement(
            getWindowControl(),
            "submissionsTable",
            coachSubmissionsTableDataModel,
            getTranslator(),
            formItemContainer);
    submissionsTableController.setCustomizeColumns(false);
    submissionsTableController.setNumOfRowsEnabled(false);
  }

  private List<CoachSubmissionsTableRow> getSubmissionTableRows() {
    List<CoachSubmissionsTableRow> rows = new ArrayList<>();
    ResolvedAssessmentItem resolvedAssessmentItem =
        assessmentItemService.getSavedAssessmentItem(courseEnvironment, courseNode, identityKey);
    if (resolvedAssessmentItem != null) {
      QTI21QuestionType qti21QuestionType =
          QTI21QuestionType.getType(
              resolvedAssessmentItem.getRootNodeLookup().getRootNodeHolder().getRootNode());
      Optional<SubmissionTask> savedSubmissionTask =
          submissionTaskService.findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(
              identityKey, repoCourseNodeIdent, 0);
      String statusMessage;
      if (savedSubmissionTask.isPresent()) {
        statusMessage = translate("run.submission.table.row.status.done_final");
      } else {
        statusMessage = translate("run.submission.table.row.status.done_unconfirmed");
      }
      CoachSubmissionsTableRow row = new CoachSubmissionsTableRow(qti21QuestionType, statusMessage);
      rows.add(row);
    }
    return rows;
  }

  @Override
  public void event(UserRequest ureq, Component source, Event event) {
    if (event instanceof SelectionEvent selectionEvent) {
      FormItem formItem = selectionEvent.getFormItemSource();
      String command = selectionEvent.getCommand();
      if (formItem == submissionsTableController && command.equals(ACTION_SELECT)) {
        int tableRowIndex = selectionEvent.getIndex();
        QTI21QuestionType qti21QuestionType = submissionsTableRows.get(tableRowIndex).qtiType();
        fireEvent(ureq, new SelectSubmissionTableEvent(qti21QuestionType.getPrefix(), true));
      }
    }
  }

  @Override
  protected void formOK(UserRequest userRequest) {
    // not used
  }

  @SuppressWarnings("java:S115")
  private final class CoachSubmissionsTableDataModel
      extends DefaultFlexiTableDataModel<CoachSubmissionsTableRow> {

    public CoachSubmissionsTableDataModel(FlexiTableColumnModel columnModel) {
      super(columnModel);
    }

    @Override
    public Object getValueAt(int r, int c) {
      CoachSubmissionsTableRow row = getObject(r);
      return switch (CoachSubmissionsTableColumns.values()[c]) {
        case qtiType ->
            QuestionCreationUIHelper.getLabelForQTIQuestion(
                row.qtiType(), CoachParticipantSubmissionController.this.getLocale());
        case status -> row.statusMessage();
      };
    }

    private enum CoachSubmissionsTableColumns implements FlexiColumnDef {
      qtiType("run.submission.table.header.qtiType"),
      status("run.submission.table.header.status");

      private final String i18nKey;

      CoachSubmissionsTableColumns(String i18nKey) {
        this.i18nKey = i18nKey;
      }

      @Override
      public String i18nHeaderKey() {
        return i18nKey;
      }
    }
  }

  private record CoachSubmissionsTableRow(QTI21QuestionType qtiType, String statusMessage) {}
}
