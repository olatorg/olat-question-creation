/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.submission;

import lombok.Getter;
import org.olat.core.gui.control.Event;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Getter
public class SelectSubmissionTableEvent extends Event {

  private final String selectedQtiType;
  private final boolean isFinallySubmitted;

  public SelectSubmissionTableEvent(String selectedQtiType, boolean isFinallySubmitted) {
    super("select-submission");
    this.selectedQtiType = selectedQtiType;
    this.isFinallySubmitted = isFinallySubmitted;
  }
}
