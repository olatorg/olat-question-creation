/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.administration;

import java.util.Date;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.questioncreation.QuestionCreationCourseNode;
import org.olat.questioncreation.ui.QuestionCreationUIHelper;

/**
 * @author Mateja Culjak
 * @since 1.1
 */
public class CoachRunAdministrationDeadlinesController extends FormBasicController {

  private final QuestionCreationCourseNode courseNode;

  protected CoachRunAdministrationDeadlinesController(
      UserRequest userRequest, WindowControl windowControl, QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    this.courseNode = courseNode;
    initForm(userRequest);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    setFormTitle("run.coach.administration.deadlines.header");
    setFormContextHelp("uzh_additions/question_creation");
    addTaskStartDateElement(formLayout);
    addSubmissionDueDateElement(formLayout);
    addPeerReviewDueDateElement(formLayout);
  }

  public void addTaskStartDateElement(FormItemContainer formLayout) {
    Date taskStartDate =
        courseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_START);
    if (taskStartDate != null) {
      String translatedTaskStartDateMessage =
          translate(
              "run.coach.administration.deadlines.taskStartDate.text",
              QuestionCreationUIHelper.formatDate(taskStartDate, getLocale()));
      uifactory.addStaticTextElement(
          "run.coach.administration.deadlines.taskStartDate.label",
          translatedTaskStartDateMessage,
          formLayout);
    }
  }

  public void addSubmissionDueDateElement(FormItemContainer formLayout) {
    Date submissionDueDate =
        courseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_TASK_DATE_DUE);
    if (submissionDueDate != null) {
      String translatedSubmissionDueDateMessage =
          translate(
              "run.coach.administration.deadlines.submissionDueDate.text",
              QuestionCreationUIHelper.formatDate(submissionDueDate, getLocale()));
      uifactory.addStaticTextElement(
          "run.coach.administration.deadlines.submissionDueDate.label",
          translatedSubmissionDueDateMessage,
          formLayout);
    }
  }

  public void addPeerReviewDueDateElement(FormItemContainer formLayout) {
    Date peerReviewDueDate =
        courseNode
            .getModuleConfiguration()
            .getDateValue(QuestionCreationCourseNode.CFG_KEY_PEERREVIEW_DATE_DUE);
    if (peerReviewDueDate != null) {
      String translatedPeerReviewDueDateMessage =
          translate(
              "run.coach.administration.deadlines.peerReviewDueDate.text",
              QuestionCreationUIHelper.formatDate(peerReviewDueDate, getLocale()));
      uifactory.addStaticTextElement(
          "run.coach.administration.deadlines.peerReviewDueDate.label",
          translatedPeerReviewDueDateMessage,
          formLayout);
    }
  }

  @Override
  protected void formOK(UserRequest ureq) {
    // unused
  }
}
