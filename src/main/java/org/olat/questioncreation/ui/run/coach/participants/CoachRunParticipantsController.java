/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.participants;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class CoachRunParticipantsController extends BasicController {

  private final CoachRunParticipantsListController coachParticipantListController;

  public CoachRunParticipantsController(
      UserRequest ureq,
      WindowControl wControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(ureq, wControl);
    VelocityContainer vcMain = createVelocityContainer("coach_participants");

    coachParticipantListController =
        new CoachRunParticipantsListController(ureq, wControl, userCourseEnvironment, courseNode);
    vcMain.put(
        "coachParticipantListController", coachParticipantListController.getInitialComponent());

    this.putInitialPanel(vcMain);
  }

  public void refreshData(UserRequest userRequest) {
    coachParticipantListController.refreshData(userRequest);
  }

  @Override
  protected void event(UserRequest userRequest, Component component, Event event) {
    // not used
  }
}
