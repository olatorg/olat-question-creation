/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.coach.administration;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.Component;
import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.course.run.userview.UserCourseEnvironment;
import org.olat.questioncreation.QuestionCreationCourseNode;

/**
 * @author Oliver Bühler
 * @since 1.0
 */
public class CoachRunAdministrationController extends BasicController {

  public CoachRunAdministrationController(
      UserRequest userRequest,
      WindowControl windowControl,
      UserCourseEnvironment userCourseEnvironment,
      QuestionCreationCourseNode courseNode) {
    super(userRequest, windowControl);
    VelocityContainer container = createVelocityContainer("coach_administration");

    CoachRunAdministrationDeadlinesController coachRunAdministrationDeadlinesController =
        new CoachRunAdministrationDeadlinesController(userRequest, windowControl, courseNode);
    listenTo(coachRunAdministrationDeadlinesController);
    container.put("deadlines", coachRunAdministrationDeadlinesController.getInitialComponent());

    CoachRunAdministrationTaskController coachRunAdministrationTaskController =
        new CoachRunAdministrationTaskController(userRequest, windowControl, courseNode);
    listenTo(coachRunAdministrationTaskController);
    container.put("task", coachRunAdministrationTaskController.getInitialComponent());

    CoachRunAdministrationPeerReviewController coachRunAdministrationPeerReviewController =
        new CoachRunAdministrationPeerReviewController(
            userRequest, windowControl, userCourseEnvironment, courseNode);
    listenTo(coachRunAdministrationPeerReviewController);
    container.put("peerReview", coachRunAdministrationPeerReviewController.getInitialComponent());

    this.putInitialPanel(container);
  }

  @Override
  protected void event(UserRequest userRequest, Component component, Event event) {
    // not used
  }
}
