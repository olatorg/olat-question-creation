/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.submission;

import java.io.File;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItem;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.elements.FormLink;
import org.olat.core.gui.components.form.flexible.elements.RichTextElement;
import org.olat.core.gui.components.form.flexible.elements.TextElement;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.components.form.flexible.impl.FormEvent;
import org.olat.core.gui.components.form.flexible.impl.FormLayoutContainer;
import org.olat.core.gui.components.form.flexible.impl.elements.richText.TextMode;
import org.olat.core.gui.components.link.Link;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.Event;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.CodeHelper;
import org.olat.core.util.Formatter;
import org.olat.core.util.StringHelper;
import org.olat.core.util.Util;
import org.olat.core.util.vfs.VFSContainer;
import org.olat.ims.qti21.model.QTI21QuestionType;
import org.olat.ims.qti21.ui.ResourcesMapper;
import org.olat.ims.qti21.ui.components.FlowFormItem;
import org.olat.ims.qti21.ui.editor.events.AssessmentItemEvent;
import org.olat.ims.qti21.ui.editor.interactions.EssayEditorController;

/**
 * Adapted from @see {@link org.olat.ims.qti21.ui.editor.interactions.EssayEditorController}
 *
 * <p>- added sampleSolutionTextEl
 *
 * <p>- removed lengthEl, heightEl, minWordsEl, maxWordsEl, copyPasteEl, placeholderEl,
 * textFormattingEl
 *
 * <p>- rich text formatting as default
 *
 * <p>- save and back button
 *
 * @author Mateja Culjak
 * @since 1.1
 */
public class QuestionCreationEssayEditorController extends FormBasicController {

  private static final String MANDATORY_FIELD_KEY = "form.legende.mandatory";

  private TextElement titleEl;
  private RichTextElement textEl;
  private RichTextElement sampleSolutionTextEl;

  private final File itemFile;
  private final File rootDirectory;
  private final VFSContainer rootContainer;

  private final boolean readOnly;
  private final boolean restrictedEdit;
  private final String mapperUri;
  private final QuestionCreationEssayAssessmentItemBuilder itemBuilder;

  public QuestionCreationEssayEditorController(
      UserRequest ureq,
      WindowControl wControl,
      QuestionCreationEssayAssessmentItemBuilder itemBuilder,
      File rootDirectory,
      VFSContainer rootContainer,
      File itemFile,
      boolean restrictedEdit,
      boolean readOnly) {
    super(ureq, wControl, LAYOUT_DEFAULT_2_10);
    setTranslator(
        Util.createPackageTranslator(
            QuestionCreationEssayEditorController.class, EssayEditorController.class, getLocale()));
    this.itemFile = itemFile;
    this.itemBuilder = itemBuilder;
    this.rootDirectory = rootDirectory;
    this.rootContainer = rootContainer;
    this.readOnly = readOnly;
    this.restrictedEdit = restrictedEdit;

    mapperUri =
        registerCacheableMapper(
            null,
            "EssayEditorController::" + CodeHelper.getRAMUniqueID(),
            new ResourcesMapper(itemFile.toURI(), rootDirectory));
    initForm(ureq);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    setFormContextHelp("manual_user/learningresources/Configure_test_questions/");

    titleEl =
        uifactory.addTextElement("title", "form.imd.title", -1, itemBuilder.getTitle(), formLayout);
    titleEl.setElementCssClass("o_sel_assessment_item_title");
    titleEl.setMandatory(true);
    titleEl.setEnabled(!readOnly);

    String description = itemBuilder.getQuestion();
    textEl =
        uifactory.addRichTextElementForStringData(
            "desc",
            "form.imd.descr",
            description,
            12,
            -1,
            false,
            null,
            null,
            formLayout,
            ureq.getUserSession(),
            getWindowControl());
    textEl.setElementCssClass("o_sel_assessment_item_question");
    textEl.setMandatory(true);
    textEl.setEnabled(!readOnly);
    textEl.setVisible(!readOnly);
    if (readOnly) {
      FlowFormItem textReadOnlyEl = new FlowFormItem("descro", itemFile);
      textReadOnlyEl.setLabel("form.imd.descr", null);
      textReadOnlyEl.setBlocks(itemBuilder.getQuestionBlocks());
      textReadOnlyEl.setMapperUri(mapperUri);
      formLayout.add(textReadOnlyEl);
    }

    String sampleSolutionText = itemBuilder.getLabel();
    sampleSolutionTextEl =
        uifactory.addRichTextElementForStringData(
            "sampleSolutionText",
            "fib.sampleSolutionText",
            sampleSolutionText,
            12,
            -1,
            false,
            null,
            null,
            formLayout,
            ureq.getUserSession(),
            getWindowControl());
    sampleSolutionTextEl.getEditorConfiguration().setSimplestTextModeAllowed(TextMode.multiLine);
    sampleSolutionTextEl.setMandatory(true);
    sampleSolutionTextEl.setEnabled(!readOnly);
    sampleSolutionTextEl.setVisible(!readOnly);
    if (readOnly) {
      String sampleSolutionWithLatexFormattedFormulas =
          Formatter.formatLatexFormulas(sampleSolutionText);
      uifactory.addStaticTextElement(
          "fib.sampleSolutionText", sampleSolutionWithLatexFormattedFormulas, formLayout);
    }

    // Submit Button
    FormLayoutContainer buttonsContainer =
        FormLayoutContainer.createButtonLayout("buttons", getTranslator());
    buttonsContainer.setRootForm(mainForm);
    buttonsContainer.setElementCssClass("o_sel_lob_save");
    buttonsContainer.setVisible(!readOnly);
    formLayout.add(buttonsContainer);
    if (!readOnly) {
      FormLayoutContainer horizontalFormLayout =
          uifactory.addHorizontalFormLayout("buttons-horizontal", null, buttonsContainer);
      uifactory.addFormSubmitButton("submit", horizontalFormLayout);
      FormLink saveAndBackButton =
          uifactory.addFormLink("saveAndBack", null, null, horizontalFormLayout, Link.BUTTON);
      saveAndBackButton.setPrimary(true);
      saveAndBackButton.setTranslator(
          Util.createPackageTranslator(
              QuestionCreationSingleChoiceEditorController.class, getLocale()));
      saveAndBackButton.setI18nKey("run.qti.question.editor.save_and_back");
    }
  }

  private String getValue(Integer integer) {
    return integer == null ? "" : integer.toString();
  }

  public Integer getValue(TextElement integerEl) {
    String val = integerEl.getValue();
    Integer integer = null;
    if (isInteger(val)) {
      return Integer.parseInt(val);
    }
    return integer;
  }

  @Override
  protected boolean validateFormLogic(UserRequest ureq) {
    boolean allOk = super.validateFormLogic(ureq);

    titleEl.clearError();
    if (!StringHelper.containsNonWhitespace(titleEl.getValue())) {
      titleEl.setErrorKey(MANDATORY_FIELD_KEY);
      allOk = false;
    }

    textEl.clearError();
    if (!StringHelper.containsNonWhitespace(textEl.getValue())) {
      textEl.setErrorKey(MANDATORY_FIELD_KEY);
      allOk = false;
    }

    sampleSolutionTextEl.clearError();
    if (!StringHelper.containsNonWhitespace(sampleSolutionTextEl.getValue())) {
      sampleSolutionTextEl.setErrorKey(MANDATORY_FIELD_KEY);
      allOk = false;
    }

    return allOk;
  }

  private boolean validateInteger(TextElement integerEl) {
    boolean allOk = true;

    integerEl.clearError();
    if (StringHelper.containsNonWhitespace(integerEl.getValue())) {
      if (!StringHelper.isLong(integerEl.getValue())) {
        integerEl.setErrorKey("form.error.nointeger");
        allOk &= false;
      } else if (!isInteger(integerEl.getValue())) {
        integerEl.setErrorKey("error.integer.positive");
        allOk &= false;
      }
    }

    return allOk;
  }

  private boolean isInteger(String val) {
    if (StringHelper.isLong(val)) {
      try {
        int num = Integer.parseInt(val);
        return num >= 0;
      } catch (NumberFormatException e) {
        return false;
      }
    }
    return false;
  }

  @Override
  protected void formOK(UserRequest ureq) {
    if (readOnly) return;
    // title
    itemBuilder.setTitle(titleEl.getValue());
    // question
    String questionText = textEl.getValue();
    itemBuilder.setQuestion(questionText);
    // rich text als default for OLAT question creation
    itemBuilder.setRichTextFormating(true);
    // sample solution - saved in label field of the ExtendedTextInteraction, since this is the only
    // unused string field in the essay type
    itemBuilder.setLabel(sampleSolutionTextEl.getValue());

    fireEvent(
        ureq,
        new AssessmentItemEvent(
            AssessmentItemEvent.ASSESSMENT_ITEM_CHANGED,
            itemBuilder.getAssessmentItem(),
            QTI21QuestionType.essay));
  }

  @Override
  protected void formInnerEvent(UserRequest ureq, FormItem source, FormEvent event) {
    if (source instanceof FormLink button) {
      String cmd = button.getCmd();
      if ("saveAndBack".equals(cmd) && validateFormLogic(ureq)) {
        formOK(ureq);
        fireEvent(ureq, Event.BACK_EVENT);
      }
    }
    super.formInnerEvent(ureq, source, event);
  }
}
