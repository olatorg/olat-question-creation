/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.ui.run.participant.submission;

import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.form.flexible.impl.FormBasicController;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.Formatter;
import org.olat.ims.qti21.model.QTI21QuestionType;
import uk.ac.ed.ph.jqtiplus.node.item.interaction.ExtendedTextInteraction;
import uk.ac.ed.ph.jqtiplus.node.item.interaction.Interaction;
import uk.ac.ed.ph.jqtiplus.resolution.ResolvedAssessmentItem;

/**
 * @author Mateja Culjak
 * @since 1.1
 */
public class AssessmentItemSampleSolutionController extends FormBasicController {

  private final ResolvedAssessmentItem resolvedAssessmentItem;
  private final QTI21QuestionType questionType;

  protected AssessmentItemSampleSolutionController(
      UserRequest userRequest,
      WindowControl windowControl,
      ResolvedAssessmentItem resolvedAssessmentItem,
      QTI21QuestionType questionType) {
    super(userRequest, windowControl);
    this.resolvedAssessmentItem = resolvedAssessmentItem;
    this.questionType = questionType;
    initForm(userRequest);
  }

  @Override
  protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
    if (QTI21QuestionType.essay.equals(questionType)) {
      List<Interaction> interactions =
          resolvedAssessmentItem
              .getItemLookup()
              .extractIfSuccessful()
              .getItemBody()
              .findInteractions();
      for (Interaction interaction : interactions) {
        if (interaction instanceof ExtendedTextInteraction extendedTextInteraction) {
          String sampleSolutionText = extendedTextInteraction.getLabel();
          String sampleSolutionWithLatexFormattedFormulas =
              Formatter.formatLatexFormulas(sampleSolutionText);
          uifactory.addStaticTextElement(
              "fib.sampleSolutionText", sampleSolutionWithLatexFormattedFormulas, formLayout);
        }
      }
    }
  }

  @Override
  protected void formOK(UserRequest ureq) {
    // unused
  }
}
