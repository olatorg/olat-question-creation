/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation;

import org.olat.core.configuration.AbstractSpringModule;
import org.olat.core.util.coordinate.CoordinatorManager;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@SuppressWarnings("LombokGetterMayBeUsed")
public class QuestionCreationModule extends AbstractSpringModule {

  private static final String QUESTION_CREATION_ENABLED_PROPERTY = "questionCreation.enabled";
  private static final String QUESTION_CREATION_LIMITED_ACCESS_ENABLED_PROPERTY =
      "questionCreation.limitedAccess.enabled";
  private static final String QUESTION_CREATION_LIMITED_ACCESS_GROUP_KEY_PROPERTY =
      "questionCreation.limitedAccessGroupKey";

  private boolean enabled;
  private boolean limitedAccessEnabled;
  private Long limitedAccessGroupKey;

  public QuestionCreationModule(CoordinatorManager coordinatorManager) {
    super(coordinatorManager);
  }

  @Override
  public void init() {
    enabled =
        Boolean.parseBoolean(getStringPropertyValue(QUESTION_CREATION_ENABLED_PROPERTY, "true"));
    limitedAccessEnabled =
        Boolean.parseBoolean(
            getStringPropertyValue(QUESTION_CREATION_LIMITED_ACCESS_ENABLED_PROPERTY, "false"));
    limitedAccessGroupKey = getLongProperty(QUESTION_CREATION_LIMITED_ACCESS_GROUP_KEY_PROPERTY);
  }

  @Override
  protected void initFromChangedProperties() {
    init();
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
    setBooleanProperty(QUESTION_CREATION_ENABLED_PROPERTY, enabled, true);
  }

  public boolean isLimitedAccessEnabled() {
    return limitedAccessEnabled;
  }

  public void setLimitedAccessEnabled(boolean limitedAccessEnabled) {
    this.limitedAccessEnabled = limitedAccessEnabled;
    setBooleanProperty(
        QUESTION_CREATION_LIMITED_ACCESS_ENABLED_PROPERTY, limitedAccessEnabled, true);
  }

  public Long getLimitedAccessGroupKey() {
    return limitedAccessGroupKey;
  }

  public void setLimitedAccessGroupKey(Long limitedAccessGroupKey) {
    this.limitedAccessGroupKey = limitedAccessGroupKey;
    setLongProperty(
        QUESTION_CREATION_LIMITED_ACCESS_GROUP_KEY_PROPERTY, limitedAccessGroupKey, true);
  }

  public void removeLimitedAccessGroupKey() {
    this.removeProperty(QUESTION_CREATION_LIMITED_ACCESS_GROUP_KEY_PROPERTY, true);
  }
}
