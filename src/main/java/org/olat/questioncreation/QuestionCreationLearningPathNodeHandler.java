/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.util.nodes.INode;
import org.olat.course.learningpath.FullyAssessedTrigger;
import org.olat.course.learningpath.LearningPathConfigs;
import org.olat.course.learningpath.LearningPathEditConfigs;
import org.olat.course.learningpath.LearningPathNodeHandler;
import org.olat.course.learningpath.model.ModuleLearningPathConfigs;
import org.olat.course.learningpath.ui.LearningPathNodeConfigController;
import org.olat.course.nodes.CourseNode;
import org.olat.repository.RepositoryEntry;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
public class QuestionCreationLearningPathNodeHandler implements LearningPathNodeHandler {

  private static final LearningPathEditConfigs EDIT_CONFIGS =
      LearningPathEditConfigs.builder().enableNodeVisited().enableConfirmed().build();

  @Override
  public String acceptCourseNodeType() {
    return QuestionCreationCourseNode.TYPE;
  }

  @Override
  public boolean isSupported() {
    return true;
  }

  @Override
  public void updateDefaultConfigs(CourseNode courseNode, boolean newNode, INode parent) {
    getLearningPathConfigs(courseNode, newNode);
  }

  @Override
  public LearningPathConfigs getConfigs(CourseNode courseNode) {
    return getLearningPathConfigs(courseNode, false);
  }

  private ModuleLearningPathConfigs getLearningPathConfigs(CourseNode courseNode, boolean newNode) {
    ModuleLearningPathConfigs configs =
        new ModuleLearningPathConfigs(courseNode.getModuleConfiguration(), true);
    configs.updateDefaults(newNode, FullyAssessedTrigger.nodeVisited);
    return configs;
  }

  @Override
  public Controller createConfigEditController(
      UserRequest userRequest,
      WindowControl windowControl,
      RepositoryEntry courseEntry,
      CourseNode courseNode) {
    return new LearningPathNodeConfigController(
        userRequest, windowControl, courseEntry, courseNode, EDIT_CONFIGS);
  }

  @Override
  public LearningPathEditConfigs getEditConfigs() {
    return EDIT_CONFIGS;
  }

  @Override
  public void onMigrated(CourseNode courseNode) {
    // Do nothing here
  }
}
