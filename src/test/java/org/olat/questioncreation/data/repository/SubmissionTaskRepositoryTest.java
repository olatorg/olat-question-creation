/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@DataJpaTest
@ActiveProfiles("test")
@ContextConfiguration(
    classes = QuestionCreationRepositoryTestConfiguration.class,
    loader = QuestionCreationRepositoryTestConfiguration.QuestionCreationTestContextLoader.class)
@Sql(scripts = "classpath:SubmissionTaskRepositoryTest_Create.sql")
@Sql(
    scripts = "classpath:SubmissionTaskRepositoryTest_Delete.sql",
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class SubmissionTaskRepositoryTest {

  @Autowired private SubmissionTaskRepository submissionTaskRepository;

  @Test
  void testFindByIdentityIdAndCourseNodeIdentAndSubmissionTaskIndex_notEmpty() {
    assertTrue(
        submissionTaskRepository
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(1, "1", 0)
            .isPresent());
    assertTrue(
        submissionTaskRepository
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(1, "2", 0)
            .isPresent());
    assertTrue(
        submissionTaskRepository
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(1, "2", 1)
            .isPresent());
  }

  @Test
  void testFindByIdentityIdAndCourseNodeIdentAndSubmissionTaskIndex_empty() {
    assertTrue(
        submissionTaskRepository
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(2, "1", 0)
            .isEmpty());
    assertTrue(
        submissionTaskRepository
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(1, "3", 0)
            .isEmpty());
    assertTrue(
        submissionTaskRepository
            .findByIdentityIdAndRepoCourseNodeIdentAndSubmissionTaskIndex(1, "1", 1)
            .isEmpty());
  }
}
