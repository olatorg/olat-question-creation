/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.data.repository;

import java.util.List;
import javax.sql.DataSource;
import org.olat.core.CoreSpringInitializer;
import org.olat.core.util.WebappHelper;
import org.olat.openolat.processor.FlywayDependsOnDatabaseUpgraderProcessor;
import org.olat.questioncreation.autoconfigure.QuestionCreationProperties;
import org.olat.upgrade.DatabaseUpgradeManager;
import org.olat.upgrade.UpgradesDefinitions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.jpa.support.MergingPersistenceUnitManager;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.MergedContextConfiguration;

/**
 * @author Mateja Culjak
 * @since 1.0
 */
@Configuration
@EnableConfigurationProperties(QuestionCreationProperties.class)
@EnableAutoConfiguration
@EnableJpaRepositories("org.olat.questioncreation.data.repository")
public class QuestionCreationRepositoryTestConfiguration {

  @Bean
  public MergingPersistenceUnitManager mergingPersistenceUnitManager() {
    return new MergingPersistenceUnitManager();
  }

  @Bean(name = "org.olat.core.util.WebappHelper", initMethod = "init", destroyMethod = "destroy")
  public WebappHelper webappHelper(@Value("${userdata.dir}") String userdataDir) {
    WebappHelper webappHelper = new WebappHelper();
    webappHelper.setFullPathToSrc("");
    webappHelper.setServletContext(new MockServletContext());
    webappHelper.setUserDataRoot(userdataDir);
    return webappHelper;
  }

  @Bean(initMethod = "init")
  @DependsOn("org.olat.core.util.WebappHelper")
  public DatabaseUpgradeManager databaseUpgrader(DataSource dataSource) {
    DatabaseUpgradeManager databaseUpgrader = new DatabaseUpgradeManager();
    databaseUpgrader.setDbVendor("postgresql");
    UpgradesDefinitions upgradesDefinitions = BeanUtils.instantiateClass(UpgradesDefinitions.class);
    upgradesDefinitions.setUpgrades(List.of());
    databaseUpgrader.setUpgradesDefinitions(upgradesDefinitions);
    databaseUpgrader.setDataSource(dataSource);
    return databaseUpgrader;
  }

  @Bean
  public static FlywayDependsOnDatabaseUpgraderProcessor
      flywayDependsOnDatabaseUpgraderProcessor() {
    return new FlywayDependsOnDatabaseUpgraderProcessor();
  }

  /**
   * This extension of {@link SpringBootContextLoader} removes OpenOlat's {@code
   * CoreSpringInitializer} from the list of initializers.
   *
   * @author Christian Schweizer
   * @since 0.0.1
   */
  public static class QuestionCreationTestContextLoader extends SpringBootContextLoader {

    @Override
    protected List<ApplicationContextInitializer<?>> getInitializers(
        MergedContextConfiguration config, SpringApplication application) {
      List<ApplicationContextInitializer<?>> initializers =
          super.getInitializers(config, application);
      initializers.removeIf(
          applicationContextInitializer ->
              applicationContextInitializer instanceof CoreSpringInitializer);
      return initializers;
    }
  }
}
