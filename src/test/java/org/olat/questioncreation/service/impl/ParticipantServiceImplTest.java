/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.service.impl;

import jakarta.annotation.Nullable;
import java.util.Date;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.olat.core.util.DateUtils;
import org.olat.questioncreation.service.Participant.QuestionStatus;

class ParticipantServiceImplTest {

  @Test
  void testEvaluateQuestionStatus() {
    Date passedDate = DateUtils.addDays(new Date(), -1);
    Date futureDate = DateUtils.addDays(new Date(), 1);

    checkEvaluatedQuestionStatus(
        futureDate, futureDate, false, false, futureDate, false, QuestionStatus.UNPUBLISHED);
    checkEvaluatedQuestionStatus(
        null, futureDate, false, false, futureDate, false, QuestionStatus.SUBMISSION_IN_PROGRESS);
    checkEvaluatedQuestionStatus(
        passedDate,
        futureDate,
        false,
        false,
        futureDate,
        false,
        QuestionStatus.SUBMISSION_IN_PROGRESS);
    checkEvaluatedQuestionStatus(
        null,
        passedDate,
        false,
        false,
        futureDate,
        false,
        QuestionStatus.SUBMISSION_DEADLINE_MISSED);
    checkEvaluatedQuestionStatus(
        passedDate, futureDate, true, false, futureDate, false, QuestionStatus.SUBMISSION_DONE);
    checkEvaluatedQuestionStatus(
        passedDate, passedDate, true, false, futureDate, false, QuestionStatus.SUBMISSION_DONE);
    checkEvaluatedQuestionStatus(
        passedDate, passedDate, true, true, futureDate, false, QuestionStatus.PEER_REVIEW_ASSIGNED);
    checkEvaluatedQuestionStatus(
        passedDate, passedDate, true, true, futureDate, true, QuestionStatus.PEER_REVIEW_DONE);
    checkEvaluatedQuestionStatus(
        passedDate, passedDate, true, true, passedDate, true, QuestionStatus.PEER_REVIEW_DONE);
    checkEvaluatedQuestionStatus(
        passedDate,
        passedDate,
        true,
        true,
        passedDate,
        false,
        QuestionStatus.PEER_REVIEW_DEADLINE_MISSED);
  }

  private void checkEvaluatedQuestionStatus(
      @Nullable Date taskStartDate,
      Date taskDueDate,
      boolean finallySubmitted,
      boolean peerReviewAssigned,
      Date peerReviewDueDate,
      boolean peerReviewFinallySubmitted,
      QuestionStatus expectedQuestionStatus) {
    Assertions.assertSame(
        expectedQuestionStatus,
        ParticipantServiceImpl.evaluateQuestionStatus(
            taskStartDate,
            taskDueDate,
            finallySubmitted,
            peerReviewAssigned,
            peerReviewDueDate,
            peerReviewFinallySubmitted));
  }
}
