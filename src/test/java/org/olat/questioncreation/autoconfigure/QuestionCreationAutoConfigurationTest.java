/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.questioncreation.autoconfigure;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.olat.core.util.coordinate.CoordinatorManager;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

class QuestionCreationAutoConfigurationTest {

  private final ApplicationContextRunner contextRunner =
      new ApplicationContextRunner()
          .withConfiguration(AutoConfigurations.of(QuestionCreationAutoConfiguration.class))
          .withUserConfiguration(TestConfiguration.class);

  @Test
  // TODO Enable tests again
  @Disabled
  void contextLoads() {
    contextRunner.run(
        context -> {
          assertThat(context).hasNotFailed();
          assertThat(context).hasSingleBean(QuestionCreationProperties.class);
        });
  }

  @Test
  void contextLoads_whenDisabled() {
    contextRunner
        .withPropertyValues("olat.questioncreation.enabled=false")
        .run(
            context -> {
              assertThat(context).hasNotFailed();
              assertThat(context).doesNotHaveBean(QuestionCreationProperties.class);
              assertThat(context).doesNotHaveBean("uzhQuestionCreationAdminExtension");
            });
  }

  @Configuration
  static class TestConfiguration {
    @Bean
    public CoordinatorManager coordinatorManager() {
      return mock(CoordinatorManager.class);
    }
  }
}
