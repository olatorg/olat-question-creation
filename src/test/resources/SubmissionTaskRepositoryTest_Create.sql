INSERT INTO o_bs_identity(id, version, name)
VALUES (1, 0, 'testuser1');

INSERT INTO qc.submission_task(id, version, repo_course_node_ident, fk_creator, submission_task_index, final_submit_date, creation_date, last_modified)
VALUES (1, 0, '1', 1, 0, '2024-08-27 12:00:00', '2024-08-27 12:00:00', '2024-08-27 12:00:00');
INSERT INTO qc.submission_task(id, version, repo_course_node_ident, fk_creator, submission_task_index, final_submit_date, creation_date, last_modified)
VALUES (2, 0, '2', 1, 0, '2024-08-27 12:00:00', '2024-08-27 12:00:00', '2024-08-27 12:00:00');
INSERT INTO qc.submission_task(id, version, repo_course_node_ident, fk_creator, submission_task_index, final_submit_date, creation_date, last_modified)
VALUES (3, 0, '2', 1, 1, '2024-08-27 12:00:00', '2024-08-27 12:00:00', '2024-08-27 12:00:00');
