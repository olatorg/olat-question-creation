# Contributing to OLAT

First off, a warm welcome to the [OLAT](https://www.olat.org/) community. Thank you for your
contribution.

Have a look at the [OLAT Developer Guide](https://docs.olat.org/) for more information.

## Code of Conduct

This project and everyone participating in it are governed by
the [OLAT Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this
code. Please report unacceptable behavior to [info@olat.org](mailto:info@olat.org).

## Code Style

OLAT uses [Google's Style Guide](https://google.github.io/styleguide/) where appropriate.
